export const year = () =>{
  var yearArr = [];
  var startYear = 1949;
  var thisYear = new Date().getFullYear();
  while(startYear < thisYear){
        yearArr = yearArr.concat(++startYear);
  };
 return yearArr;
};
export const month = () =>{
      var month = [
      {name: 'Jan', val: 1},
      {name: 'Feb', val: 2},
      {name: 'Mar', val: 3},
      {name: 'Apr', val: 4},
      {name: 'May', val: 5},
      {name: 'Jun', val: 6},
      {name: 'Jul', val: 7},
      {name: 'Aug', val: 8},
      {name: 'Sept', val: 9},
      {name: 'Oct', val: 10},
      {name: 'Nov', val: 11},
      {name: 'Dec', val: 11},];
     return month;
    };