import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles';
import {inWorkspace} from '../../actions/behave/ConfigAction'
import VI_POL_006Component from '../../components/VI_POL/VI_POL_006Component/VI_POL_006Component';
import style from '../../assets/styles/vi_calendar';

const mapState = state => ({
  workspace: state.ConfigReducer.toJS().workspace
});

const mapDispatch = dispatch => ({
  inWorkspace: () => dispatch(inWorkspace())
});

const VI_POL_006Container = connect(mapState, mapDispatch)(
  withStyles(style, { withTheme: true })(VI_POL_006Component)
)

export default VI_POL_006Container;
