import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles';
import VI_POL_001Component from '../../components/VI_POL/VI_POL_001Component';
import style from '../../assets/styles/vi_pipe';
import {inWorkspace} from '../../actions/behave/ConfigAction'

const mapState = state => ({
  workspace: state.ConfigReducer.toJS().workspace
});

const mapDispatch = dispatch => ({
  inWorkspace: () => dispatch(inWorkspace())
});

const VI_POL_001Container = connect(mapState, mapDispatch)(
  withStyles(style, { withTheme: true })(VI_POL_001Component)
)

export default VI_POL_001Container;