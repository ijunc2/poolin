import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles';
import WS_004_003Component from '../../components/WS/WS_004_003Component/WS_004_003Component';
import style from '../../assets/styles/ws';
import {inWorkspace} from '../../actions/behave/ConfigAction'

const mapState = state => ({
  workspace: state.ConfigReducer.toJS().workspace
});

const mapDispatch = dispatch => ({
  inWorkspace: () => dispatch(inWorkspace())
});

const WS_004_003Container = connect(mapState, mapDispatch)(
  withStyles(style, { withTheme: true })(WS_004_003Component)
)

export default WS_004_003Container;