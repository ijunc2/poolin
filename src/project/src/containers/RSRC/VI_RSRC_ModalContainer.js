import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles';
import VI_RSRC_Modal_Component from '../../components/RSRC/VI_RSRC_Modal_Component';
import style from '../../assets/styles/rsrc';

const mapState = state => ({
});

const mapDispatch = dispatch => ({
});

const VI_RSRC_ModalContainer = connect(mapState, mapDispatch)(
  withStyles(style, { withTheme: true })(VI_RSRC_Modal_Component)
)

export default VI_RSRC_ModalContainer;
