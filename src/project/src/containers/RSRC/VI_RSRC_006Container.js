import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles';
import VI_RSRC_006Component from '../../components/RSRC/VI_RSRC_006Component/VI_RSRC_006Component';
import style from '../../assets/styles/rsrc';
import {inWorkspace} from '../../actions/behave/ConfigAction'

const mapState = state => ({
  workspace: state.ConfigReducer.toJS().workspace
});

const mapDispatch = dispatch => ({
  inWorkspace: () => dispatch(inWorkspace())
});

const VI_RSRC_006Container = connect(mapState, mapDispatch)(
  withStyles(style, { withTheme: true })(VI_RSRC_006Component)
)

export default VI_RSRC_006Container;