import color from '../styles/material/com/color'
const styles = theme => ({
  full:{
    width: '100%',
    height: '100%',
  },
  root:{
    height: '100%',
    flexDirection: 'column',
    display: 'flex',
  },
  flex:{
    display: 'flex'
  },
  wrap:{
    display: 'flex',
    height: 'calc(100% - 32px)',
    width: '100%',
    overflowX: 'auto',
  },
  contantWrap:{
    marginRight: '10px',
    //padding: '24px 0 24px 24px',
  },
  header:{
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '13px'
  },
  itemW:{
    height: `calc(100% - ${48}px)`,
    overflowY: 'auto',
    paddingRight: '12px',
    marginRight: '4px',
   },
  contant:{
    border: `1px solid ${color.gray.cardBorder}`,
    borderRadius: '4px',
    padding: '8px 0 8px 16px',
    backgroundColor: color.white.headerWhite,
  },
  widthWrap:{
    paddingBottom: '10px',
    minWidth: '280px'
  },
  //modal
  modalRoot:{
    '&>div:nth-child(2)': {
        '&>div': {
          height: '100%',
          minWidth: '500px',
          minHeight: '390px',
          maxHeight: '390px'
        }
     }
  },
  modalContent:{
    padding: '20px',
    height: '100%',
    borderRight: `10px solid ${color.gray.cardBorder}`,
    borderLeft: `10px solid ${color.gray.cardBorder}`
  },
  modalHeader:{
    display: 'flex',
    justifyContent: 'space-between'
  },
  closeBtn:{
    position: 'absolute',
    right: '0',
    top: '0',
    minWidth: '44.74px !important',
    height: '51.82px',
    padding: '0',
  },
  mrgB10:{
    marginBottom: '10px'
  },
  mrgT0:{
    marginTop: 0
  },
  textArea:{
    '&>div':{
      padding: '6px 10px',
      height: '100px',
      '&>div':{
        height: '100%',
        overflowY: 'auto'
      }
    }
  },
  width100:{
    width: '100%',
  },
  pipeWrap: {
    minWidth: '280px',
    paddingBottom: '10px',
    marginRight: '10px',
    border: `1px solid ${color.gray.cardBorder}`,
    borderRadius: '4px',
    padding: '8px 0 8px 16px',
    backgroundColor: color.white.headerWhite,
  }
}); 

export default styles;