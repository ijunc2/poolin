const color = {
  defaultFontColor:'#4c565c',
  gray: {
    default: '#384047',
    weakGray: '#84929f',
    hoverGray: '#575B5E',
    offGray: '#A9A9A9',
    cardGray: '#636F78',
    placeHoldGray: '#CDCECE',
    cardBorder: '#DEDEDE',
    menuText: '#8a9ba8',
    borderGray: '#edeff0',
    descGray: '#848F99',
  },
  white: {
    headerWhite: '#F9FAFA'
  },
  green: {
    onGreen: '#6AD790'
  },
  red: {
    onRed: '#e15258'
  },
  yellow: {
    onYellow: '#f5d44f'
  },
  skyBlue:{
    checkedBlue: '#008CE3',
    default: '#5ac0e5',
    hover:'#3DADCC',
  },
  blue:{
    default: '#2196f3'
  }
}
export default color;