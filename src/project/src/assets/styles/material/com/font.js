const font = {
  fontThin:'Noto Sans HK,sans-serif',
  fontWeight: ['200', '300', '400', '500', '600', '700', '800', 'bold'],
  fontFamily1: 'Rubik, sans-serif',
  defaultFontColor:'#4c565c'
}
export default font;