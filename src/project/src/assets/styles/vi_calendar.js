import color from '../styles/material/com/color'

const styles = theme => ({
  root: {
    height: 'calc(100% - 48px)',
    display: 'flex',
  },
  checkWrap: {
    '& label': {
      margin: 'auto 0 !important'
    },
    '&:hover': {
        '&>div>div>div>div:first-child':{
          display: 'inherit',
        },
    },
      '&>div>div>div>div:first-child': {
          display: 'none',
          minWidth: '29px',
          flexDirection: 'column',
          justifyContent: 'center',
      },
    },
  eventWrap: {
    display: 'flex',
    '& p': {
      color: '#fff',
      display: 'block',
      lineHeight: '16px'
    },
  },
  avarta: {
    fontSize: '13px',
    margin: 'auto 4px auto 0 !important',
  },
  checkBoxChecked:{
    background: '#fff',
    borderRadius: '100%',
    color: color.green.onGreen
  },
  filterWrap: {
    minWidth: '210px',
    marginRight: '10px'
  },
  eventSelectWrap: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: '10px'
  },
  selectBtn: {
    width: '210px',
    padding: '5px 12px',
    color:' #68747e !important',
    fontWeight: 600,
    cursor: 'pointer',
    '& i': {
      marginTop: '6px'
    }
  },
  eventSelect:{
    top: '20px',
    visibility: 'hidden',
    position: 'absolute',
  },
  selectBtnInner: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between'
  },
  eventSelectItem: {
    width: '160px',
  }
});

export default styles;