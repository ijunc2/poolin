import color from '../styles/material/com/color'

const drawerWidth = 230;
const toolbarHeight = 72;
const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBarHeight: {
    height: toolbarHeight
  },
  appBar: {
    backgroundColor: color.white.headerWhite,
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
   headerTitle:{
    display: 'inline-block'
  },
  headerAvartar: {
    width: '30px',
    height: '30px',
    marginTop: '25px',
    marginBottom: '25px',
    cursor: 'pointer'
  },
  headerAvartarClose: {
    marginRight: '16px'
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: 0,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    backgroundColor: color.gray.default,
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    backgroundColor: color.gray.default,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflow: 'hidden',
    width: 0,
    [theme.breakpoints.up('sm')]: {
      width: 0,
    },
  },
  toolbarWrap: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDeriction: 'column',
    overflow: 'visible',
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '0 8px 0 16px',
    minHeight: '72px'
    //...theme.mixins.toolbar,
  },
  toolbarFavo: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px 8px 16px',
    color: color.gray.hoverGray,
    '&>i': {
      marginLeft: '8px'
    },
    //...theme.mixins.toolbar,
  },
  headerInput: {
    marginLeft: 'auto',
    marginRight: '8px',
    '& input':{
      padding: '9px 14px',
      transition: theme.transitions.create('width'),
      [theme.breakpoints.up('sm')]: {
        width: 120,
        '&:focus': {
          width: 300,
        },
      },
    }
  },
  avatMenu:{
    '& div:nth-child(2)':{
      top: '56px !important',
      left: `calc(100% - ${200}px) !important`,
      right: '24px'
    }
  },
  textCenter:{
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  MenuItem:{
    padding: '8px 16px'
  },
  iconOpen:{
    color: '#fff',
    transition: 'all ease 2s 0s',
  },
  iconClose:{
    color: color.gray.offGray,
    transition: 'all ease 2s 0s',
  },
  toolbarItemOP:{
    color: '#fff',
    borderColor: '#fff',
     '&:hover':
      { backgroundColor: color.gray.hoverGray },
  },
  menuIcon:{
    width: '1.25rem',
    height: '1.25rem',
  },
  toolbarItem:{
    color: 'white',
    paddingTop: '6px',
    paddingBottom: '6px',
  },
  ProjectItem:{
    justifyContent: 'space-between',
    paddingRight: '8px'
  },
  ListWrap: {
    paddingTop: '0',
    paddingBottom: '32px'
  },
  iconBtn:{
    padding: '3px'
  },
  BrightBtn:{
    width: '.5em',
    margin: '.3rem',
    height: '.5em',
    marginRight: '16px',
  },
  bottomWrap:{
    padding: '16px',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  },
  invtBtn:{
    width: '100%',
    marginTop: '16px',
    '&:hover': {
      color: color.gray.default,
      background: '#fff'
    }
  },
  headerTitleButtonWrap: {
    display: 'flex',
    position: 'absolute',
    bottom: 0,
  },
  headerTitleMenuOn: {
    padding: '0 16px',
    borderBottom: '3px solid #008CE3',
    '& h6':{
      color: '#008CE3 !important',
    }
  },
  headerTitleMenuOff: {
    padding: '0 16px',
    borderBottom: '3px solid #848F99',
    '& h6':{
      color: '#848F99 !important',
    }
  },
  eventSelect:{
    top: '20px',
    visibility: 'hidden',
    position: 'absolute',
  },
  eventSelectItem: {
    width: '200px',
    margin: '8px',
    padding: '8px',
    borderRadius: '4px',
  },
  ////초대 모달/////
  inviteTitleText:{
    lineHeight: '1.5',
    margin: '16px 16px 10px 16px'
  },
  inviteInputChangeBtn:{
    width: '100%',
    minHeight: '50px',
    borderRadius: '4px',
    padding: '8px 4px',
    border: `1px solid ${color.gray.placeHoldGray}`,
  },
  inviteButton:{
    padding: '8px 12px',
    verticalAlign: 'bottom',
    margin: '4px',
    width: '170px',
    height: '32px',
    display: 'inline-block',
    borderRadius: '50px',
    backgroundColor: color.white.headerWhite,
    border: `1px solid ${color.gray.placeHoldGray}`,
    '&>div':{
      color: color.gray.weakGray,
      '& input': {color: color.gray.weakGray},
      '& i':{cursor: 'pointer'},
      display: 'flex',
      justifyContent: 'space-between'
    },
    '&:hover':{
      borderColor: '#2196f3',
      '&>div':{
        color: '#2196f3',
        '& input': {color: '#2196f3'},
      }
    }
  },
  inviteText:{
    padding: 0,
    width: '125px',
    outline: '0',
    background: 'none',
    lineHeight: '1.2px',
    resize: 'none',
    border: 0,
    fontWeight: 400
  },
  inviteInput: {
    margin: '4px',
    outline: '0',
    minWidth: '170px',
    maxWidth: 'calc(100% - 8px)',
    background: 'none',
    color: '#68747e',
    lineHeight: '1.2px',
    cursor: 'default',
    textDecoration: 'none !important',
    resize: 'none',
    border: 0,
    padding: '8px 10px',
    '&::placeholder': {
      color: color.gray.placeHoldGray
    },
  },
  //projectListComponent의 css//
  projectListWrap: {
    position: 'absolute',
    left: '200px',
    top: '20px',
    backgroundColor: '#fff',
    zIndex: 2,
    flexDirection: 'column',
    borderRadius: '4px',
    border: '1px solid #D5DCE0',
  },
  paletteSwitchWrap:{
    '&:hover': {
      '&>div': {
        display: 'inherit'
      }
    }
  },
  paletteWrap: {
    width: '122px',
    position: 'absolute',
    left: '180px',
    top: '10px',
    background: '#fff',
    padding: '4px',
    borderRadius: '4px',
    display: 'none',
    border: '1px solid #D5DCE0',
    '&:hover': {
      display: 'inherit'
    }
  },
  paletteItem: {
    width: '20px',
    height: '20px',
    margin: '4px',
    float: 'left',
    borderRadius: '2px',
    '&:hover': {
      opacity: 0.5
    }
  },
  paletteItemCheck: {
    width: '1rem',
    height: '1rem',
    marginLeft: '2px'
  },
  projectListBg: {
    zIndex: 1,
    position: 'fixed',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  content: {
    marginTop: toolbarHeight,
    overflowX: 'auto',
    //overflowY: 'hidden',
    flexGrow: 1,
    height: `calc(100% - ${toolbarHeight}px)`,
    //backgroundColor: theme.palette.background.default,
    backgroundColor: '#fff',
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: 0,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: drawerWidth,
  },
});

export default styles;