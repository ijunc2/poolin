import color from '../styles/material/com/color'
import { font } from './material/com';

const styles = theme => ({
  margR5:{
    marginRight: '5px'
  },
  margL5:{
    marginLeft: '5px'
  },
  margR10:{
    marginRight: '10px'
  },
  margB5:{
    marginBottom: '5px'
  },
  margB6:{
    marginBottom: '6px'
  },
  margT5:{
    marginTop: '5px !important'
  },
  margT10:{
    marginTop: '10px !important'
  },
  margB10:{
    marginBottom: '10px !important'
  },
  margB20:{
    marginBottom: '20px !important'
  },
  margB30:{
    marginBottom: '30px !important'
  },
  margB40:{
    marginBottom: '30px !important'
  },
  margT30:{
    marginTop: '30px'
  },
  width250:{
    '&>div':{
      width: '250px'
    }
  },
  width200:{
    width: '200px',
  },
  displayBlock:{
    display: 'block'
  },
  displayInline:{
    display: 'inline'
  },
  modalRoot:{
    '&>div:nth-child(2)': {
        '&>div': {
          overflowY: 'auto',
          height: '100%',
          margin: '30px',
          maxHeight: 'calc(100% - 60px)',
          width: 'calc(81.25rem - 40px)',
          maxWidth: 'calc(81.25rem - 40px)',
        }
     }
    },
    title:{
      backgroundColor: color.gray.default,
      display: 'flex',
      padding: '20px',
      justifyContent: 'space-between'
    },
    titleTextWrap:{
      marginLeft: '10px',
      minHeight: '68px',
    },
    titleStar:{
      color: '#bbb',
      cursor: 'pointer',
      fontSize: '18px',
      marginLeft: '8px',
    },
    lightStar:{
      color: '#F9E921 !important',
      '-webkit-text-stroke-width': '1px',
      '-webkit-text-stroke-color': '#d7ce51',
    },
    phEmailText:{
      '#text':{
        marginRight: '10px',
      },
      'i':{
        marginRight: '5px'
      }
    },
    scorecardBtn:{
      height: '32px',
      marginTop: 'auto',
      marginBottom: 'auto',
    },
    avatar:{
      width: '80px',
      height: '80px',
      border: '2px solid #fff',
      fontSize: '42px',
      lineHeight: '76px',
      md:{
        width: '20px',
        height: '20px',
        fontSize: '24px',
        lineHeight: '46px',
        border: 0
      }
    },
    contentWrap:{
      height: 'calc(100% - 146px)',
      backgroundColor: color.white.headerWhite,
      display: 'flex'
    },
    contentLeft:{
      margin: '0 0 20px 20px'
    },
    contentRight:{
      margin: '0 20px 20px 20px'
    },
    menuWrap:{
      padding: '20px',
      paddingBottom: 0,
      width: '100%',
      display: 'flex',
    },
    menuItem:{
      cursor: 'pointer',
      marginRight: '20px',
      paddingBottom: '15px',
    },
    menu:{
      paddingTop: '20px',
      paddingBottom: '20px',
      width: '180px',
      height: '100%',
      backgroundColor: color.white.headerWhite,
    },
    menuText:{
      marginBottom: '4px'
    },
    menuIcon:{
      zIndex: 1,
      marginRight: '8px',
      width: '26px',
      height: '26px',
      padding: '.2rem',
      borderRadius: '50%',
      backgroundColor: color.white.headerWhite
    },
    menuIconSm:{
      zIndex: 1,
      marginRight: '8px',
      width: '24px',
      height: '24px',
      padding: '.2rem',
      borderRadius: '50%',
      backgroundColor: color.white.headerWhite
    },
    timeLineIcon: {
      backgroundColor: '#fff',
      color: '#4c565c',
      border: '2px solid #dedede',
    },
    endTimeLineIcon: {
      backgroundColor: '#dedede',
      color: '#dedede',
      border: '2px solid #fff',
    },
    noteComFoot:{
        display: 'flex',
        marginTop: '5px',
        fontSize: '10px',
        color: '#97a8b6',
        fontWeight: 200,
        height: '10px',
        '&:hover':{
          '&>div':{
            display: 'flex !important',
          }
        }
    },
    offColor: {
      transition: '.3s ease',
      backgroundColor: color.white.headerWhite,
      color: color.gray.menuText,
      border: `2px solid ${color.gray.menuText}`
    },
    onColor: {
      transition: '.3s ease',
      color: '#fff',
      backgroundColor: color.green.onGreen,
      border: `2px solid ${color.green.onGreen}`
    },
    selecMenu:{
      borderBottom: `4px solid ${color.green.onGreen}` 
    },
    unSelecMenu:{
      borderBottom: `4px solid ${color.white.headerWhite}` 
    },
    splliter:{
      height: '60px',
      margin: '0 15px',
      display: 'inline-block',
      verticalAlign: 'middle',
    },
    timeLine:{
      position: 'absolute',
      width: '3px',
      top: '20px',
      bottom: '0',
      background: '#dedede',
      left: '10.5px',
    },
    detailsContainer: {
      padding: '20px 0',
    },
    detailsSection:{
      padding: '0px 20px 20px 20px',
      '& div': {
        flex: 1
      },
      '&>div:first-child': {
        borderRight: `1px solid ${color.gray.cardBorder}`
      },
      '&>div:nth-child(2)': {
        paddingLeft: '20px'
      }
    },
    detailsList: {
      padding: 0,
      margin: 0,
      '& li': {
        display: 'flex',
        margin: '0 0 10px 0',
        listStyle: 'none',
        color: color.gray.cardBorder,
        '& i': {
          lineHeight: '24px',
          fontSize: '.8rem',
          marginRight: '6px',
        }
      }
    },
    //////////////////////////////////////
    contentRoot:{
      overflowY: 'auto',
      backgroundColor: '#fff',
      padding: '20px',
      border: `1px solid ${color.gray.borderGray}`,
      height: 'calc(100% - 64px)',
      position: 'relative'
    },
    contentTitle:{
      fontSize: '20px',
      marginBottom: '10px',
      lineHeight: '1.5',
    },
    contentSubTitle:{
      fontSize: '16px',
      marginBottom: '5px',
      marginTop: '30px',
    },
    contentSubContant:{
      fontSize: '14px',
      lineHeight: '1.5',
      fontWeight: '200',
    },
    avatarText:{
      paddingTop: '13px',
      marginLeft: '20px',
      fontSize: '18px',
    },
    avatarAddText:{
      cursor: 'pointer',
      marginLeft: '20px',
      marginTop: '8px',
    },
    textArea:{
      color: font.defaultFontColor,
      display: 'inherit !important',
      marginTop: '10px',
      marginBottom: '10px',
      '&>div':{
        height: '100px',
        width: '100%',
        '&>div':{
          height: '100%',
          overflowY: 'auto'
        }
      }
    },
    emptyWrap:{
      marginTop: '80px',
      textAlign: 'center',
      color: '#84929f'
    },
    emptyIcon: {
      fontSize: '50px'
    },
    flexColSpaceBetween:{
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between'
    },
    flexSpaceBetween:{
      display: 'flex',
      justifyContent: 'space-between'
    },
    subInput:{
      display: 'inherit !important',
      '&>div':{
        width: '200px'
      }
    },
    nameInput:{
      width: '197px',
      '&>div>input::placeholder': {
        fontSize: '1.1rem',
        fontWeight: 600,
      }
    },
    summaryContent:{
      paddingLeft: '5px',
      borderLeft: `2px solid ${color.green.onGreen}`,
      width: '70%',
    },
    addExpPop:{
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      height: 'calc(100% - 40px)'
    },
    teamDiscTextArea:{
      '& textarea::placeholder': {
        fontSize: '0.8rem',
        fontWeight: 600,
        lineHeight: '1.2rem',
        fontStyle: 'italic'
      }
    },
    timeScopeWrap:{
      marginBottom: '10px',
      padding: '10px',
      border: '1px solid #dedede',
      borderRadius: '4px',
      backgroundColor: '#fafafa',
      '& input': {
        backgroundColor: '#fff !important'
      }
    },
    interViewtitle:{
      marginRight: '10px !important',
      flex: 1,
    },
    interViewTextArea:{
      flex: 1,
      backgroundColor: '#fff',
      '&>div':{
        height: '180px'
      },
      '& textarea::placeholder': {
        fontSize: '0.8rem',
        fontWeight: 600,
        lineHeight: '1.2rem',
      }
    },
    noteTextArea:{
      width: '100%',
      maxWidth: '100%',
      padding: '6px 10px',
      borderRadius: '4px',
      backgroundColor: '#fff7ef',
      border: '1px solid #dedede',
      position: 'relative',
      wordBreak: 'break-all',
    },
    IconDescWrap: {
      display: 'flex',
      position: 'relative',
      '&>div': {
        visibility: 'hidden ',
        transition: 'opacity 0.15s linear',
        },
      '&:hover': {
        '&>div': {
          visibility: 'visible ',
          background: 'red',
          transition: 'opacity 0.15s linear',
          }  
      },
    },
    IconDesc: {
      padding: '3px 6px',
      position: 'absolute',
      background: 'black !important',
      borderRadius: '4px',
      top: '20px',
      left: '-8px',
      zIndex: 1
    },
    tasksTitle: {
      '&>div>div': {
        padding: '6.5px 26px 6.5px 0'
      },
      '&>div>div:focus': {
        padding: '6.5px 26px 6.5px 0'
      },
      '& div': {
        color: '#4c565c',
        fontFamily: 'Rubik, sans-serif',
        fontSize: '20px',
        fontWeight: 500,
        border: 'none !important',
        background: 'none !important',
      },
      '& div:before': {
        border: 'none !important',
      }
    },
    CalendarModal:{
      '&>div:nth-child(2)': {
          '&>div': {
            minWidth: '500px',
          }
       }
      },
    ////////////////////////////////////////////////
    '@media (max-width: 1550px)':{
      modalRoot:{
        '&>div:nth-child(2)': {
            '&>div': {
              maxWidth: 'calc(75rem - 40px)',
            }
         }
        },
     },
    '@media (max-width: 1419px)':{
      modalRoot:{
        '&>div:nth-child(2)': {
            '&>div': {
              maxWidth: '1090px',
            }
         }
      },
    },
   '@media (max-width: 1344px)':{
    modalRoot:{
      '&>div:nth-child(2)': {
          '&>div': {
            maxWidth: '800px',
          }
       }
      },
   },
   '@media (max-width: 840px)':{
    modalRoot:{
      '&>div:nth-child(2)': {
          '&>div': {
            maxWidth: '100%',
            width: '100%',
            minWidth: '100%',
           // padding: '0 10px',
          }
       }
      },
    },
/////////////////////////////////////////////////////
'@media (min-width: 1550px)':{
  contentLeft:{
    width: '700px !important',
  },
  contentRight:{
    width: '500px !important',
  },
},
'@media (min-width: 1420px)':{
  contentLeft:{
    width: '600px',
    position: 'relative',
  },
  contentRight:{
    width: '500px !important',
  },
},
'@media (min-width: 1345px)':{//다음부터 하나짜리
  contentLeft:{
    minWidth: '550px',
    position: 'relative',
  },
  contentRight:{
    maxWidth: '500px !important',
    minWidth: '472px !important',
  },
}
}); 

export default styles;