import color from './material/com/color'

const styles = theme => ({
  full:{
    width: '100%',
    height: '100%',
  },
  root:{
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    position: 'relative'
  },
  flex:{
    display: 'flex'
  },
  contant:{
    maxWidth: '1000px',
    height: '100%',
    width: '100%',
    padding: '16px 24px 16px 24px',
    backgroundColor: color.white.headerWhite,
    boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)',
  },
  contant1:{
    maxWidth: '1000px',
    width: '100%',
    padding: '16px 24px 16px 24px',
  },
  contant2:{
    maxWidth: '800px',
    width: '100%'
  },
  wrap003:{
    height: `calc(100% - ${48}px)`,
    overflowY: 'auto',
    margin: '16px 0',
  },
  textWrap003:{
    width: 'calc(100% - 24px)', 
    borderBottom: `1.5px solid ${color.gray.placeHoldGray}`
  },
  itemUnderBar:{
    width: '100%', 
    borderBottom: `1.5px solid ${color.gray.placeHoldGray}`
  },
  projectText: {
    background: `${color.gray.placeHoldGray}`,
    padding: '2px 8px',
    borderRadius: '16px',
    margin: 'auto 0'
  },
  eventSelect:{
    top: '20px',
    visibility: 'hidden',
    position: 'absolute',
  },
  btnWrap: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  selectWrap: {
    justifyContent: 'flex-end',
    display: 'flex',
  },
  dateWrap: {
    margin: 'auto 4px auto 0',
    width: '30px',
    justifyContent: 'flex-end',
    display: 'flex',
  },
  ///GridComponent001////////
  itemWrap001: {
    backgroundColor: '#fff',
    borderRadius: '4px',
    boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)',
  },
  itemWrap002: {
    // height: 205,
    width: 152,
    background: '#fff',
    borderRadius: '4px',
    boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)',
  },
  descWrap: {
    borderTop: '1px solid #E5E5E5',
    display: 'flex',
    padding: '4px 8px'
  },
  img002: {
    width: '-webkit-fill-available',
    height: '-webkit-fill-available',
    margin: 'auto',
    verticalAlign: 'top',
  },
  avarta: {
    fontSize: '13px',
    margin: 'auto 4px !important',
  },
  avarta1: {
    fontSize: '8px',
    margin: 'auto 4px auto 0 !important',
  },
  listIcon: {
    marginBottom: '4px',
    fontSize: '24px',
    color: '#4086DF',
  },
  listIcon1: {
    margin: 'auto',
    fontSize: '40px',
    color: '#4086DF',
  },
  gridCardtitlle: {
    display: 'flex',
    width: '160px',
    justifyContent: 'center',
  },
  inboxWrap: {
    borderRadius: '4px',
    padding: '8px 4px',
    border: '2px solid #E0E6E8',
    '&:hover': {
      background: '#F6F8F9',
    }
  },
  inboxContentWrap: {
    padding: '0 8px',
  },
  agoDayFont: {
    fontSize: '10px',
    fontWeight: 500,
    color: '#848F99 !important',
  },
}); 

export default styles;