import React from 'react'
import Workspace from '../internal/Workspace';
import classNames from 'classnames';
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import { List } from 'immutable';
import Item from './TaskListItem';
import { Typography, Button } from '../unit/index'

const taskSort = [
  {name: 'All Tasks', value: 2},
  {name: 'Completed Tasks', value: 1},
  {name: 'Incomplete Tasks', value: 0}]

const typeSort = [
  {name: 'None', value: 2},
  {name: 'Due Date', value: 1},
  {name: 'Price', value: 0},
]

class WS_004_001Component extends Workspace {
  constructor(props){
    super(props)
    this.state={
      selectOpen: false,
      selectOpen2: false,
      selectValue: 2,
      selectValue2: 2,
      detailsOpen: false,
      checked: true,
      arr: new List([
        {checked : true, title: '서울 출장!', project: '프로젝트 명', cost: 90000, date: new Date()},
        {checked : false, title: '3월 월급', project: '프로젝트 명', cost: 40000, date: new Date(2009, 11, 31)},
      ])
    }
    this.handleChanged = this.handleChanged.bind(this);
    this.handleBtnName = this.handleBtnName.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleDetailOpen = this.handleDetailOpen.bind(this);
    this.handleDetailClose = this.handleDetailClose.bind(this);
  }

  handleBtnName(val, arr){// 버튼 제목 바꿔주는 함수
    for(let i = 0; i < arr.length; i++){
      if(val === arr[i].value)
       return arr[i].name
    }
  }

  handleOpen(name){
    //모달을 여는 함수
    this.setState({[name]: !this.state[name]});
  }

  handleChanged(e, i, flg){
    const {value, name, checked} = e.target;
    var obj;
    if(flg === 'check'){
      obj={ arr: this.state.arr.setIn([i, 'checked'], checked) }
    }else{
       obj={ [name]: value };
    }
    this.setState(obj);
  }

  handleDetailOpen(e){
    e.stopPropagation();// 이벤트가 여러개 겹쳐 있을 경우 막아주는 이벤트 입니다.
    this.setState({detailsOpen: true});
  }

  handleDetailClose(){
    if(this.state.detailsOpen){
      this.setState({detailsOpen: false});
    }
  }
  
  render() {
    const { classes } = this.props;

    const taskFilterArr = this.state.selectValue === 2 ? this.state.arr : 
          this.state.arr.filter(m => m.checked === Boolean(this.state.selectValue));           

    const typeFilterArr = this.state.selectValue2 === 2 ? taskFilterArr : 
    this.state.selectValue2 === 1 ? 
      taskFilterArr.sort((a, b) => a['cost'] > b['cost'] ? 1 : ((a['cost'] < b['cost'] ? -1 : 0)))
      : taskFilterArr.sort((a, b) => a['date'] > b['date'] ? 1 : ((a['date'] < b['date'] ? -1 : 0)));

    const select__component =(value, open, arr)=> 
      <form autoComplete="off"
          style={{width: open === 'selectOpen2' ? '85px' :  '140px'}}
          className={classes.selectWrap}>
        <Button className={classes.selectBtn} onClick={()=>this.handleOpen(open)} bgNone>
            {this.handleBtnName(this.state[value], arr)==='None' ?
              <><i className="fas fa-sort-amount-down"
                  style={{marginRight: '4px'}}/>Sort</>
              :
              this.handleBtnName(this.state[value], arr)}
            <i className="fas fa-caret-down"
              style={{marginLeft: '4px'}}/>
        </Button>
      <FormControl style={{position: 'relative'}}>
        <Select
          className={classes.eventSelect}
          style={{left: open === 'selectOpen2' ? '-70px' :  '-130px'}}
          open={this.state[open]}
          onClose={()=>this.handleOpen(open)}
          onOpen={()=>this.handleOpen(open)}
          value={this.state[value]}
          name={value}
          onChange={e=>this.handleChanged(e)}>
          {arr.map((r, i)=>
            <MenuItem
              key={i}
              value={r.value}
              className={classes.eventSelectItem}>
                <Typography
                  variant={'caption'}>
                  {r.name}
                </Typography>
            </MenuItem>
            )}
        </Select>
      </FormControl>
    </form>;
    return (
        <div className={classNames(classes.root,classes.full)}
          onClick={()=>this.handleDetailClose()}>
          <div className={classes.contant}>
            <div className={classes.btnWrap}>
              {select__component('selectValue', 'selectOpen', taskSort)}
              {select__component('selectValue2', 'selectOpen2', typeSort)}
            </div>
            <div className={classNames(classes.wrap003)}>
            {typeFilterArr.toJS().map((r,i)=>(
              <Item
                data={r}
                key={i}
                classes={classes}
                onClick={e=>this.handleDetailOpen(e)}
                handleChanged={e=>this.handleChanged(e,i,'check')}/>
            ))}
            </div>
          </div>
          <div className={classes.contant}
            style={{marginLeft: '24px', display: this.state.detailsOpen ? 'inherit' : 'none'}}>

          </div>
        </div>
    )
  }
}

export default WS_004_001Component