import React from 'react'
import Workspace from '../../internal/Workspace';
import classNames from 'classnames';
import InboxItem from './InboxItem';
import { List } from 'immutable';

class WS_005Component extends Workspace {
  constructor(props){
    super(props)
    this.state={
      detailsOpen: false,
      arr: new List([
        { checked : true,
          owner: 'Ethan',
          title: '서울 출장',
          pipeTitle: '파이프명1',
          project: '프로젝트 명11112312',
          cost: 90000,
          content: [
            {owner: 'asdf', color: 'red', chat: '네 알겠습니다.', date: new Date(new Date().setMonth(new Date().getMonth() - 1))},
            {owner: 'sj', color: 'blue', chat: '금액을 조금 더 낮춰서 잡아주세요!', date: new Date(new Date().setMonth(new Date().getMonth() - 1))},
          ],
          date: new Date()},
        { checked : false,
          owner: 'Zzangu',
          title: '3월 월급',
          pipeTitle: '파이프명',
          project: '프로젝트 명',
          cost: 40000,
          content: [
            {owner: 'asdf', color: 'red', chat: '네 알겠습니다.', date: new Date(new Date().setMonth(new Date().getMonth() - 1))},
            {owner: 'sj', color: 'blue', chat: '금액을 조금 더 낮춰서 잡아주세요!', date: new Date(new Date().setMonth(new Date().getMonth() - 1))},
            {owner: 'opf', color: 'yellow',chat: '예산이 너무 많이 잡힌 것 같네요.', date: new Date(new Date().setMonth(new Date().getMonth() - 1))},
          ],
          date: new Date(new Date().setMonth(new Date().getMonth() - 1))},
      ])
    }
    this.handleChanged = this.handleChanged.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleDetailOpen = this.handleDetailOpen.bind(this);
    this.handleDetailClose = this.handleDetailClose.bind(this);
  }

  handleOpen(name){
    //모달을 여는 함수
    this.setState({[name]: !this.state[name]});
  }

  handleChanged(e, i, flg){
    const {value, name, checked} = e.target;
    var obj;
    if(flg === 'check'){
      obj={ arr: this.state.arr.setIn([i, 'checked'], checked) }
    }else{
       obj={ [name]: value };
    }
    this.setState(obj);
  }

  handleDetailOpen(e){
    e.stopPropagation();// 이벤트가 여러개 겹쳐 있을 경우 막아주는 이벤트 입니다.
    this.setState({detailsOpen: true});
  }

  handleDetailClose(){
    if(this.state.detailsOpen){
      this.setState({detailsOpen: false});
    }
  }
  
  render() {
    const { classes } = this.props;

    return (
        <div className={classNames(classes.root,classes.full)}
          onClick={()=>this.handleDetailClose()}>
          <div className={classes.contant2}>
            {this.state.arr.map((r, i)=>
              <InboxItem
                key={i}
                data={r}
                classes={classes}
                onClick={e=>this.handleDetailOpen(e)}
                handleChanged={e=>this.handleChanged(e, i, 'check')}
                />
              )}
          </div>
          <div className={classes.contant}
            style={{marginLeft: '24px', display: this.state.detailsOpen ? 'inherit' : 'none'}}>

          </div>
        </div>
    )
  }
}

export default WS_005Component