import React from 'react';
import { dateFormat } from '../../../assets/utils/comm';
import { Typography, Checkbox, Avatar } from '../../unit/index';
import { color } from '../../../assets/styles/material/com/index';
import moment from 'moment'

const InboxItem = ({ ...state }) => {

  const {
    classes,
    data,
    handleChanged,
    ...other
  } = state;

  return (
    <div className={classes.inboxWrap} {...other}>
      <div style={{display: 'flex', marginBottom: '8px'}}>
        <Checkbox
          name='checked'
          color={color.skyBlue.checkedBlue}
          onChange={e=>handleChanged(e)}
          checked={data.checked}/>
        <Typography 
          style={{margin: 'auto 16px'}}>
          {data.pipeTitle}
        </Typography>
        <Typography
          color={'black'}
          variant={'caption'}
          className={classes.projectText}>
          {data.project}
        </Typography>
      </div>
      <div className={classes.inboxContentWrap}>
        <div className={classes.flex}>
          <Typography 
            variant={'h6'}
            fontWeight={4}
            style={{marginRight: '16px'}}>
            {data.title}
          </Typography>
          <Typography 
            variant={'caption'}
            color={color.gray.descGray}
            style={{margin: 'auto 0 4px 0'}}>
            {dateFormat(data.date)}
          </Typography>
        </div>
        {data.checked ? 
          <div className={classes.flex}>
            <Checkbox
              name='checked'
              color={color.skyBlue.checkedBlue}
              onChange={handleChanged}
              checked={true}/>
            <Typography 
              variant={'caption'}
              fontWeight={3}
              color={color.skyBlue.checkedBlue}
              style={{margin: 'auto 0 4px 0'}}>
              {`${data.owner} 님이 완료처리를 했습니다.`}
            </Typography>
            <Typography 
              className={classes.agoDayFont}
              style={{margin: 'auto 0 4px 8px'}}>
              {moment(data.date, "YYYYMMDD").fromNow()}
            </Typography>
          </div> 
          :
          <>
            {data.content.map((r, i)=>
              <div className={classes.flex} key={i}>
              <Avatar
                type={'sm'}
                color={r.color}
                style={{fontSize: '10px'}}>
                {String(r.owner).substring(0, 2).toUpperCase()}
              </Avatar>
                <Typography 
                  variant={'caption'}
                  style={{margin: 'auto 0 4px 8px'}}>
                  {r.chat}
                </Typography>
                <Typography 
                  className={classes.agoDayFont}
                  style={{margin: 'auto 0 4px 8px'}}>
                  {moment(data.date, "YYYYMMDD").fromNow()}
                </Typography>
              </div>
              )}
            <div className={classes.flex}>
              <Typography 
                variant={'caption'}
                color={color.gray.descGray}
                style={{margin: 'auto 0 4px 32px'}}>
                {`${data.owner} 님이 작성을 했습니다.`}
              </Typography>
              <Typography
                className={classes.agoDayFont}
                style={{margin: 'auto 0 4px 8px'}}>
                {moment(data.date, "YYYYMMDD").fromNow()}
              </Typography>
            </div>
        </>}
      </div>
    </div>
  );
};

export default InboxItem;