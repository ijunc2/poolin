import React from 'react';
import { Typography, IconFileType, Avatar } from '../../unit/index'
import StackGrid, { transitions, easings } from  "react-stack-grid";

const transition = transitions.scaleDown;

const GridComponent001 = ({ ...state }) => {

  const {
    classes,
    data,
    handleChanged,
    images,
    ...other
  } = state;
  const img = ['bmp', 'rle', 'dib', 'jpeg', 'jpg', 'gif', 'png', 'tif', 'tiff'];
  const imgChecked=(filename)=> Boolean(img.find( data => data === filename.substring(filename.lastIndexOf('.')+1)))
  return (
    <StackGrid
      {...other}
      monitorImagesLoaded
      columnWidth={225}
      gutterWidth={8}
      gutterHeight={8}
      easing={easings.cubicOut}
      appearDelay={60}
      appear={transition.appear}
      appeared={transition.appeared}
      enter={transition.enter}
      entered={transition.entered}
      leaved={transition.leaved}>
      {images.map(obj => (
        <div
          key={obj.src}
          className={classes.itemWrap001}>
            <div 
            style={{display: 'flex',
                    justifyContent: 'center',
                    height: imgChecked(obj.fileName) ? 'auto' : '120px'}}>
            {imgChecked(obj.fileName)
              ?
              <img src={obj.src} alt={obj.label} 
                style={{
                  maxWidth: '100%',
                  height: 'auto',
                  verticalAlign: 'top',
                  padding: '8px',
                }}/>
              :
              <IconFileType
                className={classes.listIcon1}
                filename={obj.fileName}/>
            }
            </div>
          <div className={classes.descWrap}>
          <Avatar
            className={classes.avarta}
            type='small1'
            borderColor={'black'}
            color={'black'}>
            {String(obj.owner).substring(0, 2).toUpperCase()}
          </Avatar>
          <div>
            <Typography
              variant={'caption'}>
              {obj.fileName}
            </Typography>
            <Typography
              style={{fontSize: '10px'}}
              variant={'caption'}>
              {obj.cardTitle}
            </Typography>
          </div>
          </div>
        </div>
      ))}
  </StackGrid>
  );
};

export default GridComponent001;