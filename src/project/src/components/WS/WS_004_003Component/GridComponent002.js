import React from 'react';
import { Typography, IconFileType, Avatar } from '../../unit/index'

import Grid from '@material-ui/core/Grid';

const GridComponent002 = ({ ...state }) => {

  const {
    classes,
    data,
    handleChanged,
    images,
    ...other
  } = state;
  const img = ['bmp', 'rle', 'dib', 'jpeg', 'jpg', 'gif', 'png', 'tif', 'tiff'];
  return (
    <Grid container style={{flexGrow: 1}} spacing={0} {...other}>
      <Grid container justify="center" spacing={8} >
        {images.map((obj, idx) => (
          <Grid key={idx} item>
            <div className={classes.itemWrap002}>
                <div style={{display: 'flex', justifyContent: 'center', height: 170, padding: '4px'}}>
                  {Boolean(img.find( data => data === obj.fileName.substring(obj.fileName.lastIndexOf('.')+1)))
                    ?
                    <img src={obj.src} alt={obj.label} className={classes.img002}/>
                    :
                    <IconFileType
                      className={classes.listIcon1}
                      filename={obj.fileName}/>
                  }
                </div>
              <div className={classes.descWrap}>
              <Avatar
                className={classes.avarta1}
                type='sm'
                borderColor={'black'}
                color={'black'}>
                {String(obj.owner).substring(0, 2).toUpperCase()}
              </Avatar>
              <div>
                <Typography
                  style={{fontSize: '10px'}}
                  variant={'caption'}>
                  {obj.fileName}
                </Typography>
                <Typography
                  style={{fontSize: '8px'}}
                  variant={'caption'}>
                  {obj.cardTitle}
                </Typography>
              </div>
              </div>
            </div>
          </Grid>
        ))}
      </Grid>
  </Grid>
  );
};

export default GridComponent002;