import React from 'react';
import Item from './ListItem';
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import classNames from 'classnames';
import { Typography, Button } from '../../unit/index';

const agoSort = [
  {name: 'All', value: false},
  {name: 'A Week Ago', value: new Date(new Date().setDate(new Date().getDate() - 7))},
  {name: 'A Month Ago', value : new Date(new Date().setMonth(new Date().getMonth() - 1))},
  {name: 'A Year Ago', value: new Date(new Date().setYear(new Date().getFullYear() - 1))}]

class GridComponent003 extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      selectOpen: false,
      selectValue: false,
      images: this.props.images
    }
    this.handleChanged = this.handleChanged.bind(this);
    this.handleBtnName = this.handleBtnName.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
  }

  handleBtnName(val, arr){// 버튼 제목 바꿔주는 함수
    for(let i = 0; i < arr.length; i++){
      if(val === arr[i].value)
       return arr[i].name
    }
  }

  handleOpen(name){
    //모달을 여는 함수
    this.setState({[name]: !this.state[name]})
  }

  handleChanged(e){
    const {value, name} = e.target;
    var obj={ [name]: value };
    this.setState(obj)
  }

  render() { 

    const { classes } = this.props;

    const filterArray = this.state.selectValue ?
          this.state.images.filter(m => m.date > this.state.selectValue)
          :
          this.state.images;           


    return (
        // 전체를 감사는 div
      <div style={{ height: 'calc(100% - 24px)'}}>
        {/* select 태그 */}
      <div className={classes.selectWrap}>
        <form autoComplete="off"
          style={{width: '120px'}}
          className={classes.selectWrap}>
          <Button className={classes.selectBtn} onClick={()=>this.handleOpen('selectOpen')} bgNone>
            {this.handleBtnName(this.state.selectValue, agoSort)}
              <i className="fas fa-caret-down"
                style={{marginLeft: '4px'}}/>
          </Button>
        <FormControl style={{position: 'relative'}}>
          <Select
            className={classes.eventSelect}
            style={{left: '-100px'}}
            open={this.state.selectOpen}
            onClose={()=>this.handleOpen('selectOpen')}
            onOpen={()=>this.handleOpen('selectOpen')}
            value={this.state.selectValue}
            name={'selectValue'}
            onChange={e=>this.handleChanged(e)}>
            {agoSort.map((r, i)=>
              <MenuItem
                key={i}
                value={r.value}
                className={classes.eventSelectItem}>
                  <Typography
                    variant={'caption'}>
                    {r.name}
                  </Typography>
              </MenuItem>
              )}
          </Select>
        </FormControl>
        </form>
      </div>
       {/* 리스트 */}
      <div className={classes.wrap003}>
        {/* 리스트의 컬럼 바 */}
        <div
          style={{justifyContent: 'space-between', marginBottom: '8px'}}
          className={classNames(classes.itemUnderBar,classes.flex)}>
        <Typography
          style={{margin: 'auto 0 auto 24px'}}
          variant={'body2'}>
          파일이름
        </Typography>
        <div style={{display: 'flex'}}>
        <div className={classes.gridCardtitlle}
          style={{marginLeft: '8 !important'}}>
          <Typography
            style={{margin: 'auto 0'}}
            variant={'caption'}>
              카드제목
          </Typography>
        </div>
        <Typography
          style={{margin: 'auto 8px auto 8px'}}
          variant={'caption'}>
            소유자
        </Typography>
        <Typography
          style={{margin: 'auto 12px auto 0'}}
          variant={'caption'}>
            업로드 날짜
        </Typography>
        <Typography
          style={{margin: 'auto 4px'}}
          variant={'caption'}>
            파일크기
        </Typography>
        </div>
      </div>
      {filterArray.map((obj, idx) => (
        <Item
          key={idx}
          data={obj}
          classes={classes}/>
        ))}
      </div>
  </div>
    )
  }
}

export default  GridComponent003;