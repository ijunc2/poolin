import React from 'react';
import classNames from 'classnames';
 import { dateFormat, fileSize } from '../../../assets/utils/comm';
import { Typography, Avatar, IconFileType } from '../../unit/index';

const ListItem = ({ ...state }) => {

  const {
    classes,
    data,
    handleChanged,
    ...other
  } = state;

  return (
          <div
            style={{justifyContent: 'space-between', marginBottom: '8px'}}
            className={classNames(classes.itemUnderBar,classes.flex)}
            {...other}>
          <div className={classes.flex}>
            <IconFileType
              className={classes.listIcon}
              filename={data.fileName}/>
            <Typography
              style={{margin: 'auto 0 auto 8px'}}
              variant={'body2'}>
              {data.fileName}
            </Typography>
          </div>
          <div style={{display: 'flex'}}>
            <div className={classes.gridCardtitlle}
              style={{margin: '0 8px'}}>
              <Typography
                style={{margin: 'auto 0'}}
                variant={'caption'}>
                {data.cardTitle}
              </Typography>
            </div>
            <Avatar
              className={classes.avarta}
              type='small1'
              borderColor={'black'}
              color={'black'}>
              {String(data.owner).substring(0, 2).toUpperCase()}
            </Avatar>
            <Typography
              style={{margin: 'auto 8px auto 8px'}}
              variant={'caption'}>
                {dateFormat(data.date)}
            </Typography>
            <Typography
              style={{margin: 'auto 8px auto 8px'}}
              variant={'caption'}>
                {fileSize(data.fileSize, true)}
            </Typography>
          </div>
        </div>
  );
};

export default ListItem;