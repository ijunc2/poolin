import React from 'react';
import classNames from 'classnames';
import { comma } from '../../assets/utils/comm'
import { Typography, Checkbox } from '../unit/index'

const TaskListItem = ({ ...state }) => {

  const {
    classes,
    data,
    handleChanged,
    ...other
  } = state;

  return (
    <div 
    style={{display: 'flex',
            marginBottom: '8px',
            zIndex: 1,
            opacity: data.checked ? 0.7 : 1}} {...other}>
      <Checkbox
        checked={data.checked}
        onChange={e=>handleChanged(e)}/>
          <div
            style={{justifyContent: 'space-between'}}
            className={classNames(classes.textWrap003,classes.flex)}>
          <Typography
            style={{marginLeft: '4px'}}
            variant={'body2'}>
            {data.title}
          </Typography>
          <div style={{display: 'flex'}}>
          <Typography
              style={{margin: 'auto 8px auto 0'}}
              variant={'caption'}>
                {`${comma(data.cost)} 원`}
            </Typography>
            <Typography
              color={'black'}
              variant={'caption'}
              className={classes.projectText}>
              {data.project}
            </Typography>
            <div className={classes.dateWrap}>
              <Typography
                variant={'caption'}>
                  {`${data.date.getMonth()+1} ${data.date.getDate()}`}
              </Typography>
            </div>
          </div>
        </div>
  </div>
  );
};

export default TaskListItem;