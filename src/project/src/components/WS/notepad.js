import React from 'react'
import Workspace from '../../internal/Workspace';
import classNames from 'classnames';
import Grid001 from './GridComponent001';
import Grid002 from './GridComponent002';
import StackGrid, { transitions, easings } from  "react-stack-grid";

const transition = transitions.scaleDown;

const images = [
  // { src: 'https://t1.daumcdn.net/cfile/tistory/9998CD3D5AEED5B920',
  //   fileName: 'Sample image 8.jpg',    cardTitle: '카트 제목',    owner: 'AS' },
  // { src: 'https://t1.daumcdn.net/cfile/tistory/2774583D518AE5E826',
  //   fileName: 'Sample image 8.jpg',    cardTitle: '카트 제목',    owner: 'AS' },
  // { src: 'http://weekly.chosun.com/up_fd/wc_news/2116/bimg_org/2116_74_01.jpg',
  //   fileName: 'Sample image 8.jpg',    cardTitle: '카트 제목',    owner: 'AS' },
  // { src: 'https://ae01.alicdn.com/kf/HTB10TLOPXXXXXXWaVXXq6xXFXXXb/Vincent-van-gogh-bandaged-ear.jpg_640x640.jpg',
  //   fileName: 'Sample image 8.jpg',    cardTitle: '카트 제목',    owner: 'AS' },
  // { src: 'https://ae01.alicdn.com/kf/HTB1miRWOXXXXXaTaVXXq6xXFXXXP/-.jpg_640x640.jpg',
  //   fileName: 'Sample image 8.jpg',    cardTitle: '카트 제목',    owner: 'AS' },
  { src: 'http://cfile202.uf.daum.net/image/241CEF3455DE332002E3DC',
    fileName: 'Sample image 8.jpg',    cardTitle: '카트 제목',    owner: 'AS' },
  { src: 'https://t1.daumcdn.net/cfile/tistory/99113D4E5BB4ABE105',
    fileName: 'Sample image 8.jpg',    cardTitle: '카트 제목',    owner: 'AS' },
  { src: 'https://img.hankyung.com/photo/201512/AA.11053894.1.jpg',
    fileName: 'Sample image 8.jpg',    cardTitle: '카트 제목',    owner: 'AS' },
  { src: 'https://image.mycelebs.com/art/sq/213717_sq_01.jpg',
    fileName: 'Sample image 8.jpg',    cardTitle: '카트 제목',    owner: 'AS' },
  { src: 'http://archivenew.vop.co.kr/images/702813b44e7ff335aad3e5d0642c7a2c/2012-10/18053414_001.jpg',
    fileName: 'Sample image 8.jpg',    cardTitle: '카트 제목',    owner: 'AS' },
  { src: 'http://cfile222.uf.daum.net/image/1210C9484E283D1B1476BC',
    fileName: 'Sample image 8.jpg',    cardTitle: '카트 제목',    owner: 'AS' },
  { src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSMGm-0wVxq_ivP_HemAHJEF5utP9TM45Y8ObmOJ4kzLkTYyBcVdQ',
    fileName: 'Sample image 8.jpg',    cardTitle: '카트 제목',    owner: 'AS' },
  { src: 'https://i.pinimg.com/736x/88/68/f7/8868f720eb70013782af5b8cc70d3bbe.jpg',
    fileName: 'Sample image 8.jpg',    cardTitle: '카트 제목',    owner: 'AS' },
  { src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSI9p5bj63P1Yg0jAc08dLgoy2K1enjgmRiafpjy5n5PBpppP60pA',
    fileName: 'Sample image 8.jpg',    cardTitle: '카트 제목',    owner: 'AS' },
];

class WS_004_003Component extends Workspace {
  constructor(props){
    super(props)
    this.state={
      overflowActive: false
    }
    this.handleDetectOverflow=this.handleDetectOverflow.bind(this);
  }

  handleDetectOverflow(){
    const overFlowDiv = this.overFlowDiv;
    const hasOverflowingChildren = overFlowDiv.offsetHeight < overFlowDiv.scrollHeight ||
                                    overFlowDiv.offsetWidth < overFlowDiv.scrollWidth;
    this.setState({ overflowActive: hasOverflowingChildren });
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleDetectOverflow);
  }

  componentDidMount(){
    setTimeout(function() { 
      window.addEventListener("resize", this.handleDetectOverflow);
    }, 2000);
  }

  render() {
    const { classes } = this.props;

    return (
        <div className={classNames(classes.root,classes.full)}
          onClick={()=>console.log(this.state.overflowActive)}>
          <div className={classes.contant1}
            style={{overflow: 'hidden', background: 'red'}}
            ref={(el) => {this.overFlowDiv = el}}>
            {this.state.overflowActive ? 
              <Grid002
              classes={classes}
              images={images}/>
              :
              <Grid001
              classes={classes}
              images={images}/>}
        </div>
        </div>
    )
  }
}

export default WS_004_003Component