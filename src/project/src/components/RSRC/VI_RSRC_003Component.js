import React from 'react'
import Workspace from '../internal/Workspace';
import classNames from 'classnames';

import { Typography, Button, } from '../unit/index'

class VI_RSRC_003Component extends Workspace {
    constructor(props){
        super(props)
        this.state={
          arr: []
        }
    }
    render() {
        const { classes } = this.props;
        return (
            <div className={classNames(classes.contentRoot)}>
              <div className={classes.flexSpaceBetween}>
                <Typography
                  fontWeight={3}
                  variant="h1"
                  className={classes.contentTitle}>
                  Resume / CV
              </Typography>
                <div style={{display: 'flex'}}>
                  <Button
                    onClick={()=>this.handleEdit()}
                    style={{marginRight: '10px'}}
                    btnH24
                    bgNone>
                    <i className={classNames(classes.margR5,"fas fa-cloud-download-alt")}/>
                    Download
                  </Button>
                  <Button
                    btnH24
                    stepBtn>
                    <i className={classNames(classes.margR5,"fas fa-cloud-download-alt")}/>
                    Update Resume
                  </Button>
                </div>
              </div>
              {this.state.arr.length===0 ?
                <div className={classes.emptyWrap}>
                  <i className={classNames("fas fa-file-alt", classes.emptyIcon)}/>
                  <Typography
                    fontWeight={0}
                    variant="subtitle2"
                    color={'#84929f'}
                    className={classes.margT10}>
                    This candidate doesn't have an attached resume / CV.
                  </Typography>
                </div>
                :
                <div></div>
                }

            </div>
        )
    }
}

export default VI_RSRC_003Component