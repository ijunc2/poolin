import React from 'react';
import classNames from 'classnames';
import Edit from '@material-ui/icons/Edit';
import { Typography } from '../../unit/index'
import moment from 'moment'

const NoteComponent = ({ ...state }) => {

  const {
    classes,
    data,
    priv,
    text,
    endItem,
    handleChanged,
    handleRemove,
    handleEdit,
    ...other
  } = state;

  const iCon = {margin: '3px 8px 0 8px'}
  const eDitRemove =
    <div style={{display: 'none', cursor: 'pointer'}}>
      <div onClick={handleEdit}>
        <i className="fas fa-edit" style={iCon}/>
          Edit
      </div>
      <div onClick={handleRemove}>
        <i className="fas fa-times" style={iCon}/>
          Remove
      </div>
    </div>;
  return (
    <div style={{display: 'flex', marginTop: '10px', position: 'relative'}} {...other}>
      <Edit className={classNames(
         classes.menuIconSm, classes.timeLineIcon, classes.margT5)}/>
      <div style={{width: '100%'}}>
        <div 
          className={classes.noteTextArea}>
          <Typography
            fontWeight={2}
            variant="caption">
              {data.text}
          </Typography>
        </div>
        <div className={classes.noteComFoot}>
          <div className={classes.IconDescWrap}>
            <i className="far fa-clock"
              style={{margin: '3px 5px 0 0'}}/>
            <span>
                  {moment(data.time, "YYYYMMDD").fromNow()}
            </span>
                <div className={classes.IconDesc}>
                  <Typography
                    color={'#fff'}
                    fontWeight={2}
                    variant="caption">
                    {moment(data.time, "YYYYMMDD").fromNow()}
                  </Typography>
                </div>
          </div>
        <div className={classes.IconDescWrap} style={{marginLeft: '8px'}}>
          {data.priv ?
            <i className="fas fa-lock" style={iCon}/>
                    :
            <i className="fas fa-lock-open" style={iCon}/>}
            <div className={classes.IconDesc}>
              <Typography
                color={'#fff'}
                fontWeight={2}
                variant="caption">
                  {data.priv ? 'private' : 'public' }
              </Typography>
            </div>
        </div>
          {eDitRemove}
        </div>
      </div>
    </div>
  );
};

export default NoteComponent;