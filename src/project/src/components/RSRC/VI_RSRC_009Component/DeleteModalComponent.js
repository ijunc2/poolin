import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { SubPopUp, Button, Typography } from '../../unit/index'

const DeleteModalComponent = ({ ...state }) => {

  const {
    open,
    handleClose,
    handleDelete,
  } = state;
  const  useStyles = makeStyles({
    wrap:{
      textAlign: 'center',
      top: '-80%',
      '&>div>div>div>button':{
        display: 'none !important'
        }
      },
  });
  const classes = useStyles();

  return (
      <SubPopUp
        title={'Schedule Interviews'}
        width={'500px'}
        height={'150px'}
        open={open}
        className={classes.wrap}
        handleModal={handleClose}>
        <Typography
          fontWeight={2}
          variant="caption"
          style={{marginBottom: '20px'}}>
            Are you sure you want to delete this note?
        </Typography>
        <div style={{display: 'flex', justifyContent: 'flex-end'}}>
          <Button
            bgNone
            onClick={handleClose}
            style={{marginRight: '10px'}}>
            No thanks
          </Button>
          <Button
            stepBtn
            onClick={handleDelete}
            style={{height: '32px'}}>
              <i className="fas fa-check"
                 style={{marginRight: '5px'}}/>
            Yes please
          </Button>
            </div>
    </SubPopUp>
  );
};

export default DeleteModalComponent;