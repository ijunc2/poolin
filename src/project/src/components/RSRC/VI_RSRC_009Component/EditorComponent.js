import React from 'react';
import blue from '@material-ui/core/colors/blue';
import classNames from 'classnames';
import { makeStyles } from '@material-ui/styles';
import { Input, Button } from '../../unit/index'

const EditorComponent = ({ ...state }) => {

  const {
    open,
    data,
    priv,
    text,
    endItem,
    handleChanged,
    hanldeClose,
    handlePriv,
    handleModifie,
    ...other
  } = state;

  const  useStyles = makeStyles({
    wrap:{
      position: 'absolute',
      width: '100%',
      height: '100%',
      top: 0,
      left: 0,
      backgroundColor: 'rgba(0, 0, 0, 0.3)',
      zIndex: 1,
      },
    contentWrap:{
      padding: '10px',
      background: '#fafafa',
      margin: '5px',
      borderRadius: '4px',
    },
    noteTextArea:{
      margin: '0 0 10px 0',
      width: '100%',
      '&>div':{
        padding: '6px 10px'
      },
      '& textarea::placeholder':{
        fontStyle: 'italic'
      }
    },
    alert:{
      fontSize: '14px',
      borderRadius: '4px',
      fontWeight: '500',
      padding: '15px',
      marginBottom: '20px',
      border: '1px solid #faebcc',
    },
    alertWaring:{
      backgroundColor: '#fff7ef !important',
      color: '#8a6d3b',
      borderColor: '#faebcc',
    },
    alertInfo:{
      color: '#31708f',
      backgroundColor: '#d9edf7',
      borderColor: '#bce8f1',
    },
    btn:{
      height: '30px',
      padding: '5px 12px',
      fontSize: '13px !important',
    },
    smbtn:{
      minWidth: '39px',
    }
  });
  const classes = useStyles();
  return (
    <div className={classes.wrap}
       style={{visibility: open ? 'visible' : 'hidden'}} {...other}>
      <div className={classes.contentWrap}>
        {priv ?
        <div className={classNames(classes.alert, classes.alertWaring)}
          onClick={handlePriv}>
          <i className="fas fa-lock"
              style={{marginRight: '5px'}}/>
          This note is private
        </div>
           :
        <div className={classNames(classes.alert, classes.alertInfo)}
          onClick={handlePriv}>
          <i className="fas fa-lock-open"
              style={{marginRight: '5px'}}/>
          This note is public and will be visible to Hiring Managers.
        </div>}
        <Input 
          className={classes.noteTextArea}
          onChange={handleChanged}
          placeholder="Type your text"
          value={text}
          name={'text'}
          multiline
          color={blue}/>
         <div style={{display: 'flex', justifyContent: 'flex-end'}}>
         <Button
            grayBtn
            style={{marginRight: '10px'}}
            className={classNames(classes.btn, classes.smbtn)}>
            <i className="fas fa-paperclip"/>
          </Button>
          <Button
            grayBtn
            style={{marginRight: '10px'}}
            onClick={handlePriv}
            className={classNames(classes.btn, classes.smbtn)}>
              {priv ?
                 <i className="fas fa-lock"/> 
                 :
                 <i className="fas fa-lock-open"/>}
          </Button>
          <Button
            grayBtn
            style={{marginRight: '10px'}}
            className={classes.btn}
            onClick={hanldeClose}>
            Cancel
          </Button>
          <Button
            stepBtn
            className={classes.btn}
            onClick={handleModifie}>
            Save
          </Button>
          </div>
      </div>
    </div>
  );
};

export default EditorComponent;