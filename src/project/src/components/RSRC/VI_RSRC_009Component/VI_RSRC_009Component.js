import React from 'react'
import Workspace from '../../internal/Workspace';
import Edit from '@material-ui/icons/Edit';
import NoteComponent from './NoteComponent';
import classNames from 'classnames';
import EditorComponent from './EditorComponent';
import AddNoteComponent from './AddNoteComponent';
import DeleteModalComponent from './DeleteModalComponent';
import { List } from 'immutable';
import { Typography, Button } from '../../unit/index'

class VI_RSRC_009Component extends Workspace {
    constructor(props){
        super(props)
        this.state={
          editOpen: false,
          addOpen: false,
          deleteOpen: false,
          priv: true,
          text: '',
          time: new Date(),
          idx: 0,
          arr: List([
            {text: 'asdasdasdasdasdasdasdasdasdasdasdasdasdasdasd',
              priv: false,
              time: new Date()},
            {text: 'hahahahahahahah',
              priv: false,
              time: new Date()},
            {text: 'qwer',
              priv: false,
              time: new Date()},
          ])
        }
        this.handleChanged=this.handleChanged.bind(this);
        this.handleAdd=this.handleAdd.bind(this);
        this.handleEdit=this.handleEdit.bind(this);
        this.handleRemove=this.handleRemove.bind(this);
        this.handleArrPush=this.handleArrPush.bind(this);
        this.handleModifie=this.handleModifie.bind(this);
        this.handleClear=this.handleClear.bind(this);
    }
    ////////////////////////////////////////////////////////////////////
    //배열 안 객체에서 handleChanged를 안하는 이유는
    //save cancel 버튼이 있기 때문입니다.
    ////////////////////////////////////////////////////////////////////
    handleChanged(e){
      const {name, value} = e.target;
      var obj = {};
      obj={[name]: value}
      this.setState(obj)
    }

    handleClear(){
      this.setState({
        idx: null,
        text: '',
        priv: '',
        time: new Date()
      })
    }

    handleArrPush(){
    var  obj={
        arr: this.state.arr.concat({
        text: this.state.text,
        priv: this.state.priv,
        time: new Date()
      }),
      addOpen: false
      };
      this.setState(obj);
      this.handleClear();
    }

    handleModifie(){
    var obj={
        arr: this.state.arr.set(this.state.idx,
          { text: this.state.text,
            priv: this.state.priv,
            time: this.state.time,}),
      editOpen: false
      };
      this.setState(obj);
      this.handleClear();
    }

    handleAdd(){
    var  obj = {
        editOpen: false,
        addOpen: true,
        deleteOpen: false,
      }
      this.setState(obj);
      this.handleClear();
    }

    handleEdit(r, idx){
       var  obj = {
        text: r.text,
        time: r.time,
        priv: r.priv,
        idx: idx,
        editOpen: true,
        addOpen: false,
        deleteOpen: false,
      }
      this.setState(obj);
    }

    handleRemove(idx){
       var  obj = {
        editOpen: false,
        addOpen: false,
        deleteOpen: true,
        idx: idx
      }
      this.setState(obj);
    }

    handleDelete(){
      this.setState({
        arr: this.state.arr.delete(this.state.idx),
        deleteOpen: false,
      })
    }

    render() {
        const { classes } = this.props;
        return (
          <div className={classNames(classes.contentRoot)}>
            <div className={classes.flexSpaceBetween}>
              <Typography
                fontWeight={3}
                variant="h1"
                className={classes.contentTitle}>
                Your Notes
              </Typography>
              <Button
                btnH24
                stepBtn
                onClick={()=>this.handleAdd()}>
                <i className={classNames(classes.margR5,"fas fa-plus")}/>
                Add Note
              </Button>
            </div>
            <AddNoteComponent
              open={this.state.addOpen}
              priv={this.state.priv}
              text={this.state.text}
              handlePriv={()=>this.setState({priv: !this.state.priv})}
              handleChanged={e=>this.handleChanged(e)}
              handleArrPush={()=>this.handleArrPush()}
              handleClose={()=>this.setState({addOpen: false})}/>
            {this.state.arr.toJS().length===0 ?
                <div className={classes.emptyWrap}>
                  <i className={classNames("fas fa-pencil-alt", classes.emptyIcon)}/>
                  <Typography
                    fontWeight={0}
                    variant="subtitle2"
                    color={'#84929f'}
                    className={classes.margT10}>
                    You haven't added any notes.
                  </Typography>
                </div>
                :
              <>
                <div style={{position: 'relative', paddingBottom: '30px'}}>
                  <div className={classes.timeLine}/>
                    {this.state.arr.toJS().map((r, i)=>
                      <NoteComponent
                        key={i}
                        data={r}
                        classes={classes}
                        handleEdit={()=>this.handleEdit(r, i)}
                        handleRemove={()=>this.handleRemove(i)}/>)}
                </div>
                <Edit className={classNames(classes.menuIconSm, classes.endTimeLineIcon)}/>
              </>
                }
            <EditorComponent
              open={this.state.editOpen}
              priv={this.state.priv}
              text={this.state.text}
              hanldeClose={()=>this.setState({editOpen: !this.state.editOpen})}
              handlePriv={()=>this.setState({priv: !this.state.priv})}
              handleModifie={()=>this.handleModifie()}
              handleChanged={e=>this.handleChanged(e)}/>
            <DeleteModalComponent
              open={this.state.deleteOpen}
              handleDelete={()=>this.handleDelete()}
              handleClose={()=>this.setState({deleteOpen: !this.state.deleteOpen})}
              />
          </div>
        )
    }
}

export default VI_RSRC_009Component