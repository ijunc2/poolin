import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import People from '@material-ui/icons/People';
import FormatListBulleted from '@material-ui/icons/FormatListBulleted';
import ThumbUp from '@material-ui/icons/ThumbUp';
import CardTravel from '@material-ui/icons/CardTravel';
import classNames from 'classnames';

import Step01 from '../../containers/RSRC/VI_RSRC_001Container';
import Step02 from '../../containers/RSRC/VI_RSRC_002Container';
import Step03 from '../../containers/RSRC/VI_RSRC_003Container';
import Step04 from '../../containers/RSRC/VI_RSRC_004Container';
import Step05 from '../../containers/RSRC/VI_RSRC_006Container';
import Step06 from '../../containers/RSRC/VI_RSRC_007Container';
import Step08 from '../../containers/RSRC/VI_RSRC_008Container';
import Step09 from '../../containers/RSRC/VI_RSRC_009Container';
import Step10 from '../../containers/RSRC/VI_RSRC_010Container';
import Header from './VI_RSRC_ModalHeaderCom';
import { Typography } from '../unit/index'
import { color }from '../../assets/styles/material/com/index'

const state = {
  width: window.innerWidth,
  favorites: false,
  menuSelecL: 0,
  menuSelecR: 4,
  step: 0,
}

class VI_RSRC_Modal_Component extends React.Component {

  constructor(props){
    super(props);
    this.state = state;
    this.handleSelecMenu=this.handleSelecMenu.bind(this);
    this.updateDimensions=this.updateDimensions.bind(this);
    this.step__Component=this.step__Component.bind(this);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions);
  }

  updateDimensions() {
    this.setState({
      width:  window.innerWidth
    });
  }
  
  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions);
  }

  handleSelecMenu(idx, side){
    var obj = {};
    if(side==='L')
      obj = { menuSelecL: idx };
    else if(side==='R')
      obj = { menuSelecR: idx };
    this.setState(obj);
  }

  handleChanged(e){
    const {name, value} = e.target;
    this.setState({
      [name] : value
    })
  }

  moveTo(){
    var obj = {
      step: ++this.state.step
    }
    if(this.state.step > 4)
      return
    else
      this.setState(obj);
  }
  step__Component(idx){
    switch(idx){
      case 0 :  return (
         <Step01
          name={this.props.data ? this.props.data.who : ''}/>
         );
      case 1 :  return (
          <Step02
           name={this.props.data ? this.props.data.who : ''}/>
        );
        case 2 :  return (
          <Step03/>
        );
        case 3 :  return (
          <Step04/>
        );
        case 4 :  return (
          <Step05/>
        );
        case 5 :  return (
          <Step06
            name={this.props.data ? this.props.data.who : ''}/>
        );
        case 6 :  return (
          <Step08/>
        );
        case 7 :  return (
          <Step09/>
        );
        case 8 :  return (
          <Step10/>
        );
      default:  return (
        <div/>
      );
    }
  }

  render() {
    const { classes ,handleModal, open, data} = this.props;
    const menuArr =  [
      {
        name: 'Details' ,
        Icon: <CardTravel className={classNames( classes.menuIcon, this.state.step === 0 ? classes.onColor : classes.offColor)} />
      },
      {
        name: 'Experience' ,
        Icon: <FormatListBulleted className={classNames( classes.menuIcon, this.state.step === 1 ? classes.onColor : classes.offColor)} />
      },
      {
        name: 'Resume' ,
        Icon: <ThumbUp className={classNames( classes.menuIcon, this.state.step === 2 ? classes.onColor : classes.offColor)} />
      },
      {
        name: 'Attachment' ,
        Icon: <People className={classNames( classes.menuIcon, this.state.step === 3 ? classes.onColor : classes.offColor)} />
      },
      {
        name: 'Discussion' ,
        Icon: <CardTravel className={classNames( classes.menuIcon, this.state.step === 4 ? classes.onColor : classes.offColor)} />
      },
      {
        name: 'Calendar' ,
        Icon: <FormatListBulleted className={classNames( classes.menuIcon, this.state.step === 5 ? classes.onColor : classes.offColor)} />
      },
      {
        name: 'Scorecards' ,
        Icon: <ThumbUp className={classNames( classes.menuIcon, this.state.step === 6 ? classes.onColor : classes.offColor)} />
      },
      {
        name: 'Notes' ,
        Icon: <People className={classNames( classes.menuIcon, this.state.step === 7 ? classes.onColor : classes.offColor)} />
      },
      {
        name: 'Tasks' ,
        Icon: <People className={classNames( classes.menuIcon, this.state.step === 8 ? classes.onColor : classes.offColor)} />
      },
    ];
    const menuItem__Component=(r, i, side)=>
      <div key={i} className={classes.menuItem}
        onClick={()=> this.handleSelecMenu(i, side)}>
        <div className={
          this.state.menuSelecL === i || this.state.menuSelecR === i
          ? classes.selecMenu : classes.unSelecMenu}>
            <Typography fontWeight={3}
              style={{fontSize: '14px', paddingBottom: '5px'}} >
              {r}
            </Typography>
        </div>
      </div>;
    const full__Content__Component = () =>
      <div className={classes.contentWrap}>
        <div className={classes.contentLeft}>
          <nav className={classes.menuWrap}>
            {menuArr.filter((r, i)=>i < 4).map((r,i)=>
                menuItem__Component(r.name, i, 'L'))}
          </nav>
            {this.step__Component(this.state.menuSelecL)}
        </div>
        <div className={classes.contentRight}>
          <nav className={classes.menuWrap}>
            {menuArr.filter((r,i)=>i > 3).map((r,i)=>
                menuItem__Component(r.name, i + 4, 'R'))}
          </nav>
            {this.step__Component(this.state.menuSelecR)}
        </div>
      </div>;
      const Menu__Component = ()=>
        <List className={classes.menu}>
          {menuArr.map((r, i) => (
            <ListItem 
              onClick={() => {
                this.setState({step: i})
              }}
              style={{cursor: 'pointer'}}
              key={i}>
              <div style={{position: 'relative'}}>
                {r.Icon}
              </div>
                <Typography
                  variant="caption"
                  fontWeight={6}
                  color={color.gray.menuText}
                  className={classes.menuText}>
                  {r.name}
                </Typography>
              </ListItem>
          ))}
        </List>;
        const short__Content__Component = () =>
          <div style={{display: 'flex', height: '100%'}}>
            {Menu__Component()}
            <div style={{width: '100%', height: '100%'}}>
             {this.step__Component(this.state.step)}
            </div>
          </div>;
    return (
        <Dialog
          className={classes.modalRoot}
          onClose={handleModal}
          aria-labelledby="customized-dialog-title"
          open={open}>
            <Header
              classes={classes}
              favorites={this.state.favorites}
              name={data ? data.who : ''}
              handleFavorite={()=>this.setState({favorites: !this.state.favorites})}/>
              {this.state.width > 1344 ? full__Content__Component() : short__Content__Component() }
        </Dialog>
    )
  }
}

export default VI_RSRC_Modal_Component;