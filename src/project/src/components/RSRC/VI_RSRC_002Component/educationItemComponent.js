import React from 'react';
import classNames from 'classnames';
import color from '../../../assets/styles/material/com/color'
import { Typography } from '../../unit/index'

const educationItemComponent = ({ ...state }) => {

  const {
    classes,
    data,
    handleModal,
    handleDelete,
    ...other
  } = state;

  return (
    <div className={classes.margB30} {...other}>
      <div className={classNames(classes.flexSpaceBetween)}>
        <Typography
          variant={'subtitle1'}
          fontWeight={4}
          className={classes.subTitle}>
          {data.schNm}
        </Typography>
        <Typography
          color={color.skyBlue.default}
          fontWeight={3}
          style={{cursor: 'pointer'}}
          className={classNames(classes.contentSubContant)}>
          <i className="fas fa-edit"
            onClick={handleModal}
            style={{marginRight: '5px'}}/>
          <i className="fas fa-times"
            onClick={handleDelete}/>
        </Typography>
      </div>
      <Typography
        fontWeight={2}
        variant={'body2'}
        className={classes.margB5}>
        {data.study}
      </Typography>
      <Typography
        fontWeight={2}
        variant={'caption'}
        className={classes.margB5}>
        {data.stY}
        {data.endY === '' ? null : '-'}
        {data.endY}
      </Typography>
  </div>
  );
};

export default educationItemComponent;