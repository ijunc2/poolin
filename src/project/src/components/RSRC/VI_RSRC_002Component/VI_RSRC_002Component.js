import React from 'react'
import Workspace from '../../internal/Workspace';
import classNames from 'classnames';
import color from '../../../assets/styles/material/com/color'
import {List, Map} from 'immutable';
import EXPItem from './experienceItemComponent'
import EduItem from './educationItemComponent'
import AddExpModal from './AddExpComponent'
import AddEduModal from './AddEduComponent'
import { Typography } from '../../unit/index'
const newExpData = Map({
  title: '',
  cmpNm: '',
  summary: '',
  stM: '',
  endM: '',
  stY: '',
  endY: '' 
})
const newEduData = Map({
  schNm: '',
  study: '',
  stY: '',
  endY: '', 
})
class VI_RSRC_002Component extends Workspace {
    constructor(props){
        super(props)
        this.state = {
          name: this.props.name,
          expOpen: false,
          eduOpen: false,
          year: '',
          idx: '',
          expDataArr: List([
            Map({
              title: 'haha',
              cmpNm: 'samsung sds',
              summary: '룰루랄라!',
              stM: '',
              endM: '',
              stY: '2003',
              endY: '' 
            }),
            Map({
              title: '로렘입숨',
              cmpNm: 'kakao',
              summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
              stM: '',
              endM: '',
              stY: '2005',
              endY: '2013' 
            })
          ]),
          eduDataArr: List([
            Map({
              schNm: '서울대',
              study: '컴공',
              stY: '2003',
              endY: '' 
            }),
            Map({
              schNm: '고려대',
              study: '컴공',
              stY: '2005',
              endY: '2013' 
            })
          ]),
          //expData, expData는 없으면 안됩니다.
          expData: Map({
            title: '',
            cmpNm: '',
            summary: '',
            stM: '',
            endM: '',
            stY: '',
            endY: '' 
          }),
          eduData: Map({
            schNm: '',
            study: '',
            stY: '',
            endY: '' 
          })
        };
        this.handleChanged = this.handleChanged.bind(this);
        this.handleModal = this.handleModal.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleChanged(e, flg, arr){
      const {name, value} = e.target;
      var obj = {};
      if(flg === 'addData'){
        obj = {[arr]: this.state[arr].set(name, value)};
      }else{
        obj = {[name]: value };
      }
      this.setState(obj);
    }

    handleModal(modalSwitch, modifData, flg, data, idx){
      // modalSwitch는 모달을 껏다 키는 스위치 입니다
      // modifData 수정하는데 도와줄 데이터 입니다.
      // newData 배열 마지막에 추가할 빈값의 데이터 입니다. ex)newExpData newEduData
      // flg가 old이미 이미 있는데 데이터를 불러오고

      var obj = {}
      if(flg === 'old'){
        obj = {[modalSwitch]: !this.state[modalSwitch],
                [modifData]: new Map(data),
                idx: idx};
      }else if(flg === 'new'){
        obj = {[modalSwitch]: !this.state[modalSwitch],
                // [modifData]: modalSwitch === 'expOpen' ? newExpData : newEduData,
                // modifData에 넣을 데이터는 state가 아닌 const로 선언을 해서
                // []이런 형식으로 안되서 삼항연산자를 이용하였습니다.
                [modifData]: modalSwitch === 'expOpen' ? newExpData : newEduData,
                idx: null };
      }else{
        obj = {[modalSwitch]: !this.state[modalSwitch] }
      }
      this.setState(obj);
    }

    handleSave(arr, modalSwitch, modifData){  
      // modifData 배열에 수정이 되어 저장할 데이터 입니다. ex)newExpData newEduData
      // idx가 null이면 새로 추가되는 데이터 입니다.
      var obj = {};
      if(this.state.idx===null){
        obj = { [arr]: this.state[arr].concat( this.state[modifData].toJS()) };
      }else{
        obj = { [arr]: this.state[arr].set(this.state.idx, this.state[modifData]) };
      }
      this.handleModal(modalSwitch);
      this.setState(obj);
    }

    handleDelete(arr,idx){
      var obj = {[arr]: this.state[arr].delete(idx)};
      this.setState(obj)
    }

    render() {
        const { classes } = this.props;

        return (
          <div className={classNames(classes.contentRoot)}>
            <div className={classes.margB40}>
              <div className={classNames(classes.flexSpaceBetween,classes.margB10)}>
                <Typography
                  fontWeight={3}
                  variant="h1"
                  className={classes.contentTitle}>
                  Work Experience
                </Typography>
                <Typography
                  onClick={()=>this.handleModal('expOpen', 'expData', 'new')}
                  color={color.blue.default}
                  fontWeight={3}
                  style={{cursor: 'pointer'}}
                  className={classNames(classes.contentSubContant,classes.margT10)}>
                  <i className="fas fa-plus" style={{marginRight: '5px'}}/>
                    Add Experience
                </Typography>
                </div>
                  {this.state.expDataArr.toJS().map((r,i)=>
                    <EXPItem
                      key={i}
                      data={r}
                      handleModal={()=>this.handleModal('expOpen', 'expData', 'old', r, i)}
                      handleDelete={()=>this.handleDelete('expDataArr', i)}
                      classes={classes}/>
                    )}
              </div>
              <div className={classNames(classes.flexSpaceBetween,classes.margB10)}>
                <Typography
                  fontWeight={3}
                  variant="h1"
                  className={classes.contentTitle}>
                  Education
                </Typography>
                <Typography
                  onClick={()=>this.handleModal('eduOpen', 'eduData', 'new')}
                  color={color.blue.default}
                  fontWeight={3}
                  style={{cursor: 'pointer'}}
                  className={classNames(classes.contentSubContant,classes.margT10)}>
                  <i className="fas fa-plus" style={{marginRight: '5px'}}/>
                    Add Educations
              </Typography>
              </div>
              {this.state.eduDataArr.toJS().map((r, i)=>
                <EduItem
                  key={i}
                  data={r}
                  handleModal={()=>this.handleModal('eduOpen', 'eduData', 'old', r, i)}
                  handleDelete={()=>this.handleDelete('eduDataArr', i)}
                  classes={classes}/>
                )}
                <AddExpModal
                  classes={classes}
                  open={this.state.expOpen}
                  data={this.state.expData.toJS()}
                  handleSave={()=>this.handleSave('expDataArr', 'expOpen', 'expData')}
                  handleModal={()=>this.handleModal('expOpen')}
                  handleChanged={e=>this.handleChanged(e, 'addData', 'expData')}/>
                <AddEduModal
                  classes={classes}
                  open={this.state.eduOpen}
                  data={this.state.eduData.toJS()}
                  handleSave={()=>this.handleSave('eduDataArr', 'eduOpen', 'eduData')}
                  handleModal={()=>this.handleModal('eduOpen')}
                  handleChanged={e=>this.handleChanged(e, 'addData', 'eduData')}/>
            </div>
        )
    }
}

export default VI_RSRC_002Component