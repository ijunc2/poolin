import React from 'react';
import classNames from 'classnames';
import color from '../../../assets/styles/material/com/color'
import { Typography } from '../../unit/index'
const experienceItemComponent = ({ ...state }) => {

  const {
    classes,
    handleModal,
    handleDelete,
    data,
    ...other
  } = state;

  return (
    <div className={classes.margB20} {...other}>
      <div className={classNames(classes.flexSpaceBetween)}>
        <Typography
          variant={'subtitle1'}
          fontWeight={4}
          className={classes.subTitle}>
          {data.title}
        </Typography>
        <Typography
          color={color.skyBlue.default}
          fontWeight={3}
          style={{cursor: 'pointer'}}
          className={classNames(classes.contentSubContant)}>
          <i className="fas fa-edit"
            onClick={handleModal}
            style={{marginRight: '5px'}}/>
          <i className="fas fa-times"
            onClick={handleDelete}/>
        </Typography>
      </div>
      <Typography
        fontWeight={2}
        variant={'body2'}
        className={classes.margB5}>
        {data.cmpNm}
      </Typography>
      <Typography
        fontWeight={2}
        variant={'caption'}
        className={classes.margB5}>
        {data.stM}{data.stY}
        {data.endM === '' && data.endY === '' ? null : '-'}
        {data.endM}{data.endY}
      </Typography>
      <div className={classes.summaryContent}>
      <Typography
        fontWeight={2}
        style={{wordBreak: 'break-all'}}
        variant={'caption'}>
        {data.summary}
      </Typography>
    </div>
  </div>
  );
};

export default experienceItemComponent;