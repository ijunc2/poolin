import React from 'react';
import classNames from 'classnames';
import blue from '@material-ui/core/colors/blue';
import MenuItem from "@material-ui/core/MenuItem";
import { year, month } from '../../../utils/common'
import { SubPopUp, Input, Select, Button } from '../../unit/index'

const AddExpComponent = ({ ...state }) => {

  const {
    classes,
    open,
    handleModal,
    handleChanged,
    handleSave,
    data,
  } = state;

  return (
      <SubPopUp
        title={'Add Work Experience'}
        height={'430px'}
        open={open}
        handleModal={handleModal}>
      <div className={classes.addExpPop}
        style={{height: 'calc(100% - 40px) !important'}}>
        <div>
          <Input 
            placeholder='Job Title (Required)'
            value={data.title}
            name={'title'}
            className={classNames(classes.width250, classes.mrgB10, classes.displayBlock)}
            onChange={handleChanged}
            style={{marginBottom: '10px'}}
            shape={'sm'} 
            color={blue}/>
          <Input 
            placeholder='Company Name (Required)'
            value={data.cmpNm}
            name={'cmpNm'}
            className={classNames(classes.width250, classes.mrgB10, classes.displayBlock)}
            onChange={handleChanged}
            style={{marginBottom: '10px'}}
            shape={'sm'} 
            color={blue}/>
          <Input 
            className={classes.textArea}
            onChange={handleChanged}
            placeholder="Summary"
            value={data.summary}
            name={'summary'}
            multiline
            color={blue}/>
          <div style={{display: 'inline-block'}}>
            <Select
              style={{width: '120px'}}
              value={data.stM}
              onChange={handleChanged}
              className={classes.margB10}
              name={"stM"}
              displayEmpty
              shape={'sm'}>
              <MenuItem value="" disabled>
                  Start Month
              </MenuItem>
              {month().map((r,i) => (
                <MenuItem key={i} value={r.name}>
                    {r.name}
                </MenuItem>
              ))}
            </Select>
            <Select
              style={{width: '120px'}}
              value={data.stY}
              onChange={handleChanged}
              name={"stY"}
              displayEmpty
              shape={'sm'}>
              <MenuItem value="" disabled>
                  Start Year
              </MenuItem>
              {year().map((r,i) => (
                <MenuItem key={i} value={r}>
                    {r}
                </MenuItem>
              ))}
            </Select>            
          </div>
          <div className={classes.splliter}>
            <span>
              -
            </span>
          </div>
          <div style={{display: 'inline-block'}}>
            <Select
              style={{width: '120px'}}
              value={data.endM}
              onChange={handleChanged}
              className={classes.margB10}
              name={"endM"}
              displayEmpty
              shape={'sm'}>
              <MenuItem value="" disabled>
                  End Month
              </MenuItem>
              {month().map((r,i) => (
                <MenuItem key={i} value={r.name}>
                    {r.name}
                </MenuItem>
              ))}
            </Select>
            <Select
              style={{width: '120px'}}
              value={data.endY}
              onChange={handleChanged}
              name={"endY"}
              displayEmpty
              shape={'sm'}>
              <MenuItem value="" disabled>
                  End Year
              </MenuItem>
              {year().map((r,i) => (
                <MenuItem key={i} value={r}>
                    {r}
                </MenuItem>
              ))}
            </Select>            
          </div>
        </div>
        <div style={{display: 'flex', justifyContent: 'flex-end'}}>
          <Button
            style={{marginRight: '10px'}}
            onClick={handleModal}
            bgNone>
            <i className="fas fa-times" style={{marginRight: '5px'}}/>
            Cancel
          </Button>
          <Button
            onClick={handleSave}
            style={{height: '32px'}}
            stepBtn>
            <i className="fas fa-edit" style={{marginRight: '5px'}}/>
            Save Changes
          </Button>
        </div>
      </div>
    </SubPopUp>
  );
};

export default AddExpComponent;