import React from 'react'
import Workspace from '../../internal/Workspace';
import RatingBar from './RatingBar';
import classNames from 'classnames';
import { Typography, Button, Avatar } from '../../unit/index'

class VI_RSRC_008Component extends Workspace {
    constructor(props){
        super(props)
        this.state={
          arr:[
            {title: 'Very Good',
              num: 12,
              percent: 40},
            {title: 'Good',
              num: 0,
              percent: 0},
            {title: 'Neutral',
              num: 0,
              percent: 20},
            {title: 'Poor',
              num: 3,
              percent: 40},
            {title: 'Very Poor',
              num: 0,
              percent: 0}
          ]
        }
    }
    render() {
        const { classes } = this.props;
        return (
          <div className={classNames(classes.contentRoot)}>
            <div className={classes.flexSpaceBetween}>
              <Typography
              fontWeight={3}
              variant="h1"
              className={classes.contentTitle}>
              Team Scorecards
            </Typography>
                <Button
                  btnH24
                  stepBtn>
                  <i className={classNames(classes.margR5,"fas fa-thumbs-up")}/>
                  Request Scorecards
                </Button>
            </div>
            <div style={{display: 'flex', marginBottom: '20px'}}>
              <Avatar
                type={'midbig'}
                color='green'
                style={{marginRight: '16px'}}>
                2
              </Avatar>
              <div>
                <Typography
                  fontWeight={2}
                  variant="subtitle1">
                    Submitted By
                </Typography>
                <Avatar
                  type={'smallmid'}
                  color='red'>
                  1
                </Avatar>
              </div>
            </div>
            <Typography
              fontWeight={2}
              className={classes.contentSubTitle}>
              Overall Rating
            </Typography>
              {this.state.arr.map((r,i)=>
                <RatingBar
                  data={r}
                  key={i}/>)}
            <Typography
              fontWeight={2}
              className={classes.contentSubTitle}>
              언어
            </Typography>
            <div className={classNames(classes.margT10, classes.flexSpaceBetween)}>
              <Typography
                fontWeight={2}
                variant="subtitle2">
                  한국어
              </Typography>
              <Avatar
                type={'sm'}
                color={'yellow'}/>
            </div>
          </div>
        )
    }
}

export default VI_RSRC_008Component