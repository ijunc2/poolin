import React from 'react';
import {color} from '../../../assets/styles/material/com/index'
import { makeStyles } from '@material-ui/styles';
import { Typography } from '../../unit/index'

const RatingBar = ({ ...state }) => {

  const {
    data,
    endItem,
    ...other
  } = state;

  const  useStyles = makeStyles({
    wrap:{
      borderBottom: `2px solid #dedede`,
      padding: '10px 0',
      display: 'flex',
      '&>div:nth-child(1)':{
        width: '25%',
        paddingLeft: '10px'
      },
      '&>div:nth-child(2)':{
        width: '5%',
      },
      '&>div:nth-child(3)':{
        width: '60%',
        padding: '0 10px',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
      },
      '&>div:nth-child(4)':{
        width: '10%',
      },
    },
    percentBar:{
      background: color.green.onGreen,
      height: '8px',
      width: data.percent ? `${data.percent}%` : 0,
      borderRadius: '4px',
    }
  });
  const classes = useStyles();
  return (
    <div className={classes.wrap} {...other}>
      <div>
        <Typography
          fontWeight={2}
          variant="subtitle2">
            {data.title}
        </Typography>
      </div>
      <div>
        <Typography
          fontWeight={2}
          variant="subtitle2">
            {data.num}
        </Typography>
      </div>
      <div>
        <div className={classes.percentBar}/>
      </div>
      <div>
        <Typography
          fontWeight={2}
          variant="subtitle2">
            {`${data.percent}%`}
        </Typography>
      </div>
    </div>
  );
};

export default RatingBar;