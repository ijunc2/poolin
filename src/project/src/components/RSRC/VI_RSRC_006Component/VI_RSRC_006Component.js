import React from 'react'
import Workspace from '../../internal/Workspace';
import classNames from 'classnames';
import blue from '@material-ui/core/colors/blue';
import ListM from '@material-ui/core/List';
import TimeLineItem from './TimeLineItem';
import { List } from 'immutable';
import { Typography, Input, Button } from '../../unit/index'

class VI_RSRC_006Component extends Workspace {
    constructor(props){
        super(props)
        this.state={
          arr: List([
            { text1: 'GunKim1',
              text2: 'updated',
              text3: 'Ethan',
              text4: 'Applied',
              text5: 'Feedback',
              date: new Date()},
            { text1: 'GunKim',
              text2: 'updated',
              text3: 'Ethan',
              text4: 'Applied',
              text5: 'Feedback',
              date: new Date()},
            { text1: 'GunKim',
              text2: 'updated',
              text3: 'Ethan',
              text4: 'Applied',
              text5: 'Feedback',
              date: new Date()},
          ])
        }
        this.handleChanged = this.handleChanged.bind(this);
    }

    handleChanged(e){
      const {name, value} = e.target;
      var obj = {
          [name]: value
      }
      this.setState(obj)
    }

    
    render() {
        const { classes } = this.props;
        const placeHolder = 
        'Share a message with Your team here.\n You can use @-mentions to notufy individual team members.\n * Remember not to include sensitive information'  

        return (
            <div className={classNames(classes.contentRoot)}>
            <Typography
              fontWeight={3}
              variant="h1"
              className={classes.contentTitle}>
              Team Discussion
            </Typography>
            <Input 
              className={classNames(classes.textArea, classes.teamDiscTextArea)}
              onChange={e=>this.handleChanged(e)}
              placeholder={placeHolder}
              value={this.state.textArea}
              name={'textArea'}
              multiline
              color={blue}/>
               <div style={{display: 'flex', justifyContent: 'flex-end'}}>
                <Button
                  onClick={()=>this.handleEdit()}
                  style={{marginRight: '10px'}}
                  btnH24
                  bgNone>
                  <i className={classNames(classes.margR5,"fas fa-cloud-download-alt")}/>
                  Download
                </Button>
                <Button
                  btnH24
                  stepBtn>
                  <i className={classNames(classes.margR5,"fas fa-cloud-download-alt")}/>
                  Update Resume
                </Button>
              </div>
              <ListM >
                {this.state.arr.toJS().map((r, i)=>
                  <TimeLineItem
                    endItem={i === this.state.arr.size - 1}
                    key={i}
                    data={r}/>
                  )}
              </ListM>     
            </div>
        )
    }
}

export default VI_RSRC_006Component