import React from 'react';
import classNames from 'classnames';
import ListItem from '@material-ui/core/ListItem'
import { makeStyles } from '@material-ui/styles';
import { Typography } from '../../unit/index'
import moment from 'moment'

const TimeLineItem = ({ ...state }) => {

  const {
    data,
    key,
    endItem,
    ...other
  } = state;

  const  useStyles = makeStyles({
    timeLine:{
      width: '100%',
      display: 'flex',
      position: 'relative',
      padding: '10px 0 10px 10px !important',
    },
    line:{
      position: 'absolute',
      width: '3px',
      top: '20px',
      bottom: '0',
      background: '#dedede',
      left: '16.5px',
      height: '40px',
    },
    circle:{
      color: '#ffa955',
      border: '2px solid #dedede',
      borderRadius: '50%',
      fontSize: '13px',
      marginRight: '10px',
      zIndex: 1,
    },
    text:{
      fontSize: '14px',
      fontWeight: '600',
    },
    smallText:{
      margin: '4px 5px 0 5px',
      fontSize: '12px',
      fontWeight: '200',
    },
    textWrap:{
      color: '#84929f',
      margin: '4px 0',
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between'
    },
    date:{
      color: '#84929f',
      textTransform: 'lowercase',
      fontSize: '10px',
    },
  });
  const classes = useStyles();
  return (
    <ListItem 
      className={classes.timeLine}
      {...other}
      key={key}>
        {endItem ? null : <div className={classes.line}/>}
      <i className={classNames("fas fa-circle",classes.circle)}/>
      <div className={classes.textWrap}>
        <div className={classes.text}>
            {data.text1}
          <span
            className={classes.smallText}>
            {data.text2}
          </span>
          {data.text3}
          <span
            className={classes.smallText}>
            {data.text4}
          </span>
            <i className="fas fa-arrow-right"/>
          <span
            className={classes.smallText}>
            {data.text5}
          </span>
        </div>
        <Typography
          className={classes.date}
          fontWeight={2}>
          {moment(data.date, "YYYYMMDD").fromNow()}
        </Typography>
      </div>
    </ListItem>
  );
};

export default TimeLineItem;