import React from 'react'
import Workspace from '../../internal/Workspace';
import ListM from '@material-ui/core/List';
import AttachItem from './AttachItem';
import classNames from 'classnames';
import {List} from 'immutable';
import { Typography, Button } from '../../unit/index'

class VI_RSRC_004Component extends Workspace {
    constructor(props){
        super(props)
        this.state={
          arr: List([
            { title: 'Resume1.pdf',
              volume: '12mb',
              owner: 'kim',
              ago: new Date()},
            { title: 'Resume2.pdf',
              volume: '',
              owner: 'park',
              ago: new Date()},
            { title: 'Resume3.pdf',
              volume: '12mb',
              owner: 'choi',
              ago: new Date()},
          ])
        }
    }

    handleDelete(idx){
      this.setState({arr: this.state.arr.delete(idx)})
    }

    render() {
        const { classes } = this.props;
        return (
          <div className={classNames(classes.contentRoot)}>
            <div className={classes.flexSpaceBetween}>
              <Typography
                fontWeight={3}
                variant="h1"
                className={classes.contentTitle}>
                Attachments
              </Typography>
                <Button
                  btnH24
                  stepBtn>
                  <i className={classNames(classes.margR5,"fas fa-cloud-download-alt")}/>
                  Upload Attachments
                </Button>
            </div>
            {this.state.arr.toJS().length===0 ?
                <div className={classes.emptyWrap}>
                  <i className={classNames("fas fa-paperclip", classes.emptyIcon)}/>
                  <Typography
                    fontWeight={0}
                    variant="subtitle2"
                    color={'#84929f'}
                    className={classes.margT10}>
                    No attachments have been added.
                  </Typography>
                </div>
                :
                <ListM>
                  {this.state.arr.toJS().map((r, i)=>
                    <AttachItem
                      key={i}
                      data={r}
                      handleDelete={()=>this.handleDelete(i)}
                      />
                    )}
                </ListM>
                }
          </div>
        )
    }
}

export default VI_RSRC_004Component