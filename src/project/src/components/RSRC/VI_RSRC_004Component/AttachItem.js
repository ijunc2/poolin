import React from 'react';
import classNames from 'classnames';
import ListItem from '@material-ui/core/ListItem'
import moment from 'moment'
import { makeStyles } from '@material-ui/styles';
import { Typography } from '../../unit/index'

const AttachItem = ({ ...state }) => {

  const {
    data,
    endItem,
    handleDelete,
    ...other
  } = state;

  const  useStyles = makeStyles({
    AttachItem:{
      width: '100%',
      padding: '10px !important',
      cursor: 'pointer',
      flexDirection: 'column',
      borderRadius: '4px',
      '&:hover':{
         backgroundColor: '#dedede',
         transition: 'all ease .3s 0s',
      }
    },
    icon:{
      fontSize: '20px',
      color: '#4c555c',
      marginRight: '5px'
    },
    titleText:{
      fontSize: '15px',
      display: 'flex',
      fontWeight: '600 !important',
    },
    volumeText:{
      fontSize: '12px',
      fontWeight: '600 !important',
      color: '#84929f !important',
      margin: '4px 0 0 10px',
    },
    textWrap:{
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between'
    },
    bottomTextWrap:{
      display: 'flex',
      width: '100%',
    },
    bottomText:{
      fontSize: '10px !important',
    },
    dateText:{
      marginLeft: '5px',
      color: '#84929f !important',
    }
  });
  const classes = useStyles();
  return (
    <ListItem 
      className={classes.AttachItem}
      {...other}
      key={1}>
      <div className={classes.textWrap}>
        <div style={{display: 'flex'}}>
          <Typography
            className={classes.titleText}>
            <i className={classNames("fas fa-file-pdf", classes.icon)}/>
              {data.title}
          </Typography>
          <Typography
            className={classes.volumeText}>
              {data.volume}
          </Typography>
        </div>
        <Typography
          style={{cursor: 'pointer'}}
          className={classes.bottomText}
          fontWeight={2}
          onClick={handleDelete}>
          {'Remove'}
          <i className="fas fa-times" style={{marginLeft: '2px'}}/>
        </Typography>
      </div>
      <div className={classes.bottomTextWrap}>
      <Typography
        className={classes.bottomText}>
        Added by: {data.owner}
      </Typography>
      <Typography
        className={classNames(classes.bottomText, classes.dateText)}>
        {moment(data.ago, "YYYYMMDD").fromNow()}
      </Typography>
      </div>
  </ListItem>
  );
};

export default AttachItem;