import React from 'react'
import Workspace from '../internal/Workspace';
import classNames from 'classnames';
import color from '../../assets/styles/material/com/color'
import Avatar from "@material-ui/core/Avatar";
import blue from '@material-ui/core/colors/blue';

import { Typography, Button,Input } from '../unit/index'

class VI_RSRC_001Component extends Workspace {
    constructor(props){
        super(props)
        this.state={
            edit: false,
            name: this.props.name,
            address: '울산 서울',
            email: 'fsadfa@gmail.com',
            phnumber: '010-1234-1234',
            summary: '잘하노'
        }
        this.handleChanged = this.handleChanged.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
    }

    handleChanged(e){
      const {name, value} = e.target;
      var obj = {
          [name]: value
      }
      this.setState(obj)
    }

    handleEdit(){
      this.setState({
        edit: !this.state.edit
      })
    }

    render() {
        const { classes } = this.props;
        const subTitle__Component = (text)=>
          <Typography
            fontWeight={4}
            className={classes.contentSubTitle}>
            {text}
          </Typography>;
        const subContant__Component = (text,weigth)=>
          <Typography
            fontWeight={weigth}
            className={classes.contentSubContant}>
            {text}
          </Typography>;
        const linkContant__Component = (text,weigth)=>
          <Typography
            color={color.skyBlue.default}
            fontWeight={weigth}
            style={{cursor: 'pointer'}}
            className={classes.contentSubContant}>
            {text}
          </Typography>;
        return (
            <div className={classNames(classes.contentRoot)}>
              {!this.state.edit ? 
                <>
                  <Typography
                    fontWeight={3}
                    variant="h1"
                    className={classes.contentTitle}>
                    Candidate Details
                  </Typography>
                  <div style={{display: 'flex'}}>
                  <Avatar className={classes.avatar.md}>
                    {String(this.state.name).substring(0, 1).toUpperCase()}
                  </Avatar>
                  <Typography
                    fontWeight={4}
                    variant="h1"
                    className={classes.avatarText}>
                    {String(this.state.name).substring(0, 1).toUpperCase()}
                    {String(this.state.name).substring(1, this.state.name.length)}
                  </Typography>
                  </div>
                  {subTitle__Component('Contact Points')}
                  <div style={{display: 'flex'}}>
                    <div style={{width: '100px'}}>
                      {subContant__Component('Address',4)}
                      {subContant__Component('Email',4)}
                      {subContant__Component('Phone',4)}
                    </div>
                    <div>
                      {subContant__Component(this.state.address, 3)}
                      {linkContant__Component(this.state.email, 3)}
                      {linkContant__Component(this.state.phnumber, 3)}
                    </div>
                  </div>
                  {subTitle__Component('Summary')}
                  {subContant__Component(this.state.summary, 3)}
                  <Typography
                    onClick={()=>this.handleEdit()}
                    color={color.skyBlue.default}
                    fontWeight={3}
                    style={{cursor: 'pointer'}}
                    className={classNames(classes.contentSubContant,classes.margT30,classes.margB10)}>
                    <i className="fas fa-edit" style={{marginRight: '5px'}}/>
                    Edit Candidate Details
                  </Typography>
                </>
                :
                  <div className={classes.flexColSpaceBetween}>
                    <div>
                      <Typography
                      fontWeight={3}
                      variant="h1"
                      className={classes.contentTitle}>
                      Candidate Details
                    </Typography>
                    <div style={{display: 'flex'}}>
                    <Avatar className={classes.avatar.md}>
                      {String(this.state.name).substring(0, 1).toUpperCase()}
                    </Avatar>
                    <Typography
                      color={color.skyBlue.default}
                      fontWeight={3}
                      style={{cursor: 'pointer'}}
                      className={classNames(classes.contentSubContant,classes.avatarAddText)}>
                      <i className="fas fa-plus" style={{marginRight: '5px'}}/>
                      Upload Profile Photo
                    </Typography>
                    </div>
                    <Input
                      autoComplete={'off'}
                      placeholder='Full Name (Required)'
                      value={this.state.name}
                      className={classNames(classes.margT10,classes.margB10,classes.nameInput)}
                      onChange={e=>this.handleChanged(e)}
                      name={'name'}
                      shape={'sm'}
                      color={blue}/>
                    <Input 
                      className={classes.textArea}
                      onChange={e=>this.handleChanged(e)}
                      placeholder="Mailing Address"
                      value={this.state.address}
                      name={'address'}
                      multiline
                      color={blue}/>
                    <Input
                      autoComplete={'off'}
                      placeholder='Email Address'
                      value={this.state.email}
                      className={classNames(classes.margB10,classes.subInput)}
                      onChange={e=>this.handleChanged(e)}
                      name={'email'}
                      shape={'sm'}
                      color={blue}/>
                    <Input
                      autoComplete={'off'}
                      placeholder='Phone Number'
                      value={this.state.phnumber}
                      className={classNames(classes.margB10,classes.subInput)}
                      onChange={e=>this.handleChanged(e)}
                      name={'phnumber'}
                      shape={'sm'}
                      color={blue}/>
                    <Input 
                      className={classNames(classes.margB10,classes.textArea)}
                      onChange={e=>this.handleChanged(e)}
                      placeholder="Summary"
                      value={this.state.summary}
                      name={'summary'}
                      multiline
                      color={blue}/>
                    </div>
                    <div style={{display: 'flex'}}>
                      <Button
                        onClick={()=>this.handleEdit()}
                        style={{marginRight: '10px'}}
                        stepBtn>
                        <i className={classNames(classes.margR5,"fas fa-save")}/>
                        Save Changes
                      </Button>
                      <Button
                        bgNone>
                        <i className={classNames(classes.margR5,"fas fa-times")}/>
                        Cancel
                      </Button>
                    </div>
                  </div>}
            </div>
        )
    }
}

export default VI_RSRC_001Component