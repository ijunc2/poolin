import React from 'react'
import Workspace from '../../internal/Workspace';
import classNames from 'classnames';
import MenuItem from "@material-ui/core/MenuItem";
import ListM from "@material-ui/core/List";
import TaskItem from './TaskItem';
import { List } from 'immutable';
import { Button, Select, Typography, ScheduleModal } from '../../unit/index'

  const titleArr = ['My Tasks', 'Created Tasks', 'Team Tasks'];
  const autoCardData = [
    {name: 'ZZanggu', email: 'asd@saf.com', color: 'cyan'},
    {name: 'huni', email: 'asd@saf.com', color: 'blue'},
    {name: 'chulsu', email: 'asd@saf.com', color: 'yellow'},
    {name: 'mengu', email: 'asd@saf.com', color: 'red'},
    {name: 'lala', email: 'asd@saf.com', color: 'cyan'},
    {name: 'jojo', email: 'asd@saf.com', color: 'blue'},
    {name: 'hoho', email: 'asd@saf.com', color: 'yellow'},
    {name: 'zaza', email: 'asd@saf.com', color: 'red'},
    {name: 'qqqq', email: 'asd@saf.com', color: 'red'},
    {name: 'eeee', email: 'asd@saf.com', color: 'cyan'},
    {name: 'rrrrr', email: 'asd@saf.com', color: 'blue'},
    {name: 'ttttt', email: 'asd@saf.com', color: 'yellow'},
    {name: 'yyyyy', email: 'asd@saf.com', color: 'red'}
  ]
  const clear ={
    start: new Date(),
    end: new Date(),
    worldtime: '',
    time: '',
    duration: '',
    interViewers: new List([]),
    title: '',
    location: '',
    desc: '',
    task: 'My Tasks',
    check: false,
    };
  
class VI_RSRC_010Component extends Workspace {
    constructor(props){
        super(props)
        this.state={
          arr: List([]),
          open: false,
          //////추가할 때 필요한 state,
          selectArr: ['haha', 'hoho', 'lolo', 'lala'],
          date: new Date(),
          worldtime: '',
          time: '',
          duration: '',
          interViewers: new List([]),
          title: '',
          location: '',
          task: 'My Tasks',
          desc: '',
        }
        this.handleModal=this.handleModal.bind(this);
        this.handleDatePickerChange=this.handleDatePickerChange.bind(this);
        this.handleChanged=this.handleChanged.bind(this);
        this.handleAddSchedule=this.handleAddSchedule.bind(this);
        this.handleAutoCompleteDelete=this.handleAutoCompleteDelete.bind(this);
        this.handleModal=this.handleModal.bind(this);
    }
    
    handleAutoCompleteDelete(idx){
      this.setState({
        interViewers: this.state.interViewers.delete(idx),
      })
    }

    handleAutoCompleteAdd(value) {
      this.setState({
        interViewers: this.state.interViewers.concat(value),
      });
    } 

    handleChanged(e, key, flg){
      const {name, value, checked} = e.target;
      var obj = {};
      if(flg === 'check')
        obj={
          arr: this.state.arr.setIn([key, 'check'], checked)
        }
      else
        obj={[name]: value}
      this.setState(obj)
    }

    handleModal(){
      this.setState({open: !this.state.open})
    }

    handleDatePickerChange(date) {
      this.setState({
        date: date === undefined || date === '' ? new Date() : date
      })
    }

    handleAddSchedule(){
      var obj={}
      obj = {
        arr: this.state.arr.push({
          start: this.state.date,
          end: this.state.date,
          worldtime: '',
          time: this.state.time,
          duration: this.state.duration,
          interViewers: new List([]),
          title: this.state.title,
          location: this.state.location,
          desc: this.state.desc,
          task: this.state.task,
          check: false
        })
      }
      this.setState(obj);
      this.handleModal();
      this.setState(clear)
    }

    render() {
        const { classes } = this.props;
        return (
          <div className={classNames(classes.contentRoot)}>
            <div className={classes.flexSpaceBetween}>
              <Select
                value={this.state.task}
                onChange={e => this.handleChanged(e)}
                name={"task"}
                className={classes.tasksTitle}>
                  {titleArr.map((r, i)=>
                    <MenuItem key={i} value={r}>
                        {r}
                    </MenuItem>
                    )}
                </Select>
                <Button
                  btnH24
                  stepBtn
                  onClick={()=>this.handleModal()}>
                  <i className={classNames(classes.margR5,"fas fa-plus")}/>
                  Add Task
                </Button>
              </div>
              {this.state.arr.toJS().length===0 ?
                <div className={classes.emptyWrap}>
                  <i className={classNames("fas fa-check-square", classes.emptyIcon)}/>
                  <Typography
                    fontWeight={0}
                    variant="subtitle2"
                    color={'#84929f'}
                    className={classes.margT10}>
                    No outstanding tasks for Gun Kim
                  </Typography>
                  {/* Gun Kim은 사용자 이름을 어떤 방식으로 불러 올지 몰라서 임시로 해둔 것 입니다. */}
                </div>
                :
                <ListM>
                  {this.state.arr.toJS().filter(m => m.task === this.state.task).map((r, i)=>
                    <TaskItem
                      key={i}
                      data={r}
                      handleChanged={e=>this.handleChanged(e, i, 'check')}/>
                    )}
                </ListM>
                }
              <ScheduleModal
                open={this.state.open}
                selectArr={this.state.selectArr}
                worldtime={this.state.worldtime}
                time={this.state.time}
                duration={this.state.duration}
                title={this.state.title}
                location={this.state.location}
                desc={this.state.desc}
                date={this.state.date}
                handleModal={()=>this.handleModal()}
                handleDatePickerChange={this.handleDatePickerChange}
                handleChange={e=>this.handleChanged(e)}
                handleAddSchedule={()=>this.handleAddSchedule()}
                //////////////////////////////////////////////
                autoCardData={autoCardData}
                interViewers={this.state.interViewers.toJS()}
                valueName={'name'}
                handleAutoCompleteAdd={value => this.handleAutoCompleteAdd(value)}
                handleAutoCompleteDelete={idx => this.handleAutoCompleteDelete(idx)}
               />
          </div>
        )
    }
}

export default VI_RSRC_010Component