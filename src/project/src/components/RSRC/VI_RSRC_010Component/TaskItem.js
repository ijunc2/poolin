import React from 'react';
import classNames from 'classnames';
import ListItem from '@material-ui/core/ListItem'
import { makeStyles } from '@material-ui/styles';
import { Typography, Checkbox } from '../../unit/index'

const TaskItem = ({ ...state }) => {

  const {
    data,
    handleChanged,
    endItem,
    ...other
  } = state;

  const  useStyles = makeStyles({
    TaskItem:{
      width: '100%',
      padding: '12px 12px 12px 4px !important',
      borderRadius: '4px',
      border: '1px solid #dedede',
      marginBottom: '8px',
      '&:hover':{
         backgroundColor: '#dedede',
         transition: 'all ease .3s 0s',
      }
    },
    margR5:{
      marginRight: '5px'
    },
    checkBox: {
      margin: '0 8px auto 4px !important',
    },
    titleText:{
      fontSize: '15px',
      fontWeight: '600 !important',
    },
    textWrap: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      minHeight: '45px',
      width: '100%',
    },
    bottomTextWrap:{
      display: 'flex',
      width: '100%',
    },
  });
  const classes = useStyles();
  return (
    <ListItem 
      className={classes.TaskItem} {...other}>
        <Checkbox
          checked={data.check}
          onChange={handleChanged}
          className={classes.checkBox}/>
      <div className={classes.textWrap}>
        <Typography
          className={classes.titleText}>
          {data.title}
        </Typography>
        <div className={classes.bottomTextWrap}>
          <Typography
            variant='caption'
            color={'#84929f'}
            className={classes.margR5}>
              <i className={classNames(classes.margR5, "far fa-clock")}/>
              Due 05/30/2019
          </Typography>
          <Typography
            variant='caption'
            color={'#84929f'}>
              <i className={classNames(classes.margR5, "fas fa-user-alt")}/>
              You
          </Typography>
        </div>
      </div>
  </ListItem>
  );
};

export default TaskItem;