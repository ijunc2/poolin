import React from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import Avatar from '@material-ui/core/Avatar';
import classNames from 'classnames';
import { Button, Typography } from '../unit/index'

const VI_RSRC_ModalHeaderCom = ({...state}) => {
  const {classes, name, favorites, handleFavorite}= state;
  return (
    <DialogTitle disableTypography className={classes.title}>
      <div style={{display: 'flex'}}>
        <Avatar className={classes.avatar}>
          {String(name).substring(0, 1).toUpperCase()}
        </Avatar>
        <div className={classes.titleTextWrap}>
          <Typography color={'#fff'} fontWeight={2} variant="h5" className={classes.margB6}>
            {name} 
            <i className={classNames("fas fa-star",
              classes.titleStar, favorites ? classes.lightStar : null)}
              onClick={handleFavorite}/>
          </Typography>
          <div style={{display: 'flex'}} className={classes.margB6}>
            <Typography color={'#ddd'} fontWeight={2} variant="caption"
              className={classNames(classes.margR10, classes.margB5)}>
              <i className={classNames("fas fa-phone",classes.margR5)}/>
                ffff
            </Typography>
            <Typography color={'#ddd'} fontWeight={2} variant="caption" className={classes.margR10}>
              <i className={classNames("fas fa-envelope",classes.margR5)}/>
                rrr
            </Typography>
          </div>
          <Typography color={'#ddd'} fontWeight={2} variant="caption" className={classes.margB6} style={{display: 'flex'}}>
            {`Position: `}
              <Typography color={'#ddd'} fontWeight={4} variant="caption"
                className={classNames(classes.margL5,classes.margR5)}>
              {`Info desk`}
            </Typography>
            {`Added by:`}
            <Typography color={'#ddd'} fontWeight={4} variant="caption" className={classes.margL5}>
              {`Gun kim`}
            </Typography> 
          </Typography>
          <Typography color={'#ddd'} fontWeight={2} variant="caption" style={{marginTop: '15px'}}>
            <i className={classNames("fas fa-plus",classes.margR5)}/>
              Add tag
          </Typography> 
        </div>
      </div>
        <Button grayBtn type={'sm'} className={classes.scorecardBtn}>
          <Typography fontWeight={3} variant="caption" >
            <i className="fas fa-folder-open"/>
            Your Scorecard
          </Typography> 
        </Button>
    </DialogTitle>
  );
};

export default VI_RSRC_ModalHeaderCom;