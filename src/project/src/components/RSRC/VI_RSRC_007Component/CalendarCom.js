import React from 'react';
import { Calendar, momentLocalizer } from 'react-big-calendar'
import './react-big-calendar.css';
import moment from 'moment'

const localizer = momentLocalizer(moment)

const CalendarCom = props => (
  <>
    <Calendar
      onSelectEvent={props.handleEvent}
      localizer={localizer}
      events={props.data}
      startAccessor="start"
      endAccessor="end"
      step={60}
      popup={true}/>
  </>
)
export default CalendarCom;
