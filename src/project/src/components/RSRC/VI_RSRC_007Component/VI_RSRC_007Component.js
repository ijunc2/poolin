import React from 'react'
import Workspace from '../../internal/Workspace';
import classNames from 'classnames';
import Calendar from './CalendarCom';
import CalendarModal from './CalendarModal';
import { List } from 'immutable';
import { arrData } from './dump'
import { Typography, Button, ScheduleModal } from '../../unit/index'

class VI_RSRC_007Component extends Workspace {
    constructor(props){
        super(props)
        this.state={
          ScheduleAddOpen: false,
          CalendarItemOpen: false,
          arr: List([
          {
            title: 'lalalala',
            start: new Date(new Date().setHours(new Date().getHours() - 3)),
            end: new Date(new Date().setHours(new Date().getHours() - 3)),
            interViewers: [
                      {name: 'Ethan', email: 'asd@saf.com', color: 'red'},
                      {name: 'Ethan', email: 'asd@saf.com', color: 'blue'},
                      {name: 'Ethan', email: 'asd@saf.com', color: 'yellow'},
                      {name: 'Ethan', email: 'asd@saf.com', color: 'cyan'}
                    ],
            check: true,
            desc: false
          },
          {
            title: 'lololo',
            start: new Date(new Date().setHours(new Date().getHours() - 3)),
            end: new Date(new Date().setHours(new Date().getHours() - 3)),
            interViewers: [
                    {name: 'Ethan', email: 'asd@saf.com', color: 'red'},
                    {name: 'Ethan', email: 'asd@saf.com', color: 'blue'},
                    {name: 'Ethan', email: 'asd@saf.com', color: 'yellow'}
                    ],
            check: true,
            desc: false
          },  
          {
            title: 'lololo',
            start: new Date(new Date().setHours(new Date().getHours() - 3)),
            end: new Date(new Date().setHours(new Date().getHours() - 3)),
            interViewers: [
                      {name: 'Ethan', email: 'asd@saf.com', color: 'yellow'}
                    ],
            check: true,
            desc: false
          },
        ]),
        calendarItem: null,
        selectArr: ['haha', 'hoho', 'lolo', 'lala'],
        worldtime: '',
        time: '',
        duration: '',
        interViewer: '',
        title: `${this.props.name} Meeting`,
        location: '',
        desc: '',
        interViewers: new List([]),
        date: new Date(),
        }
        this.handleAutoCompleteDelete=this.handleAutoCompleteDelete.bind(this);
        this.handleModal=this.handleModal.bind(this);
        this.handleDatePickerChange=this.handleDatePickerChange.bind(this);
        this.handleChange=this.handleChange.bind(this);
        this.handleAddSchedule=this.handleAddSchedule.bind(this);
        this.handleClickCalendar=this.handleClickCalendar.bind(this);
    }

    handleModal(name){
      this.setState({[name]: !this.state[name]})
    }

    handleChange(e){
      const {name, value} = e.target;
      this.setState({
        [name]: value
      })
    }

    handleDatePickerChange(date) {
      this.setState({
        date: date === undefined || date === '' ? new Date() : date
      })
    }

    handleAutoCompleteDelete(idx){
      this.setState({
        interViewers: this.state.interViewers.delete(idx),
      })
    }

    handleAutoCompleteAdd(value) {
      this.setState({
        interViewers: this.state.interViewers.concat(value),
      });
    } 

    handleAddSchedule(){
      var obj={}
      obj = {
        arr: this.state.arr.concat({
          worldtime: this.state.worldtime,
          start: this.state.date,
          end: this.state.date,
          time: this.state.time,
          duration: this.state.duration,
          interViewer: this.state.interViewer,
          title: this.state.title,
          location: this.state.location,
          interViewers: this.state.interViewers,
          desc: this.state.desc,
          date: this.state.date,
        })
      }
      this.setState(obj);
      this.handleModal('ScheduleAddOpen');
      this.setState({
          worldtime: '',
          time: '',
          duration: '',
          interViewer: '',
          title: `${this.props.name} Meeting`,
          location: '',
          interViewers: new List([]),
          desc: '',
          date: new Date(),
      })
    }

    handleClickCalendar(e){
      this.handleModal('CalendarItemOpen');
      this.setState({calendarItem: e});
    }

    render() {
        const { classes } = this.props;
        return (
          <div className={classNames(classes.contentRoot)}>
            <div className={classNames(classes.flexSpaceBetween, classes.margB10)}>
              <Typography
                fontWeight={3}
                variant="h1"
                className={classes.contentTitle}>
                Calendar
            </Typography>
                <Button
                  btnH24
                  stepBtn
                  onClick={()=>this.handleModal('ScheduleAddOpen')}>
                  <i className={classNames(classes.margR5,"fas fa-calendar-alt")}/>
                  Schedule Meeting
                </Button>
            </div>
            <Calendar
              data={this.state.arr.toJS()}
              handleEvent={e=>this.handleClickCalendar(e)}/>
            <ScheduleModal
               open={this.state.ScheduleAddOpen}
               selectArr={this.state.selectArr}
               worldtime={this.state.worldtime}
               time={this.state.time}
               duration={this.state.duration}
               interViewer={this.state.interViewer}
               title={this.state.title}
               location={this.state.location}
               desc={this.state.desc}
               date={this.state.date}
               handleModal={()=>this.handleModal('ScheduleAddOpen')}
               handleDatePickerChange={this.handleDatePickerChange}
               handleChange={e=>this.handleChange(e)}
               handleAddSchedule={()=>this.handleAddSchedule()}
               //////자동완성 맴버추가를 위한 데이터////////
               autoCardData={arrData}
               interViewers={this.state.interViewers.toJS()}
               valueName={'name'}
               handleAutoCompleteAdd={value => this.handleAutoCompleteAdd(value)}
               handleAutoCompleteDelete={idx => this.handleAutoCompleteDelete(idx)}/>
            <CalendarModal
              classes={classes}
              open={this.state.CalendarItemOpen}
              name={this.props.name}
              data={this.state.calendarItem}
              handleModal={e=>this.handleModal('CalendarItemOpen')}/>
          </div>
        )
    }
}

export default VI_RSRC_007Component