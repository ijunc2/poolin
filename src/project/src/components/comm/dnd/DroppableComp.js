import React from 'react';
import { Droppable } from 'react-beautiful-dnd';

const DroppableComp = ({...state}) => {

  return (
    <Droppable droppableId={state.pipe}>
        {(provided, snapshot) => (
            <div
                ref={provided.innerRef}
                style={state.getListStyle(snapshot.isDraggingOver)}
                className={state.style}>

                {state.children}

                {provided.placeholder}
            </div>
        )}
    </Droppable>
  );
}

export default DroppableComp;