import React from 'react';
import { Draggable } from 'react-beautiful-dnd';

const DraggableComp = ({...state}) => {
    const {...other} = state
  return (
    <Draggable
        draggableId={state.draggableId}
        index={state.index}
        {...other}>
        {(provided, snapshot) => (
            <div
                ref={provided.innerRef}
                {...provided.draggableProps}
                {...provided.dragHandleProps}
                style={state.getItemStyle(
                    snapshot.isDragging,
                    provided.draggableProps.style
                )}
                className={state.style}>

                {state.children}
            </div>
        )}
    </Draggable>
  );
}

export default DraggableComp;