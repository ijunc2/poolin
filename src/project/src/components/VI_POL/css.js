
const css = theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        height: 'calc(100% - 64px)',
        '& > hr': {
            margin: ` 24px - 24px`
        },
        '& > div:nth-child(n+2)': {
            display: 'flex',
            overflow: 'auto',
            margin: `0 - 24px`,
            paddingLeft:  16,
            '& > div:nth-child(n+2)': {
                marginLeft:  16,
            },
            '& > div:nth-last-child(1)': {
                paddingRight:  16,
            },
        }
    },
    rct__header: {
        '& > div:nth-child(1)': {
            display: 'flex',
            '& > div:nth-child(2)': {
                display: 'flex',
                alignItems: 'center',
                marginLeft:  16,
                '& > button': {
                    padding:  4,
                },
                '& > button:nth-last-child(1)': {
                    marginLeft:  8,
                }
            }
        }
        
    },
    pipelineWarp: {
        position: 'relative',
    },
    pipe: {
        height: 'calc(100% - 48px)',
        overflow: 'scroll',
    },
    pipe_header: {
        marginBottom:  8,
        display: 'flex',
        justifyContent: 'space-between',
        '& > h3 span': {
            color: '#009688',
        },
        '& button': {
            padding: 0,
        },
    },

    card: {
        display: 'flex',
    },
    avatar: {
        width: '32px',
        height: '32px',
        fontSize: '1.0rem',
    },
    draggable: {
        '&:hover': {
            background: 'rgba(255, 255, 255, 0.5) !important'
        }
    },
    card_content: {
        paddingLeft:  16,
        flexGrow: 1,
        '& > h6': {
            lineHeight: '0.5',
        }
    },
    card_bottom: {
        display: 'flex',
        justifyContent: 'space-between',
        '& > div:nth-child(2)': {
            textAlign: 'right',
            '& > *': {
                color: 'rgba(0, 0, 0, 0.47)',
                lineHeight: '1.2'
            },
            '& >:nth-child(1) > span': {
                fontWeight: 'bold',
                color: 'rgba(0, 0, 0, 0.87)',
            }
        }
    },
    card_bottom_coin_warp: {
        fontSize: 11,
        display: 'flex',
        paddingTop:  8,
        '& > div:nth-child(1)': {
            width: 14,
            height: 14,
            fontSize: 10,
        },
        '& > p': {
            margin: ` 2px 0 0  2px`,
            color: '#009688',
        }
    },
});

export default css;