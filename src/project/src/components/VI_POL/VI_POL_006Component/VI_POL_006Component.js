import React from 'react'
import Workspace from '../../internal/Workspace';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/styles';
import classNames from 'classnames';
import { List, Map } from 'immutable';
import { Calendar, momentLocalizer } from 'react-big-calendar'
import './react-big-calendar.css';
import moment from 'moment'
import { CalendarModal, Typography, Button, OpenCheckBox, Avatar, Checkbox, ScheduleModal, ScoreCardModal } from '../../unit/index'

const localizer = momentLocalizer(moment)

const clear = {
  worldtime: '',
  time: '',
  duration: '',
  title: ``,
  location: '',
  desc: '',
  interViewers: new List([]),
  date: new Date(),
}

const autoCardData = [
  {name: 'ZZanggu', email: 'asd@saf.com', color: 'cyan'},
  {name: 'huni', email: 'asd@saf.com', color: 'blue'},
  {name: 'chulsu', email: 'asd@saf.com', color: 'yellow'},
  {name: 'mengu', email: 'asd@saf.com', color: 'red'}
]
class VI_POL_006Component extends Workspace {
  constructor(props){
    super(props)
    this.state={
      open: false, //select의 open입니다
      open2: false,//select의 open입니다
      CalendarItemOpen: false,
      ScheduleModalOpen: false,
      ScoreCardModalOpne: false,
      eventSelectV: 'All Events',
      calendarItem: null,
      age: '',
      eventArray: [
        {name: 'All Events', value: 'All Events'},
        {name: 'My Events', value: 'My Events'},
        {name: 'Not Events', value: 'Not Events'}
      ],
      Position: List([
                    {title: 'Software Engineer',
                    check: true},
                    {title: 'Designer',
                    check: true},
                    {title: 'Data analysis',
                    check: true}
                  ]),
      arr: List([
        {
          id: 1, //id pk로 꼭 넣어주세요~ 그래야 인덱스 값을 알 수 있어요
          name: 'choi',
          title: 'kkk',
          start: new Date(new Date().setDate(24)),
          end: new Date(new Date().setDate(24)),
          interViewers: new List([
              {name: 'chulsu', email: 'asd@saf.com', color: 'yellow'},
              {name: 'mengu', email: 'asd@saf.com', color: 'red'}
                  ]),
          worldtime: 'Asia/Seoul',
          time: '02:00PM',
          duration: '1 hr',
          date: new Date(),    
          location: '울산 중구 동구 북구',    
          event: 'My Events',
          job: 'Software Engineer',
          check: false,
          desc: '설명 이러쿵 저러쿵',
          color: null,
          cost : 1000,
        },
      ]),
      //달력을 클릭하면 나오는 수정하는 창을 위한 데이터
      modifiedData: Map(clear),
    }
    this.handleEdit = this.handleEdit.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleChanged = this.handleChanged.bind(this);
    this.handleModifiedItem = this.handleModifiedItem.bind(this);
    this.handleClickCalendar = this.handleClickCalendar.bind(this);
    this.handleFindIndex = this.handleFindIndex.bind(this);
    this.handleChangedPosition = this.handleChangedPosition.bind(this); //OpenCheckBox의 handlechange입니다.
  }

  handleModifiedItem(){
    const modifiedData = this.state.modifiedData.toJS()
    let obj = {
       arr: this.state.arr.set(this.handleFindIndex(modifiedData.id), modifiedData ) 
    };
    this.setState(obj);
    this.handleOpen('ScheduleModalOpen');
  }

  handleAutoCompleteDelete(idx){ //schedule모달 안에 자동완성 인풋을 위한 함수
    let val = this.state.modifiedData.get('interViewers')
    val.splice(idx, 1);
    this.setState({
      modifiedData: this.state.modifiedData.set('interViewers', val)
    })
  }

  handleAutoCompleteAdd(value) { //schedule모달 안에 자동완성 인풋을 위한 함수
     this.setState({modifiedData: this.state.modifiedData.update('interViewers', list => list.concat(value))})
    //  this.setState({
    //   modifiedData: this.state.modifiedData.set('interViewers', val)
    // })
    // var obj = {}
    // obj = { modifiedData: this.state.modifiedData.update('interViewers', list => list.push(value)) }
    // this.setState(obj)
  } 

  handleClickCalendar(e){
    this.handleOpen('CalendarItemOpen');
    this.setState({calendarItem: e});
  }

  handleChanged(e, flg){
    let obj;
    if(flg === 'checkbox'){
      obj = {
        arr: this.state.arr.setIn([this.handleFindIndex(e.id), 'check'], !e.check ) 
      };
    }else if(flg === 'ScheduleModal'){
      const {name, value} = e.target;
      obj = {
        modifiedData: this.state.modifiedData.set(name, value)
      };
      this.setState(obj);
    }else{
      const {name, value} = e.target;
      obj = { [name]: value };
    }
    this.setState(obj);
  }

  handleEdit(data){  //수정하는 schedule모달 여는 함수
    this.handleOpen('ScheduleModalOpen');
     //클릭한 아이템의 데이터를 따로 넣어주고 수정을 합니다.
    this.setState({modifiedData: Map(data)})
  }

  ////big calendar 모듈에서 선택한 값의 인덱스를 리턴하는 함수가 없어서 만들었습니다.////
  handleFindIndex(id){
    const array = this.state.arr.toJS();
    for(var i = array.length - 1; i >= 0; i--){
      if(id === array[i].id ){
        return i
      }
    }
  }

  handleChangedPosition(e, i){ 
    //달력 안의 check를 true false 수정하는 함수
    const {name, checked} = e.target;
    var obj
    obj = {
      [name]: this.state[name].setIn([i, 'check'], checked)
    }
    this.setState(obj)
  }

  handleOpen(name){
    //모달을 여는 함수
    this.setState({[name]: !this.state[name]})
  }
  
  render() {
    const { classes } = this.props;

    const eventWrapperProps = ({ event, children }) => {
      const useStyles = makeStyles({
        wrap: {
          '&>div': {
            background: `${event.color ? event.color : 'rgba(90, 192, 229, 0.8)'} !important`
          },}})
      const myClasses = useStyles();
      return  <div className={classNames(myClasses.wrap, classes.checkWrap)}>
                {children}
              </div>}

    const Event = ({ event }) => {
      return (
        <div className={classes.eventWrap}>
          <div>
            <Checkbox
              checked={event.check}
              onChange={()=>this.handleChanged(event, 'checkbox')}
              checkedClassName={classes.checkBoxChecked}
              className={classes.checkBox}/>
          </div>
          <Avatar
            className={classes.avarta}
            type='small1'
            borderColor={'black'}
            color={'black'}
            onClick={()=>this.handleClickCalendar(event)}>
            {String(event.title).substring(0, 2).toUpperCase()}
          </Avatar>
          <div
            style={{width: '100%'}}
            onClick={()=>this.handleClickCalendar(event)}>
            <Typography
              variant={'body2'}>
            {event.title}
            </Typography>
            <Typography
              variant={'body2'}>
            {event.cost} 원
            </Typography>
          </div>
        </div>
      )
    }

    const eventFilterArr = this.state.eventSelectV === 'All Events' ? this.state.arr.toJS() : //eventSelectV의 값이all events이면 필터를 안합니다
                              this.state.arr.toJS().filter(m => m.event === this.state.eventSelectV);

    // const positionFilterArr = 
    //Position select의 필터는 어떤 방식으로 해야 할지 모르겠습니다.

    return (
      <div className={classes.root}>
        <div className={classes.filterWrap}>
          <Typography
            variant={'h6'}
            style={{marginBottom: '20px'}}>
            Filter Options
          </Typography>
          <form autoComplete="off" className={classes.eventSelectWrap}>
            <Button className={classes.selectBtn} onClick={()=>this.handleOpen('open2')} grayBtn>
              <div className={classes.selectBtnInner}>
                <div className={classes.flex}>
                <i className="fas fa-user-alt" style={{marginRight: '4px'}}/>
                {this.state.eventSelectV}
                </div>
                <i className="fas fa-caret-down"/>
              </div>
            </Button>
            <FormControl style={{position: 'relative'}}>
              <Select
                className={classes.eventSelect}
                open={this.state.open2}
                onClose={()=>this.handleOpen('open2')}
                onOpen={()=>this.handleOpen('open2')}
                value={this.state.eventSelectV}
                name={'eventSelectV'}
                onChange={e=>this.handleChanged(e)}>
                {this.state.eventArray.map((r, i)=>
                  <MenuItem
                    key={i}
                    value={r.value}
                    className={classes.eventSelectItem}>
                      <Typography
                        fontWeight={5}
                        variant={'subtitle2'}
                        className={classes.text}>
                        {r.name}
                      </Typography>
                  </MenuItem>
                  )}
              </Select>
            </FormControl>
          </form>
        <OpenCheckBox
          open={this.state.open}
          arrNameA={'Position'}
          arrNameB={'Position'}
          arrayA={this.state.Position.toJS()}
          arrayB={this.state.Position.toJS()}
          handleOpen={()=>this.handleOpen('open')}
          handleChangeA={(e, i)=>this.handleChangedPosition(e, i)}
          handleChangeB={(e, i)=>this.handleChangedPosition(e, i)}/>
        </div>
        <Calendar
          localizer={localizer}
          events={eventFilterArr}
          startAccessor="start"
          endAccessor="end"
          popup={true}
          components={{
            eventWrapper: eventWrapperProps,
            event: Event}}
          style={{
            width: '100%'
          }}/>
        <CalendarModal
          open={this.state.CalendarItemOpen}
          name={this.state.calendarItem ? this.state.calendarItem.name : ''}
          data={this.state.calendarItem}
          handleModal={e=>this.handleOpen('CalendarItemOpen')}
          handleEditBtn={data=>this.handleEdit(data)}
          handleCandidateBtn={()=> this.handleOpen('ScoreCardModalOpne')}/>
        <ScheduleModal
          //안에 들어가는 데이터가 obj형으로 한번에 들어가는게 아닌 따로 빼는 이유는 
           // 다른데서도 사용하기 때문입니다.
          open={this.state.ScheduleModalOpen}
          worldtime={this.state.modifiedData.toJS().worldtime}
          time={this.state.modifiedData.toJS().time}
          duration={this.state.modifiedData.toJS().duration}
          interViewer={this.state.modifiedData.toJS().interViewer}
          title={this.state.modifiedData.toJS().title}
          location={this.state.modifiedData.toJS().location}
          desc={this.state.modifiedData.toJS().desc}
          date={this.state.modifiedData.toJS().date}
          handleModal={()=> this.handleOpen('ScheduleModalOpen')}
          handleDatePickerChange={this.handleDatePickerChange}
          handleChange={e=>this.handleChanged(e, 'ScheduleModal')}
          handleAddSchedule={()=>this.handleModifiedItem()}
          //////자동완성 맴버추가를 위한 데이터////////
          autoCardData={autoCardData}
          interViewers={this.state.modifiedData.toJS().interViewers}
          valueName={'name'}
          handleAutoCompleteAdd={value => this.handleAutoCompleteAdd(value)}
          handleAutoCompleteDelete={idx => this.handleAutoCompleteDelete(idx)}/>
        <ScoreCardModal
          open={this.state.ScoreCardModalOpne}
          name={this.state.calendarItem ? this.state.calendarItem.name : ''}
          handleModal={()=> this.handleOpen('ScoreCardModalOpne')}
        />
      </div>
    )
  }
}

export default VI_POL_006Component