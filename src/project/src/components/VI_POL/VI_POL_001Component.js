import React from 'react'
import Workspace from '../internal/Workspace';
import classNames from 'classnames';
import MenuItem from "@material-ui/core/MenuItem";
import AddCandModal from './VI_POL_001ModalComponent'
import RSRCdModal from '../../containers/RSRC/VI_RSRC_ModalContainer'
import {List, Map} from 'immutable';
import Dnd from './VI_POL_001/VI_POL_001DND'
import { Select, Button } from '../unit/index'

class VI_POL_001Component extends Workspace {
    constructor(props){
        super(props)
        this.state={
            selectArr: new List([ Map({name: 'hahaa'}), Map({name: 'hahah'}), Map({name: 'happ'})]),
            seleVal1:'hahah',
            candmodalOp: false,
            candNm: '',
            candEmail: '',
            candPn: '',
            canDesc: '',
            rsrcModalOp: false,
            rsrcData: null,
        }
        this.handleChanged = this.handleChanged.bind(this);
        this.handleModal = this.handleModal.bind(this);
        this.handleRSRC = this.handleRSRC.bind(this);
    }

    handleChanged(e){
      const {name, value} = e.target;
      var obj = {
          [name]: value
      }
      this.setState(obj)
    }

    handleModal(name){
        this.setState({
          [name]: !this.state[name]
        })
    }

    handleRSRC(data){
      this.setState({
        rsrcData: data
      })
      this.handleModal('rsrcModalOp');
    }
    
    render() {
        const { classes } = this.props;

        return (
            <div className={classNames(classes.root)}>
                <div className={classes.header}>
                    <Select
                      TopBtn
                      style={{width: '100px'}}
                      value={this.state.seleVal1}
                      onChange={e => this.handleChanged(e)}
                      name={"seleVal1"}
                      shape={'sm'}>
                      {this.state.selectArr.toJS().map((r,i) => (
                          <MenuItem key={i} value={r.name}>
                              {r.name}
                         </MenuItem>
                      ))}
                    </Select>
                    <Button
                      stepBtn
                      onClick={()=>this.handleModal('candmodalOp')}
                      className={classes.stepBtn}>
                      <i style={{marginRight: '8px'}}
                      className="fas fa-plus"/>
                      Add Candidates
                  </Button>
                </div>
                  <Dnd
                    handleRSRC={data=>this.handleRSRC(data)}/>
                  <AddCandModal
                    open={this.state.candmodalOp}
                    handleModal={()=>this.handleModal('candmodalOp')}
                    handleChanged={e=>this.handleChanged(e)}
                    candNm={this.state.candNm}
                    candEmail={this.state.candEmail}
                    candPn={this.state.candPn}
                    canDesc={this.state.canDesc}
                  />
                  <RSRCdModal
                    open={this.state.rsrcModalOp}
                    handleModal={()=>this.handleModal('rsrcModalOp')}
                    data={this.state.rsrcData}
                  />
            </div>
        )
    }
}

export default VI_POL_001Component