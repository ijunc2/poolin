import customTheme from "./theme";

const css = theme => ({
    /////////////
    root: {
        display: 'flex',
        width: '100%',
        height: '100%',
        overflowX: 'scroll',
        paddingBottom: '8px',
    },
    pipelineWarp: {
        background: 'rgb(238, 238, 238)',
        position: 'relative',
        marginRight: '8px',
        borderRadius: '4px',
    },
    pipe: {
        height: '100%',
        padding: '50px 4px 8px 8px',
        borderRadius: '4px',
    },
    pipe_header: {
        marginBottom: '8px',
        display: 'flex',
        justifyContent: 'space-between',
        position: 'absolute',
        width: '238px',
        margin: '8px 4px 8px 10px',
        '& button': {
            padding: 0,
        },
        '& i': {
            marginRight: '4px',
            fontSize: '13px'
        },
    },
    pipe_scroll: {
        height: '100%',
        paddingRight: '4px',
        overflowY: 'hidden',
        '&:hover': {
            overflowY: 'auto',
        }
    },
    pipe_number: {
        margin: 'auto 0 auto 4px',
    },
    testDiv: {
        width: '200px',
        height: '150px',
        background: 'red',
    },




    footerMenu: {
        position: 'absolute',
        height: '200px',
        width: '500px',
        // background: 'red',
        bottom: 0,
        zIndex: 1,
    },
    footerItem: {
        position: 'absolute',
        width: '150px',
        height: '150px',
        marginRight: '10px',
        bottom: 0,
        zIndex: '1',
        '&:nth-child(1)': {
            background: 'red',
        },
        '&:nth-child(2)': {
            background: 'blue',
        },
        '&:nth-child(3)': {
            background: 'yellow',
        },
        '&:nth-child(4)': {
            background: 'green',
        },
        '&:nth-child(5)': {
            background: 'orange',
        },
        '&:nth-child(6)': {
            background: 'gray',
        },


    },
























    card: {
        display: 'flex',
    },
    avatar: {
        width: '32px',
        height: '32px',
        fontSize: '1.0rem',
    },
    draggable: {
        '&:hover': {
            background: 'rgba(255, 255, 255, 0.5) !important'
        }
    },
    card_content: {
        paddingLeft: theme.spacing.unit * 2,
        flexGrow: 1,
        '& > h6': {
            lineHeight: '0.5',
        }
    },
    card_bottom: {
        display: 'flex',
        justifyContent: 'space-between',
        '& > div:nth-child(2)': {
            textAlign: 'right',
            '& > *': {
                color: 'rgba(0, 0, 0, 0.47)',
                lineHeight: '1.2'
            },
            '& >:nth-child(1) > span': {
                fontWeight: 'bold',
                color: 'rgba(0, 0, 0, 0.87)',
            }
        }
    },
    card_bottom_coin_warp: {
        fontSize: 11,
        display: 'flex',
        paddingTop: theme.spacing.unit,
        '& > div:nth-child(1)': {
            width: 14,
            height: 14,
            fontSize: 10,
        },
        '& > p': {
            margin: `${theme.spacing.unit / 4}px 0 0 ${theme.spacing.unit / 4}px`,
            color: '#009688',
        }
    },
    addButtonWarp: {
        marginBottom: theme.spacing.unit * 3,
        '& button': {
            color: customTheme.palette.white.contrastText,
            backgroundColor: customTheme.palette.white.main,
            '&:hover': {
                backgroundColor: customTheme.palette.white.dark,
            },
        }
        
    }
});

export default css;