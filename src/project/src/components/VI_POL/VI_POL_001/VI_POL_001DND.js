import React, { Component } from 'react';

import MoreVertIcon from '@material-ui/icons/MoreVert';
import IconButton from '@material-ui/core/IconButton';
import style from "./css";
import DroppableComp from "../../comm/dnd/DroppableComp";
import DraggableComp from "../../comm/dnd/DraggableComp";
import {DragDropContextComp, 
    reorder, move, 
    getItemStyle, getListStyle} from "../../comm/dnd/DragDropContextComp";
import {withStyles} from '@material-ui/core/styles';
import { CandidateCard, Typography } from '../../unit/index'

const pipes = ['Applied', 'Feedback', 'Interviewing', 'Made Offer', 'Disqualified', 'Hired']
const icons = [
  <i className="fas fa-user-alt"/>,
  <i className="fas fa-users"/>,
  <i className="fas fa-comments"/>,
  <i className="fas fa-gift"/>,
  <i className="fas fa-trash"/>,
  <i className="fas fa-trophy"/> ]

class VI_POL_001DND extends Component {
  constructor(props) {
    super(props)  
    this.state = {
            //배열 안의 카드 데이터에 pk가 반드시 있어야 합니다
      Applied: [
        {id: 11, task: '밥먹기', who: '짱구', num: 1, color: '#83dcb7', date: new Date()},
        {id: 12, task: '밥먹기', who: '철수', num: 1, color: '#edacb1', date: new Date()},
        {id: 13, task: '밥먹기', who: '유리', num: 1, color: '#bfff00', date: new Date()},
        {id: 14, task: '밥먹기', who: '짱구', num: 1, color: '#008d62', date: new Date()},
        {id: 15, task: '밥먹기', who: '짱구', num: 1, color: '#8770b4', date: new Date()},
        {id: 16, task: '밥먹기', who: '짱구', num: 1, color: '#5e7e9b', date: new Date()},
        {id: 17, task: '밥먹기', who: '짱구', num: 1, color: '#ff4040', date: new Date()},
        {id: 18, task: '밥먹기', who: '짱구', num: 1, color: '#392f31', date: new Date()},
        {id: 19, task: '밥먹기', who: '짱구', num: 1, color: '#005666', date: new Date()},
        {id: 20, task: '밥먹기', who: '짱구', num: 1, color: '#ff7f00', date: new Date()},
      ],
      Feedback: [],
      Interviewing: [],
      'Made Offer': [],
      Disqualified: [],
      Hired: [],
    }
}

onDragEnd = result => {
    const { source, destination } = result;

    if (!destination) {
        return;
    }

    // 같은 pipeline 내에서 움직일 때
    if (source.droppableId === destination.droppableId) {
        const items = reorder(
            this.state[source.droppableId],
            source.index,
            destination.index
        );
        
        let state = { [source.droppableId]: items };
        this.setState(state);
    } else {
        // pipeline이 달라질 때
        const result = move(
            this.state[source.droppableId],
            this.state[destination.droppableId],
            source,
            destination
        );
        this.setState(result)
    }
};

render() {

    const { classes } = this.props;

    return (
        <div className={classes.root}>
          <DragDropContextComp onDragEnd={this.onDragEnd}>
              {pipes.map((pipe, index) => (
                  <div  className={classes.pipelineWarp} key={index}>
                      <div className={classes.pipe_header}>
                        <div style={{display: 'flex'}}>
                          <Typography variant={'subtitle1'}
                            fontWeight={7}>
                              {icons[index]}
                            {pipe}
                          </Typography>
                          <Typography variant={'caption'}
                            className={classes.pipe_number}>
                            {this.state[pipe].length}
                          </Typography>
                        </div>
                          <div>
                            <IconButton aria-label="MoreVert">
                                <MoreVertIcon />
                            </IconButton>
                          </div>
                      </div>
                      {/* pipeline start */}
                      <DroppableComp
                        pipe={pipe}
                        getListStyle={getListStyle}
                        style={classes.pipe}>
                          {/* item start */}
                          <div className={classes.pipe_scroll}>
                          {this.state[pipe].map((item, index) => (
                              <DraggableComp
                                key={item.id}
                                draggableId={item.id}
                                index={index}
                                getItemStyle={getItemStyle}
                                style={classes.draggable}>
                              <CandidateCard
                                onClick={()=>this.props.handleRSRC(item)}
                                key={index} data={item}/>
                              </DraggableComp>
                          ))}
                          </div>
                          {/* item end */}
                      </DroppableComp>
                      {/* pipeline end */}
                  </div>
              ))}
          </DragDropContextComp>
          <div className={classes.testDiv}
            onDragEnd={()=>console.log('hahah')}>
            
          </div>
        </div>
    );
}
}
export default withStyles(style, { withTheme: true })(VI_POL_001DND);