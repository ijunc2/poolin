import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import moment from 'moment'
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';

import style from "./css";

const Card001Compoent = ({...state}) => {
    const {classes, item} = state

  return (
    <div className={classes.card}>
        <Avatar className={classes.avatar} style={{backgroundColor: 'blue'}}>{item.name}</Avatar>
        <div className={classes.card_content}>
            <Typography variant="h6">
                {item.name}
            </Typography>
            <Typography variant="caption" gutterBottom>
                {item.email}
            </Typography>
            <div className={classes.card_bottom}>
                <div className={classes.card_bottom_coin_warp}>
                    <Avatar>C</Avatar>
                    <p>10</p>
                </div>
                <div>
                    <Typography variant="caption">
                        Added by <span>{item.history.who}</span>
                    </Typography>
                    <Typography variant="caption">
                    {moment(new Date, "YYYYMMDD").fromNow()}
                    </Typography>
                </div>
            </div>
        </div>
    </div>
  );
}

export default withStyles(style, { withTheme: true })(Card001Compoent);