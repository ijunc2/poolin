import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import classNames from 'classnames';
import blue from '@material-ui/core/colors/blue';
import { Typography, Input, Button } from '../unit/index'

import css from '../../assets/styles/vi_pipe';

const VI_POL_001ModalComponent = ({ ...state }) => {
  const {
    handleModal,
    handleChanged,
    open,
    classes,
    candNm,
    candEmail,
    candPn,
    canDesc,
  } = state;
  return (
    <Dialog
      className={classes.modalRoot}
      onClose={handleModal}
      aria-labelledby="customized-dialog-title"
      open={open}>
        <div className={classes.modalContent}>
          <Button
            onClick={handleModal}
            className={classes.closeBtn}
            grayBtn>
            <i style={{fontSize: '21px'}} className="fas fa-times"/>
          </Button>
          <Typography
            variant={'h5'}
            fontWeight={3}
            style={{marginBottom: '10px'}}>
            Add Candidates
          </Typography>
          <Typography
            variant={'subtitle1'}
            fontWeight={3}
            style={{marginBottom: '10px'}}>
             Add a Candidate Manually
          </Typography>
          <Input 
            placeholder='Full Name (Required)'
            value={candNm}
            name={'candNm'}
            className={classNames(classes.width100, classes.mrgB10)}
            onChange={e=>handleChanged(e)}
            style={{marginBottom: '10px'}}
            shape={'sm'} 
            color={blue}/>
            <div style={{marginBottom: '10px'}}>
              <Input 
                placeholder='Email Address'
                value={candEmail}
                name={'candEmail'}
                style={{width: '215px', marginRight: '10px'}}
                onChange={e=>handleChanged(e)}
                shape={'sm'} 
                color={blue}/>
              <Input 
                placeholder='Phone Number'
                value={candPn}
                name={'candPn'}
                style={{width: '215px'}}
                onChange={e=>handleChanged(e)}
                shape={'sm'} 
                color={blue}/>
            </div>
            <Typography
              variant={'subtitle1'}
              fontWeight={3}
              style={{marginBottom: '10px'}}>
              Summary / Description
            </Typography>
            <Input 
              className={classNames(
                classes.width100, classes.mrgB10, classes.mrgT0, classes.textArea)}
              onChange={e=>handleChanged(e)}
              placeholder="e.g A great software Darren intro'd me to"
              value={canDesc}
              name={'canDesc'}
              multiline
              color={blue}/>
            <div style={{display: 'flex', justifyContent: 'flex-end'}}>
              <Button
                style={{marginRight: '10px'}}
                bgNone>
                Cancel
              </Button>
              <Button
                style={{height: '32px'}}
                stepBtn>
                Continue
              </Button>
            </div>
        </div>
  </Dialog>
  );
};
export default withStyles(css)(VI_POL_001ModalComponent);