const fontcolor = '#4c565c';
const fontWeight = 400;

const styles = () => ({
  autoComplete:{
    width: 'calc(100% - 220px)',
    position: 'relative',
    fontWeight: fontWeight,
    color: fontcolor,
  },
  borderWrap: {
    minHeight: '33.85px',
    borderRadius: '4px',
    border: '1px solid #ced4da',
    padding: '2px 4px',
    backgroundColor: '#fff',
    position: 'absolute',
    right: 0,
    left: 0,
    zIndex: 2,
    '& input': {
      border: 0,
      outline: 'none',
    }
  },
  nameItem: {
    display: 'flex',
    margin: '2.5px 3px 3px 0',
    backgroundColor: '#30a6d1',
    padding: '2px 8px',
    borderRadius: '3px',
    float: 'left',
    '& i': {
      margin: '4px 0 0 6px',
      '&:hover':{
        cursor: 'pointer'
      }
    }
  },
  itemText: {
    fontSize: '13px',
    color: '#fff !important',
  },
  itemListWrap:{
    top: '35px',
    ///
    padding: '.5rem 0',
    border: '1px solid #dedede',
    borderRadius: '4px',
    background: '#fff',
    marginTop: '4px',
    display: 'block',
    width: '200px',
    zIndex: 2,
    position: 'absolute',
    flexDirection: 'column',
    maxHeight: '142px',
    overflowY: 'auto',
    overflowX: 'auto',
  },
  avatar: {
    fontSize: '16px',
    border: '0 !important',
    marginRight: '6px'
  },
  item: {
    display: 'flex',
    backgroundColor: '#edeff0',
    fontWeight: fontWeight,
    fontSize: '.825rem',
    padding: '8px 4px',
    borderRadius: '4px',
    lineHeight: '32px',
    color: 'rgba(0, 0, 0, 0.6)',
    margin: '4px .75rem',
    cursor: 'pointer',
  },
  bg: {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    position: 'fixed',
    zIndex: 1,
  },
  defaultAutoComplete: {
    width: 'calc(100% - 220px)',
  },
});

export default styles;