import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import classNames from 'classnames';
import { Typography, Avatar } from '../index';
import css from "./autocompleteCardCSS";

class AutocompleteCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      input: '',
      filterArr: this.props.autoCardData,
    }
    this.handleFilter = this.handleFilter.bind(this);
    this.handleShowOpen = this.handleShowOpen.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.textView = React.createRef();
  }

  handleFilter(){
    const { autoCardData, interViewers }= this.props
    const valueName = this.props.valueName;
    let arr = [...autoCardData];
    let result =[]
    // input에 추가되면 아래 아이템이 제거되는 코드
    for(let i = 0; i < arr.length; i++){
      for(let j = 0; j < interViewers.length; j++){
          if(arr[i] && (interViewers[j][valueName] === arr[i][valueName])){
            console.log(interViewers[j][valueName]);
            console.log(arr[i][valueName]);
            console.log('//////////');
            arr.splice(i, 1);
          }
      }
    }
    //속도 늦으면 중복이 들어오는 경우 있어서 중복제거 (중복이 가끔 들어갑니다)
    // result = arr.reduce((accumulator, current) => {
    //   if (checkIfAlreadyExist(current)) {
    //     return accumulator;
    //   } else {
    //     return [...accumulator, current];
    //   }
    //   function checkIfAlreadyExist(currentVal) {
    //     return accumulator.some((item) => {
    //       return (item.name === currentVal.name);
    //     });
    //   }
    // }, []);
    return arr;
  }

  handleShowOpen() {
    this.setState({
      show: !this.state.show
    })
    this.textView.current.focus();
  }

  handleShowClose() {
    this.setState({
      show: false
    })
  }

  handleChange(e) {
    const { name, value } = e.target;
    var obj = {}
    

    if(value !=='')
    obj = { [name]: value,
            show: true }
    else
     obj = { [name]: value }

    this.setState(obj);
  }

  render() {
    const { classes } = this.props;

    const {
      className, // className을 받는 프롭스
      placeholder, // placeholder 프롭스
      handleAutoCompleteAdd, //추가할때 함수
      handleAutoCompleteDelete, //삭제할때 함수
      valueName, //구조체 안에 필터 할 이름
      autoCardData, //전체 리스트 데이터
      interViewers, //담을 리스트 데이터
      ...other
    } = this.props;

    const showSwitch = {
      display: this.state.show ? 'block' : 'none',
    }

    const filter_list = this.handleFilter().filter(m => m[`${valueName}`].toLowerCase().includes(this.state.input.toLowerCase()));

    return (
      <div className={classNames(classes.autoComplete,className ? className : classes.defaultAutoComplete)}
            onClick={() => this.handleShowOpen()} {...other}>
        <div className={classes.borderWrap}>
          {interViewers.length === 0 ?
            null 
            :
            interViewers.map((r, i)=>
              <div className={classes.nameItem} key={i}
                style={{cursor: 'pointer'}}
                onClick={()=>handleAutoCompleteDelete(i)}>
                  <Typography
                    className={classes.itemText}>
                    {r.name}
                  </Typography>
                  <i className={classNames("fas fa-times", classes.itemText)}
                    />
              </div>
            )}
          <input
            ref={this.textView}
            onChange={e=>this.handleChange(e)}
            name='input'
            value={this.state.input}
            placeholder={placeholder ? placeholder : ''}/>
        </div>
        {interViewers.length !== autoCardData.length ? 
        <div style={showSwitch} className={classes.itemListWrap}>
          {filter_list.sort().map((r, i) => 
            <div
              key={i}
              className={classes.item}
              onClick={()=>handleAutoCompleteAdd(r)}>
              <Avatar
                type={'sm'}
                color={r.color}
                className={classes.avatar}>
                {String(r.name).substring(0, 1).toUpperCase()}
              </Avatar>
              <Typography
                fontWeight={6}>
                {r.name}
              </Typography>
            </div>
          )} 
        </div>
        :
         null}
          <div style={showSwitch} className={classes.bg} onClick={()=>this.handleShowOpen()}/>
    </div>
    );
  }
};

const Autocomplete = withStyles(css, { withTheme: true })(AutocompleteCard)

export default Autocomplete;