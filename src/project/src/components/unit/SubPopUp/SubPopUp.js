import React from 'react';
import classNames from 'classnames';
import Dialog from '@material-ui/core/Dialog';
import { Typography, Button } from '../../unit/index'
import { makeStyles } from '@material-ui/styles';
import color from '../../../assets/styles/material/com/color'

const SubPopUp = ({ ...state }) => {

  const {
    children,
    open,
    handleModal,
    title,
    width,
    height,
    className,
  } = state;

  const useStyles = makeStyles({
    modalRoot:{
      '&>div:nth-child(2)': {
          '&>div': {
            height: '100%',
            minWidth: width ? width :'500px',
            minHeight: height ? height : '390px',
            maxHeight: height ? height : '390px'
          }
       }
    },
    modalContent:{
      padding: '20px',
      height: '100%',
      borderRight: `10px solid ${color.gray.cardBorder}`,
      borderLeft: `10px solid ${color.gray.cardBorder}`
    },
    closeBtn:{
      position: 'absolute',
      right: '0',
      top: '0',
      minWidth: '44.74px !important',
      height: '51.82px',
      padding: '0',
    },
  })
  const classes = useStyles();
  return (
    <Dialog
    className={classNames(classes.modalRoot, className ? className : null )}
    onClose={handleModal}
    aria-labelledby="customized-dialog-title"
    open={open}>
      <div className={classes.modalContent}>
        <Button
          onClick={handleModal}
          className={classes.closeBtn}
          grayBtn>
          <i style={{fontSize: '21px'}} className="fas fa-times"/>
        </Button>
        <Typography
          variant={'h5'}
          fontWeight={3}
          style={{marginBottom: '10px'}}>
          {title}
        </Typography>
        {children}
      </div>
</Dialog>
  );
};

export default SubPopUp;