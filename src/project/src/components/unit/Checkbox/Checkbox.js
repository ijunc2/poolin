import React from 'react';
import { makeStyles } from '@material-ui/styles';
import classNames from 'classnames';
import comcolor from '../../../assets/styles/material/com/color'
import FormControlLabel from "@material-ui/core/FormControlLabel";
import CheckboxM from "@material-ui/core/Checkbox";
import CheckCircle from '@material-ui/icons/CheckCircle';
import CheckCircleOutline from '@material-ui/icons/CheckCircleOutline';

const Checkbox = ({ ...state }) => {

  const {
    children,
    className,
    checkedClassName,
    color,
    ...other
  } = state;

  const useStyles = makeStyles({
    default:{
      margin: 0,
      '&>span': {
        padding: 0,
      }
    },
    true: {
        fill: color ? color : `${comcolor.green.onGreen} !important`,
    },
    false: {

    }
 });
 
 const classes = useStyles();
  return (
    <FormControlLabel
      className={classNames(classes.default,
      className ? className : null )}
      control={
      <CheckboxM
        {...other}
        icon={<CheckCircleOutline />}
        checkedIcon={<CheckCircle
           className={classNames(checkedClassName ? checkedClassName : classes.true)}/>}
        />
    }/>
  );
};

export default Checkbox;