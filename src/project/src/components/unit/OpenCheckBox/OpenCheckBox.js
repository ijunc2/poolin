import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Button, Typography } from '../index'

const OpenCheckBox = ({ ...state }) => {

  const useStyles = makeStyles({
    bg:{
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      position: 'fixed',
      zIndex: 1,
    },
    btn: {
      width: '210px',
      padding: '5px 12px',
      color:' #68747e !important',
      fontWeight: 600,
      cursor: 'pointer',
      '& i': {
        marginTop: '6px'
      }
    },
    flex: {
      display: 'flex'
    },
    flexBtnWrap: {
      display: 'flex',
      justifyContent: 'space-between',
      width: '100%',
    },
    section: {
      borderBottom: '1px solid #eee',
      marginBottom: '5px',
    },
    list: {
      padding: '5px 12px',
      marginTop: '8px',
      borderRadius: '4px',
      position: 'absolute',
      backgroundColor: '#fff',
      zIndex: 2,
      maxHeight: '200px',
      overflowY: 'auto',
      width: '200px',
      border: '2px solid #dedede',
      '& li': {
        listStyle: 'none'
      }
    },
    text: {
      margin: '5px 0'
    },
    listItem: {
      display: 'flex',
    },
    check: {
      cursor: 'pointer',
      margin: 'auto 4px auto 0',
      height: '16px',
      width: '16px',
    }
  });
  const classes = useStyles();

  const {
    data,
    children,
    className,
    handleOpen,
    arrayA,
    arrayB,
    arrNameA,
    arrNameB,
    handleChangeA,
    handleChangeB,
    style,
    open,
    ...other
  } = state;

  const showSwitch = {
    display: open ? 'block' : 'none',
  }

  return (
    <div {...other}>
      <Button onClick={handleOpen} className={classes.btn} grayBtn>
        <div className={classes.flexBtnWrap}>
          <div className={classes.flex}>
          <i className="fas fa-briefcase" style={{marginRight: '4px'}}/>
          Position
          </div>
          <i className="fas fa-caret-down"/>
        </div>
      </Button>
      <ul style={showSwitch} className={classes.list}>
        <li className={classes.section}>
          <Typography
            fontWeight={5}
            variant={'subtitle2'}
            className={classes.text}>
            Active
          </Typography>
          {arrayB.map((r, i)=>
            <div className={classes.listItem} key={i}>
              <input className={classes.check}
                  name={arrNameA}
                  onChange={e=>handleChangeA(e, i)}
                  type="checkbox"
                  checked={r.check}/>
              <Typography
                fontWeight={5}
                variant={'subtitle2'}
                className={classes.text}>
                {r.title}
              </Typography>
            </div>
            )}

        </li>
        <li className={classes.section}>
          <Typography
            fontWeight={5}
            variant={'subtitle2'}
            className={classes.text}>
            Draft
          </Typography>
          {arrayB.map((r, i)=>
            <div className={classes.listItem} key={i}>
              <input className={classes.check}
                  name={arrNameB}
                  onChange={e=>handleChangeA(e, i)}
                  type="checkbox"
                  checked={r.check}/>
              <Typography
                fontWeight={5}
                variant={'subtitle2'}
                className={classes.text}>
                {r.title}
              </Typography>
            </div>
            )}
        </li>
      </ul>
      <div style={showSwitch} className={classes.bg} onClick={handleOpen}/>
  </div>
  );
};

export default OpenCheckBox;