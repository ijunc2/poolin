import React from 'react';
import Button1 from '@material-ui/core/Button';
import classNames from 'classnames';
import { makeStyles } from '@material-ui/styles';
import mycolor from '../../../assets/styles/material/com/color'

const Button = ({ ...state }) => {

  const {
    children,
    color,
    hoverColor,
    className,
    bgNone,
    stepBtn,
    grayBtn,
    btnH24,
    ...other
  } = state;

  const  useStyles = makeStyles({
    bgNoneButton:{
      transition: '.3s ease',
      color: color ? color : mycolor.defaultFontColor,
      fontSize: '13.3333px',
      textTransform: 'none',
    '&:hover': {
      transition: '.3s ease',
      background: `${mycolor.gray.cardBorder} !important`,
      color: hoverColor ? hoverColor: '#2D3135',
      }
    },
    stepBtn:{
      textTransform: 'none',
      color: '#fff',
      fontSize: '13.3333px',
      backgroundColor: mycolor.skyBlue.default,
      '&:hover':{
        backgroundColor: `${mycolor.skyBlue.hover} !important`
      } 
    },
    grayBtn:{
      textTransform: 'none',
      backgroundColor: '#dedede',
      borderRadius: '4px',
      color: '#444',
      '&:hover':{
        backgroundColor: `#dedede !important`
      } 
    },
    btnH24:{
      marginTop: 'auto',
      marginBottom: 'auto',
      height: '24px',
      padding: '3px 6px',
      fontSize: '.75rem !important'
    },
  });
  const classes = useStyles();
  return (
    <Button1
        { ...other}
        color="primary"
        className={classNames(className,
        bgNone ? classes.bgNoneButton : null,
        stepBtn ? classes.stepBtn : null,
        btnH24 ? classes.btnH24 : null,
        grayBtn ? classes.grayBtn : null)}>
         {children}
    </Button1>
  );
};

export default Button;