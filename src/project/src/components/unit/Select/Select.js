import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import InputBase from '@material-ui/core/InputBase';
import Select1 from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import mycolor from '../../../assets/styles/material/com/color'

const BootstrapInput = withStyles(theme => ({
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: '0.875rem',
    width: 'auto',
    padding: '6.5px 26px 6.5px 12px',
    color: '#868686',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    '&:focus': {
      borderRadius: 4,
      border: '2px solid #2196f3',
      padding: '5.5px 25px 5.5px 11px',
    },
    '&:hover':{
      borderColor: 'black'
    }
  },
}))(InputBase);

const TopBtnCom = withStyles(theme => ({
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: '#dedede',
    border: 'none',
    fontSize: '0.875rem',
    width: 'auto',
    padding: '8px 28px 8px 14px',
    color: '#868686',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    '&:focus': {
      borderRadius: 4,
      backgroundColor: mycolor.green.onGreen,
      color: '#fff'
    },
  },
}))(InputBase);

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    margin: 0,
  },
  bootstrapFormLabel: {
    fontSize: 18,
    top: theme.spacing.unit * -3,
  },
  selectItem:{
    fontSize: '.875rem',
    paddingTop: '7px',
    paddingBottom: '7px',
  }
});

const Select = ({ ...state }) => {
  const {
    classes,
    className,
    color,
    children,
    items,
    // 받을 라벨
    label,
    labelClassName,
    fullWidth,
    type2,
    TopBtn,
    ...other
  } = state;

  return (
    <FormControl className={classes.root}>
      {!label ? null :
      <InputLabel
        htmlFor="customized-select1"
        className={classes.bootstrapFormLabel}>
        {label}
      </InputLabel>
      }
      <Select1
        {...other}
        className={className ?  className : ""}
        input={TopBtn ? <TopBtnCom/> : <BootstrapInput />}>
          {children ? 
           children :
            items === undefined ? [] : items.map((item, index) => (
              <MenuItem className="select-item" key={index} value={item.value ? item.value : index}>{item.name}</MenuItem>
            ))}
      </Select1>
    </FormControl>
  );
};

export default withStyles(styles)(Select);