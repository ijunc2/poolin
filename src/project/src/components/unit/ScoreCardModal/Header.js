import React from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import Avatar from '@material-ui/core/Avatar';
import css from './css';
import { withStyles } from '@material-ui/core/styles';

const HeaderComponent = ({...state}) => {

  const { name, classes }= state;

  return (
    <DialogTitle disableTypography className={classes.title}>
        <Avatar className={classes.avatar}>
          {String(name).substring(0, 1).toUpperCase()}
        </Avatar>
    </DialogTitle>
  );
};

const Header = withStyles(css, { withTheme: true })(HeaderComponent)

export default Header;