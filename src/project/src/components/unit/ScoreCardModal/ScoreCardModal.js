import React from 'react';
import Dialog from "@material-ui/core/Dialog";
import blue from '@material-ui/core/colors/blue';
import classNames from 'classnames';
import Header from './Header';
import {withStyles} from '@material-ui/core/styles';
import css from './css';
import { Typography, Input, Button, RatingRadioButton } from '../index';

const ScoreCardModalComponent = ({ ...state }) => {

  const {
    open,
    handleModal,
    handleChanged,
    handleSaveScorecard,
    desc,
    name,
    classes,
    ...other
  } = state;

  return (
    <Dialog
      className={classes.root}
      onClose={handleModal}
      aria-labelledby="customized-dialog-title"
      open={open}
      {...other}>
        <Header
          name={name}/>
        <div className={classes.container}>
          <Typography className={classes.titleText}>
            Thoughts on this Candidate?
          </Typography>
          <Input 
            className={classNames(classes.textArea, classes.interViewTextArea)}
            onChange={handleChanged}
            placeholder={'Share your thoughts on the candidate. This information will only be visible by Hiring Managers, not general team members.'}
            value={desc}
            name={'desc'}
            multiline
            color={blue}/>
            <div className={classes.section}>
              <Typography className={classes.titleText}>
                Overall Rating?
              </Typography>
              <RatingRadioButton/>
            </div>
            <Button
              stepBtn
              onClick={handleSaveScorecard}
              style={{marginTop: '20px'}}>
              <i style={{marginRight: '8px'}}
                  className="fas fa-save"/>
              Save Scorecard
            </Button>
        </div>
  </Dialog>
  );
};

const ScoreCardModal = withStyles(css, { withTheme: true })(ScoreCardModalComponent)

export default ScoreCardModal;