import { color, font } from '../../../assets/styles/material/com/index';

const styles = ( ) => ({

  root:{
    '&>div:nth-child(2)': {
        '&>div': {
          minWidth: '500px',
        }
     }
  },
  container: {
    padding: '20px',
  },
  title:{
    backgroundColor: color.gray.default,
    display: 'flex',
    padding: '20px',
    justifyContent: 'space-between'
  },
  avatar:{
    width: '80px',
    height: '80px',
    border: '2px solid #fff',
    fontSize: '42px',
    lineHeight: '76px',
    md:{
      width: '20px',
      height: '20px',
      fontSize: '24px',
      lineHeight: '46px',
      border: 0
    }
  },
  titleText: {
    fontSize: '18px',
    fontWeight: 400,
    lineHeight: '1.5',
    marginBottom: '10px'
  },
  interViewTextArea:{
    flex: 1,
    backgroundColor: '#fff',
    '&>div':{
      height: '180px'
    },
    '& textarea::placeholder': {
      fontSize: '0.8rem',
      fontWeight: 600,
      lineHeight: '1.2rem',
    }
  },
  textArea:{
    color: font.defaultFontColor,
    display: 'inherit !important',
    marginTop: '10px',
    marginBottom: '20px',
    '&>div':{
      height: '100px',
      width: '100%',
      '&>div':{
        height: '100%',
        overflowY: 'auto'
      }
    }
  },
  section: {
    marginBottom: '10px',
  }
})

export default styles;