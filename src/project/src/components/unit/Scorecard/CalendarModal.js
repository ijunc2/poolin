import React from 'react';
import Dialog from "@material-ui/core/Dialog";
import Header from './Header';
import {withStyles} from '@material-ui/core/styles';
import css from './css';
import { Typography } from '../index';

const CalendarModalComponent = ({ ...state }) => {

  const {
    open,
    handleModal,
    data,
    name,
    classes,
    ...other
  } = state;

  const date_format= () =>{
    return `${data.start.getFullYear()} ${data.start.getMonth()+1} ${data.start.getDate()}`
  };

  const date = () =>{
    var date = null;
    data ?
      date = date_format()
    :
      date = ''
    return date
    };

  return (
    <Dialog
      className={classes.CalendarModal}
      onClose={handleModal}
      aria-labelledby="customized-dialog-title"
      open={open}
      {...other}>
        <Header
          name={name}/>
        <div className={classes.detailsContainer}>
          <div className={classes.detailsSection} style={{display: 'flex'}}>
            <div>
            <Typography
              variant={'subtitle1'}
              fontWeight={4}>
              Date / Time
            </Typography>
            <Typography
              variant={'body2'}>
              {date()} 
            </Typography>
            </div>
            <div>
            <Typography
              variant={'subtitle1'}
              fontWeight={4}>
              Organizer
            </Typography>
            <Typography
              variant={'body2'}>
              Gun Kim
            </Typography>
            </div>
          </div>
          <div className={classes.detailsSection}>
            <Typography
                variant={'subtitle1'}
                fontWeight={4}>
                Attendees
              </Typography>
            <ul className={classes.detailsList}>
              {data ? data.people.map((r, i)=>
                <li key={i}>
                  <i className="fas fa-circle"/>
                  <Typography
                    fontWeight={4}
                    variant={'body2'}>
                    {r.name}
                  </Typography>
                  <Typography
                    style={{marginLeft: '4px'}}
                    variant={'body2'}>
                    {r.email}
                  </Typography>
                </li>)
                  : ''}
            </ul>
          </div>
        </div>
  </Dialog>
  );
};

const CalendarModal = withStyles(css, { withTheme: true })(CalendarModalComponent)

export default CalendarModal;