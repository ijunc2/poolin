import {color} from '../../../assets/styles/material/com/index';

const styles = theme => ({
  // autoComplete:{
  //   width: 'calc(100% - 220px)',
  //   position: 'relative',
  //   fontWeight: fontWeight,
  //   color: fontcolor,
  // },
  // borderWrap: {
  //   height: '33.85px',
  //   display: 'flex',
  //   borderRadius: '4px',
  //   border: '1px solid #ced4da',
  //   padding: '2px 4px',
  //   backgroundColor: '#fff',
  //   '& input': {
  //     border: 0,
  //     outline: 'none',
  //   }
  // },
  nameItem: {
    display: 'flex',
    margin: '3px 3px 3px 0',
    backgroundColor: '#30a6d1',
    padding: '2px 8px',
    borderRadius: '3px',
    '& i': {
      margin: '4px 0 0 6px',
      '&:hover':{
        cursor: 'pointer'
      }
    }
  },
  itemText: {
    fontSize: '13px',
    color: '#fff !important',
  },
  listLinkItem: {
    display: 'flex'
  },
  listLinkItemIcon: {
    color: '#9e9e9e',
    width: '1rem',
    height: '1rem',
    margin: 'auto 8px auto 0'
  },
  taskTexWrap: {
    borderTop: `1px solid ${color.defaultFontColor}`,
    padding: '4px 8px',
  },
  showMoreWrap: {
    padding: '4px 8px',
    cursor: 'pointer',
    '&:hover': {
      background: color.white.headerWhite
    }
  },
  listWrap:{
    padding: '.5rem 0',
    border: '1px solid #dedede',
    borderRadius: '4px',
    background: '#fff',
    display: 'block',
    width: '250px',
    zIndex: 2,
    position: 'absolute',
    flexDirection: 'column',
  },
  listScrollWrap: {
    overflowY: 'auto',
    maxHeight: '500px',
  },
  item: {
    display: 'flex',
    padding: '4px 8px',
    minHeight: '30px'
  },
  headerInput: {
    marginLeft: 'auto',
    marginRight: '8px',
    '& input':{
      padding: '9px 14px',
      transition: theme.transitions.create('width'),
      [theme.breakpoints.up('sm')]: {
        width: 120,
        '&:focus': {
          width: 200,
          '&<div<div<div': {
            backgroundColor: 'red'
          }
        },
      },
    },
  },
  bg: {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    position: 'fixed',
    zIndex: 1,
   // background: 'red'
  },
  defaultAutoComplete: {
    marginLeft: 'auto'
  },
});

export default styles;