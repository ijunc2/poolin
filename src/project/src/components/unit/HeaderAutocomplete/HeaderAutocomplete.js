import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import InputAdornment from '@material-ui/core/InputAdornment';
import Search from '@material-ui/icons/Search';
import classNames from 'classnames';
import { comma } from '../../../assets/utils/comm';
import { color } from '../../../assets/styles/material/com/index';
import { Typography, Input, Atag, Checkbox } from '../index';
import css from "./headerAutocompleteCSS";

class HeaderAutocompleteComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      input: '',
      openMoreList: false,
    }
    this.handleShowOpen = this.handleShowOpen.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleShowOpen(name, e) {
    if(name === 'openMoreList')
      e.stopPropagation();// 이벤트가 여러개 겹쳐 있을 경우 막아주는 이벤트 입니다.
    this.setState({
      [name]: !this.state[name]
    })
  }

  handleShowClose() {
    this.setState({
      show: false
    })
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  }

  SearchBoldComponent =(title) => { 
    //input에 동일한 검색어가 있으면 굵은 글씨로 만들어 주는 함수
    let titleArr = [...title];
    let inputArr = [...this.state.input];
    if(this.state.input === ''){
      return(
        <Typography
          style={{margin: 'auto 0'}}>
          {title}
        </Typography>
      )
    }else{
      return (
        titleArr.map(r=>
          inputArr.find(m => m === r) ? 
          r === " " ?
          <div style={{marginRight: '4px'}}/>
            : 
          <Typography
            fontWeight={4}
            style={{margin: 'auto 0'}}>
            {r}
         </Typography>
              :
          r=== " " ?
           <div style={{marginRight: '4px'}}/>
            : 
          <Typography
            style={{margin: 'auto 0'}}>
            {r}
          </Typography>))
    }
  };

  render() {
    const { classes } = this.props;

    const {
      className, // className을 받는 프롭스
      placeholder, // placeholder 프롭스
      handleAutoCompleteAdd, //추가할때 함수
      valueName, //구조체 안에 필터 할 이름
      autoCardData, //전체 리스트 데이터
      interViewers, //담을 리스트 데이터
      ...other
    } = this.props;

    const showSwitch = {
      display: this.state.show ? 'block' : 'none',
    }

    const filter_list = autoCardData.sort().filter(m => m[`${valueName}`].toLowerCase().includes(this.state.input.toLowerCase()));

    return (
      <div className={classNames(className ? className : classes.defaultAutoComplete)}
            onClick={() => this.handleShowOpen('show')} {...other}>
        <Input
          autoComplete='off'
          value={this.state.input}
          name='input'
          placeholder="검색"
          className={classes.headerInput}
          onChange={e => this.handleChange(e)}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <Search color={'primary'}/>
              </InputAdornment>
            ),
          }}/>
        <div style={showSwitch} className={classes.listWrap}>
          <Atag className={classNames(classes.listLinkItem, classes.item)}
             href={`/${this.props.workspaceId}/my-tasks`}>
            <Search
              className={classes.listLinkItemIcon}/>
            <Typography
              style={{margin: 'auto 0'}}
              variant={'caption'}>
              {'Item Width'}
            </Typography>
            <Typography
              style={{margin: 'auto 4px'}}
              fontWeight={6}>
              {this.state.input}
            </Typography>
            <Typography
              style={{margin: 'auto 0'}}
              variant={'caption'}>
             {' >>'}
            </Typography>
          </Atag>
          <Typography
            className={classes.taskTexWrap}
            variant={'caption'}>
            {'Task'}
          </Typography>
          <div className={classes.listScrollWrap}>
          {filter_list.map((r, i) => 
            !this.state.openMoreList && i > 3 ?
            //접혀있고 3개만 보여주기 위한 조건문 입니다.
            null
            :
            <div
              key={i}
              className={classes.item}
              onClick={()=>handleAutoCompleteAdd(r)}>
              <Checkbox
                checked={r.checked}/>
                {this.SearchBoldComponent(r.title)}
              <Typography
                style={{margin: 'auto 4px'}}
                color={color.skyBlue.default}
                variant={'caption'}>
                {comma(r.cost)}
              </Typography>
              <Typography
                style={{margin: 'auto 0'}}
                variant={'caption'}>
                {r.project}
              </Typography>
            </div>
          )}
          </div>
          <Typography
            color={color.skyBlue.default}
            className={classes.showMoreWrap}
            onClick={e => this.handleShowOpen('openMoreList', e)}
            variant={'caption'}>
              {this.state.openMoreList ?
                'Fold a list'
                  :
                'open show more'}
          </Typography> 
        </div>
          <div style={showSwitch} className={classes.bg} onClick={()=>this.handleShowClose()}/>
    </div>
    );
  }
};

const HeaderAutocomplete = withStyles(css, { withTheme: true })(HeaderAutocompleteComponent)

export default HeaderAutocomplete;