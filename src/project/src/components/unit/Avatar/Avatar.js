import React from 'react';
import AvatarM from '@material-ui/core/Avatar';
import classNames from 'classnames';
import { makeStyles } from '@material-ui/styles';

const Avatar = ({ ...state }) => {

  const {
    children,
    className,
    color,
    type,
    borderColor,
    ...other
  } = state;

  const  useStyles = makeStyles({
    default: {
      border: `2px solid ${borderColor ? borderColor : '#fff'}`,
      background: color ? color : 'gray'
    },
    big: {
      width: '80px',
      height: '80px',
      fontSize: '3.5rem'
    },
    midbig:{
      width: '60px',
      height: '60px',
      fontSize: '2.5rem'
    },
    mid: {
      width: '40px',
      height: '40px',
    },
    smallmid: {
      width: '30px',
      height: '30px',
    },
    small1: {
      width: '25px',
      height: '25px',
    },
    small: {
      width: '20px',
      height: '20px',
    }
  });

  const classes = useStyles();

  return (
    <AvatarM
        { ...other}
        className={classNames(className,
        classes.default,
        type === 'big' ? classes.big : null,
        type === 'midbig' ? classes.midbig : null,
        type === 'mid' ? classes.mid : null,
        type === 'smallmid' ? classes.smallmid : null,
        type === 'small1' ? classes.small1 : null,
        type === 'sm' ? classes.small : null)}>
         {children}
    </AvatarM>
  );
};

export default Avatar;