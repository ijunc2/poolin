import { color, font } from '../../../assets/styles/material/com/index';

const styles = ( ) => ({
  defaultGroup:{
    flexDirection: 'row',
    width: '100%',
    height: '30px'
  },  
  defaultItem:{
    position: 'relative',
    border: '1px solid #dedede',
    margin: '0',
    width: '23%',
    display: 'flex',
    justifyContent: 'center',
    '& i': {
      marginRight: '4px'
    },
    '&>span': {
      fontSize: '12px',
      padding: 0,
    },
    '&>span:nth-child(1)': {
      visibility: 'hidden'
    },
    '&>span:nth-child(2)': {
      position: 'absolute'
    },
    '&:first-child':{
      borderTopLeftRadius: '4px',
      borderBottomLeftRadius: '4px',
    },
    '&:nth-child(2)':{
    },
    '&:nth-child(3)':{
      width: '8%',
    },
    '&:nth-child(4)':{
    },
    '&:last-child':{
      borderTopRightRadius: '4px',
      borderBottomRightRadius: '4px',
    },
  },
  onItemGreen:{
    backgroundColor: color.green.onGreen,
    borderColor: `${color.green.onGreen} !important`,
    '& svg': {color: color.green.onGreen},
    '& span': {color: '#fff !important'},
  },
  onItemRed:{
    backgroundColor: color.red.onRed,
    borderColor: `${color.red.onRed} !important`,
    '& svg': {color: color.red.onRed},
    '& span': {color: '#fff !important'},
  },
  onItemYellow:{
    backgroundColor: color.yellow.onYellow,
    borderColor: `${color.yellow.onYellow} !important`,
    '& svg': {color: color.yellow.onYellow},
    '& span': {color: '#fff !important'},
  },
  offItemGreen:{
    backgroundColor: '#edeff0',
    '& svg': { color: '#edeff0',
               backgroundColor:'#edeff0'},
    '& span': { color: `${font.defaultFontColor} !important`},
    '&:hover':{
      transition: '.3s ease',
      backgroundColor : color.green.onGreen,
      borderColor: `${color.green.onGreen} !important`,
      '& svg': {color: color.green.onGreen,
                backgroundColor:color.green.onGreen},
      '& span': {transition: '.3s ease',
                  color: '#fff !important',},
    }
},
  offItemRed:{
    backgroundColor: '#edeff0',
    '& svg': { color: '#edeff0',
              backgroundColor:'#edeff0'},
    '& span': { color: `${font.defaultFontColor} !important`},
    '&:hover':{
      transition: '.3s ease',
      backgroundColor : color.red.onRed,
      borderColor: `${color.red.onRed} !important`,
      '& svg': {color: color.red.onRed,
                backgroundColor: color.red.onRed},
      '& span': {transition: '.3s ease',
                  color: '#fff !important',},
    }
},
    offItemYellow:{
      backgroundColor: '#edeff0',
      '& svg': { color: '#edeff0',
                backgroundColor:'#edeff0'},
      '& span': { color: `${font.defaultFontColor} !important`},
      '&:hover':{
        transition: '.3s ease',
        backgroundColor : color.yellow.onYellow,
        borderColor: `${color.yellow.onYellow} !important`,
        '& svg': {color: color.yellow.onYellow,
                  backgroundColor: color.yellow.onYellow},
        '& span': {transition: '.3s ease',
                    color: '#fff !important',},
      }
    },
})

export default styles;