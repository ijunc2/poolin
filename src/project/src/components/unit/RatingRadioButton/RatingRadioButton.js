import React from 'react';
import classNames from 'classnames';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import {withStyles} from '@material-ui/core/styles';
import css from './css';

const ThreeRadioButtonComponent = ({ ...state }) => {

  const {
    classes,
    className,
    handleChanged,
    value,
    name,
    ...other
  } = state;

  return (
      <RadioGroup
          {...other}
          aria-label="RadioGroup"
          name={name}
          className={classNames(classes.defaultGroup,className)}
          value={value}
          onChange={handleChanged}>
          <FormControlLabel
            className={classNames(classes.defaultItem,
              value === '1' ? classes.onItemRed : classes.offItemRed)}
            value={'1'}
            control={<Radio />}
            label={<><i className="fas fa-thumbs-down"/><i className="fas fa-thumbs-down"/><span>Very Poor</span></>}/>
          <FormControlLabel
            className={classNames(classes.defaultItem,
            value === '2' ? classes.onItemRed : classes.offItemRed)}
            value={'2'}
            control={<Radio />}
            label={<><i className="fas fa-thumbs-down"/><span>Poor</span></>}/>
          <FormControlLabel
            className={classNames(classes.defaultItem,
              value === '3' ? classes.onItemYellow : classes.offItemYellow)}
            value={'3'}
            control={<Radio />}
            label={<i className="fas fa-circle" style={{margin: 0}}/>} />
          <FormControlLabel
            className={classNames(classes.defaultItem,
              value === '4' ? classes.onItemGreen : classes.offItemGreen)}
            value={'4'}
            control={<Radio />}
            label={<><i className="fas fa-thumbs-up"/><span>Good</span></>} /> />
          <FormControlLabel
            className={classNames(classes.defaultItem,
              value === '5' ? classes.onItemGreen : classes.offItemGreen)}
            value={'5'}
            control={<Radio />}
            label={<><i className="fas fa-thumbs-up"/><i className="fas fa-thumbs-up"/><span>Very Good</span></>} />
        </RadioGroup>
  );
};
const ThreeRadioButton = withStyles(css, { withTheme: true })(ThreeRadioButtonComponent)

export default ThreeRadioButton;