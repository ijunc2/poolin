import React from 'react';
import classNames from 'classnames';
import { DatePickerInput } from 'rc-datepicker';
import {withStyles} from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import MenuItem from "@material-ui/core/MenuItem";
import {times} from './times';
import {timezone} from './timezone';
import {durations} from './durations';
import "moment/locale/ko";
import "./rc-datepicker.scss";
import css from "./css";
import { SubPopUp, Input, Select, Button, Typography, AutocompleteCard } from '../index'

const ScheduleModalComponent = ({ ...state }) => {

  const {
    classes,
    open,
    date,
    worldtime,
    time,
    duration,
    desc,
    title,
    location,
    handleModal,
    handleChange,
    handleDatePickerChange,
    handleAddSchedule,
    //
    autoCardData,
    interViewers,
    valueName,
    handleAutoCompleteAdd,
    handleAutoCompleteDelete,
  } = state;

  return (
      <SubPopUp
        title={'Schedule Interviews'}
        width={'900px'}
        height={'534px'}
        open={open}
        handleModal={handleModal}>
        <Typography
          fontWeight={3}
          className={classNames(classes.contentSubContant, classes.margB10)}>
          {'Plan, organize and schedule one or more interviews.'}
        </Typography>
        <div style={{display: 'flex', marginBottom: '10px'}}>
          <DatePickerInput
            style={{marginRight: '10px'}}
            name='date'
            displayFormat='YYYY-MM-DD'
            returnFormat='YYYY-MM-DD'
            validationFormat='YYYY-MM-DD'
            className='my-react-component'
            value={date}
            onChange={handleDatePickerChange}
            showOnInputClick
            placeholder='placeholder'
            locale='ko'
            iconClassName='far fa-calendar-alt'
            />
          <Select
            style={{width: '250px'}}
            value={worldtime}
            onChange={handleChange}
            name={"worldtime"}
            displayEmpty
            shape={'sm'}
            className={classes.select}>
              <MenuItem
                className={classes.menuItem}
                value="" disabled>
                  TimeZone
              </MenuItem>
            {timezone.map((r,i) => (
              <MenuItem key={i} value={r.country}
                className={classes.menuItem}>
                <div>
                  <Typography variant={'body2'}>
                    {r.country}
                  </Typography>
                  <Typography variant={'caption'}>
                    {r.time}
                  </Typography>
                </div>
              </MenuItem>
            ))}
          </Select>  
        </div>
        <div className={classes.timeScopeWrap}>
          <div style={{display: 'flex', marginBottom: '10px'}}>
            <Select
              style={{width: '100px', marginRight: '10px'}}
              value={time}
              onChange={handleChange}
              name={"time"}
              displayEmpty
              shape={'sm'}>
              <MenuItem
                className={classes.menuItem2}
                value="" disabled>
                  Time
              </MenuItem>
              {times().map((r,i) => (
                <MenuItem
                  className={classes.menuItem2}
                  key={i} value={r}>
                    {r}
                </MenuItem>
              ))}
            </Select>  
            <Select
              style={{width: '100px', marginRight: '10px'}}
              value={duration}
              onChange={handleChange}
              name={"duration"}
              displayEmpty
              shape={'sm'}>
              <MenuItem
                className={classes.menuItem2}
                value="" disabled>
                  Duration
              </MenuItem>
              {durations.map((r,i) => (
                <MenuItem
                  className={classes.menuItem2}
                  key={i} value={r.value}>
                    {r.name}
                </MenuItem>
              ))}
            </Select> 
            <AutocompleteCard
              autoCardData={autoCardData}
              interViewers={interViewers}
              valueName={valueName}
              handleAutoCompleteAdd={handleAutoCompleteAdd}
              handleAutoCompleteDelete={handleAutoCompleteDelete}/>
          </div>
          <div style={{display: 'flex', marginBottom: '10px'}}>
          <Input
              autoComplete={'off'}
              placeholder='Interview title'
              value={title}
              onChange={handleChange}
              className={classes.interViewtitle}
              name={'title'}
              shape={'sm'}
              color={blue}/>
            <Input
              autoComplete={'off'}
              placeholder='Location'
              value={location}
              onChange={handleChange}
              name={'location'}
              shape={'sm'}
              color={blue}/>
          </div>
          <div style={{display: 'flex', marginBottom: '20px'}}>
          <Input 
              className={classNames(classes.textArea, classes.interViewTextArea)}
              onChange={handleChange}
              placeholder={'Interview description'}
              value={desc}
              name={'desc'}
              multiline
              color={blue}/>
          </div>
        </div>
        <div style={{display: 'flex', justifyContent: 'flex-end'}}>
          <Button
            bgNone
            onClick={handleModal}
            style={{marginRight: '10px'}}>
            Cancel
          </Button>
          <Button
            stepBtn
            onClick={handleAddSchedule}
            style={{height: '32px'}}>
            Continue
          </Button>
            </div>
    </SubPopUp>
  );
};

const ScheduleModal = withStyles(css, { withTheme: true })(ScheduleModalComponent)

export default ScheduleModal;