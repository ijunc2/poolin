import { font } from '../../../assets/styles/material/com/index';

const styles = ( ) => ({
  margB10:{
    marginBottom: '10px !important'
  },
  contentSubContant:{
    fontSize: '14px',
    lineHeight: '1.5',
    fontWeight: '200',
  },
  timeScopeWrap:{
    marginBottom: '10px',
    padding: '10px',
    border: '1px solid #dedede',
    borderRadius: '4px',
    backgroundColor: '#fafafa',
    '& input': {
      backgroundColor: '#fff !important'
    }
  },
  interViewtitle:{
    marginRight: '10px !important',
    flex: 1,
  },
  textArea:{
    color: font.defaultFontColor,
    display: 'inherit !important',
    marginTop: '10px',
    marginBottom: '10px',
    '&>div':{
      height: '100px',
      width: '100%',
      '&>div':{
        height: '100%',
        overflowY: 'auto'
      }
    }
  },
  interViewTextArea:{
    flex: 1,
    backgroundColor: '#fff',
    '&>div':{
      height: '180px'
    },
    '& textarea::placeholder': {
      fontSize: '0.8rem',
      fontWeight: 600,
      lineHeight: '1.2rem',
    }
  },
  menuItem: {
    padding: 0,
    margin: '8px',
    height: '36px',
    borderRadius: '4px',
  },
  menuItem2: {
    margin: '8px',
    padding: '4px 16px',
    borderRadius: '4px',
  },
  select: {
    '& ul': {
      maxHeight: '400px'
    }
  }
})

export default styles;