import React from 'react';
import classNames from 'classnames';

const IconFileType = ({ ...state }) => {

  const fileTypeIcon = (filename) => {
    const icon = [
      "fas fa-file-pdf","fas fa-file-word", "fas fa-file-powerpoint",
      //pdf, word, 파워포인트,
       "fas fa-file-image",  "fas fa-file-excel",
       //이미지,엑셀
       "fas fa-file-audio", "fas fa-file-code", "fas fa-file-archive", "fas fa-file-video", "fas fa-file-alt"
       //오디오, 코드, 압축파일, 비디오, file
      ]
  const exel = ['xlsx','xlsm','xlsb','xltx','xltm','xls','xlt','xls','xml','xml','xlam','xla','xlw','xlr'];
  const img = ['bmp', 'rle', 'dib', 'jpeg', 'jpg', 'gif', 'png', 'tif', 'tiff'];
  const code = ['asp','cer','cfm','cgi','css','htm','js','jsp','part','php','py','rss','xhtml','c','class','cpp','cs','h','java','sh','swift','vb',]
  const zip = ['7z','AAC','ace','ALZ','APK','APPX','AT3','bke','ARC','ARJ','ASS','B','BA','big','BIN','bjsn','BKF','bzip2','bld','cab','c4','cab','cals','CLIPFLAIR','CPT','SEA','DAA','deb','DMG','DDZ','DN','DPE','egg','EGT','ECAB','ESD','ESS','Flipchart','GBP','GHO','GIF','gzip','HTML','IPG','jar','LBR','LBR','LQR','LHA','lzip','lzo','lzma','LZX','MBW','MHTML','MPQ','BIN','NTH','OAR','PAK','py','RAR','RAG','RaX','sb','sb2','SEN','SIT','SIS','SISX','SKB','SQ','SWM','SZS','TAR','TGZ','TB','TIB','UHA','UUE','VIV','VOL','VSA','WAX','WIM','XAP','xz','Z','zoo','zip']
  const videos = ['3g2','3gp','avi','flv','h264','m4v','mkv','mov','mp4','mpg','mpeg','rm','swf','vob','wmv']
  const audio = ['mp3','ogg','wma','wav','flac','mid','ac3','aac','ra']
  
  let samefileType = 9;
  let ft = -1; //ft == filetype
  ft = filename.substring(filename.lastIndexOf('.')+1) //가장 뒤에 확장자명 문자열
  ft.toLowerCase();
  if(ft === 'pdf')
    samefileType = 0;
  else if(ft === 'word')
    samefileType = 1;
  else if(ft === 'ppt')
    samefileType = 2;
  else if(Boolean(img.find( data => data === ft)))
    samefileType = 3;
  else if(Boolean(exel.find( data => data === ft)))
    samefileType = 4;
  else if(Boolean(audio.find( data => data === ft)))
    samefileType = 5;
  else if(Boolean(code.find( data => data === ft)))
    samefileType = 6;
  else if(Boolean(zip.find( data => data === ft)))
    samefileType = 7;
    else if(Boolean(videos.find( data => data === ft)))
    samefileType = 8;
    else  
    samefileType = 9;
  return icon[samefileType];
  };

  const {
    filename,
    className,
    ...other
  } = state;

  return (
    <i
      { ...other}
      className={classNames(className,fileTypeIcon(filename))}/>
  );
};

export default IconFileType;