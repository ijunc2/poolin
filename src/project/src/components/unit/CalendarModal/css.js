import { color } from '../../../assets/styles/material/com/index';

const styles = ( ) => ({

  CalendarModal:{
    '&>div:nth-child(2)': {
        '&>div': {
          minWidth: '500px',
        }
     }
  },
  detailsContainer: {
    padding: '20px 0',
  },
  detailsSection:{
    padding: '0px 20px 20px 20px',
    '& div': {
      flex: 1
    },
    '&>div:first-child': {
      borderRight: `1px solid ${color.gray.cardBorder}`
    },
    '&>div:nth-child(2)': {
      paddingLeft: '20px'
    }
  },
  detailsList: {
    padding: 0,
    margin: 0,
    '& li': {
      display: 'flex',
      margin: '0 0 10px 0',
      listStyle: 'none',
      color: color.gray.cardBorder,
      '& i': {
        lineHeight: '24px',
        fontSize: '.8rem',
        marginRight: '6px',
      }
    }
  },
  title:{
    backgroundColor: color.gray.default,
    display: 'flex',
    padding: '20px',
    justifyContent: 'space-between'
  },
  avatar:{
    width: '80px',
    height: '80px',
    border: '2px solid #fff',
    fontSize: '42px',
    lineHeight: '76px',
    md:{
      width: '20px',
      height: '20px',
      fontSize: '24px',
      lineHeight: '46px',
      border: 0
    }
  },
  titleTextWrap:{
    marginLeft: '10px',
    minHeight: '68px',
  },
  margB6:{
    marginBottom: '6px'
  },
  margR10:{
    marginRight: '10px'
  },
  margB5:{
    marginBottom: '5px'
  },
  margR5:{
    marginRight: '5px'
  },
  editBtn:{
    minWidth: '38.38px',
    height: '30px',
  },
})

export default styles;