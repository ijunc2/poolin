import React from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import Avatar from '@material-ui/core/Avatar';
import classNames from 'classnames';
import css from './css';
import { withStyles } from '@material-ui/core/styles';
import { Button, Typography } from '../index'

const HeaderComponent = ({...state}) => {

  const { name, classes, handleEditBtn, handleCandidateBtn }= state;

  return (
    <DialogTitle disableTypography className={classes.title}>
      <div style={{display: 'flex'}}>
        <Avatar className={classes.avatar}>
          {String(name).substring(0, 1).toUpperCase()}
        </Avatar>
        <div className={classes.titleTextWrap}>
          <Typography color={'#fff'} fontWeight={2} variant="h5" className={classes.margB6}>
            {name}
          </Typography>
          <div className={classes.margB6}>
            <Typography color={'#ddd'} fontWeight={2} variant="caption"
               className={classNames(classes.margR10, classes.margB5)}>
              <i className={classNames("fas fa-phone",classes.margR5)}/>
                ffff
            </Typography>
            <Typography color={'#ddd'} fontWeight={2} variant="caption" className={classes.margR10}>
              <i className={classNames("fas fa-envelope",classes.margR5)}/>
                rrr
            </Typography>
          </div>
        </div>
      </div>
        <div>
          <Button grayBtn type={'sm'}
            className={classNames(classes.editBtn, classes.margR5)}
            onClick={handleCandidateBtn}>
            <Typography fontWeight={3} variant="caption" >
              View Candidate
            </Typography> 
          </Button>
          <Button grayBtn type={'sm'} onClick={handleEditBtn}
            className={classes.editBtn}>
            <Typography fontWeight={3} variant="caption" >
              <i className="fas fa-edit"/>
            </Typography> 
          </Button>
        </div>
    </DialogTitle>
  );
};

const Header = withStyles(css, { withTheme: true })(HeaderComponent)

export default Header;