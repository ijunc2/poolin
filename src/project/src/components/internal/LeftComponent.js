import React from 'react';
import classNames from 'classnames';
import Drawer from "@material-ui/core/Drawer";
import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Collapse from '@material-ui/core/Collapse';

import Add from '@material-ui/icons/Add';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Home from '@material-ui/icons/Home';
import Done from '@material-ui/icons/Done';
import Brightness1 from '@material-ui/icons/Brightness1';
import CheckCircleOutlined from '@material-ui/icons/CheckCircleOutlined';
import Notifications from '@material-ui/icons/Notifications';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import MoreHoriz from '@material-ui/icons/MoreHoriz';
import Modal from '../../containers/VI_POL/PopProjectDetail';
import { ToolBarAvatar, Typography, Atag, Button } from '../unit/index';
import color from '../../assets/styles/material/com/color';
import CircularProgress from "@material-ui/core/CircularProgress";
import InviteModal from './LeftInviteComponent';

const projectArr = [{project_name: 'aaaaa', color: '#E5213B'},{project_name: 'bbbbb', color: '#4186E0'}]
const PalleteColor = ['#F6F8F9', '#E5213B', '#FD4E13', '#FD9A00', '#EEC300', '#94BA2B',
                      '#94BA2B', '#37C5AB', '#20AAEA', '#4186E0', '#7A6FF0', '#9E4CDF',
                      '#E362E3', '#EA4E9D', '#FC91AD', '#8DA3A6']
const ProjectItem = [
                {title: 'Remove from Favorites'},
                {title: 'Duplicate Project...'},
                {title: 'Archive Project'},
                {title: 'Delete Project'}]

const state = {
  modal: false,
  anchorEl: null,
  projectArr: [
    {color: 'red', name:'aaaa'},
    {color: 'blue', name:'bbbb'},
    {color: 'yellow', name:'cccc'}
  ],
  prjSelectOpne: false,
  favoriteOpen:false,
  reportOpen:false,
  inviteModalOpen: false,
  project: {color: 'red', name:'aaaa'}
}

class LeftComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state=state;
    this.handleModal = this.handleModal.bind(this);
    this.handleChanged = this.handleChanged.bind(this);
    this.handleToggleClick = this.handleToggleClick.bind(this);
    this.handleToggleClose = this.handleToggleClose.bind(this);
  }

  handleChanged(e){
    const {name, value} = e.target;
    this.setState({[name]: value})
  }

  handleToggleClick(value) {
    console.log(value)
    this.setState({anchorEl: value})
  }

  handleToggleClose() {
    this.setState({anchorEl: null})
  }

  handleModal(name){
    this.setState({
      [name]: !this.state[name]
    })
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if(this.props.open !== nextProps.workspace && !nextProps.workspace){
      this.props.close();
    }
  }

  render() {
    const { classes, theme, handleOpen, workspace } = this.props;

    const MenuArr =  [
         {
              name: 'Home' ,
              Icon:<Home className={classes.menuIcon}/>,
              url: '/overview'
          },
          {
              name: 'My Tasks' ,
              Icon:<CheckCircleOutlined className={classes.menuIcon}/>,
              url: '/my-tasks'
          },
          {
              name: 'Inbox' ,
              Icon:<Notifications className={classes.menuIcon}/>,
              url: '/inbox'
          },
          {
              name: 'Reports' ,
              Icon:<Notifications className={classes.menuIcon}/>,
              url: '/reports'
          },
    ];

    const MenuList__Component = (arr) =>
      <List className={classes.ListWrap}>
        {arr.map((r, index) => (
          <Atag
            onClick={()=>this.props.changeHeaderTitle(r.name)}
            key={index}
            href={`/${this.props.workspaceId}${r.url}`}>
              <ListItem 
                className={classNames(
                this.props.open ? classes.toolbarItemOP : null)}
                button >
                  <ListItemIcon
                      className={this.props.open ? classes.iconOpen : classes.iconClose }>
                      {r.Icon}
                  </ListItemIcon>
                  <Typography
                      variant={'caption'}
                      fontWeight={2}
                      color={this.props.open ? '#fff' : color.gray.default}>
                      {r.name}
                  </Typography>
              </ListItem>
          </Atag >
        ))}
      </List>;

    const projectListComponent = () =>
      <List className={classes.ListWrap}>
        {this.props.projectList.map((r, index) => (
          <ListItem 
            className={classNames(
            classes.toolbarItem,
            classes.ProjectItem)}
            button
            key={index}>
            <div style={{display: 'flex'}}>
              <Brightness1
                style={{color: this.props.open ? 
                  r.num === 0 ? color.gray.offGray : '#6AD790'
                  : color.gray.offGray}}
               className={classes.BrightBtn}>
              </Brightness1>
                <Typography
                  fontWeight={2}>
                  {r.project_name}
                </Typography>
              </div>
              <MoreHoriz
                style={{color: this.props.open ? 
                                r.num === 0 ? color.gray.offGray : '#6AD790'
                                : color.gray.offGray,
                        marginRight: '3px'}}>
              </MoreHoriz>
          </ListItem>
        ))}
    </List>;

  const projectListComponent1 = () =>
    <List>
      {projectArr.map((r, i)=>
        <div key={i}>
            <ListItem 
              className={classNames(
              classes.toolbarItem,
              classes.ProjectItem)}
              button>
            <div style={{display: 'flex'}}>
              <Brightness1
                style={{color: true ?
                  color.gray.offGray : r.color}}
              className={classes.BrightBtn}>
              </Brightness1>
                <Typography
                  fontWeight={2}>
                  {r.project_name}
                </Typography>
              </div>
              <IconButton
                onClick={()=>this.handleToggleClick(r.project_name)}
                className={classes.iconBtn}
                style={{color: true ? 
                                  0 === 0 ? color.gray.offGray : '#6AD790'
                                  : color.gray.offGray,
                          marginRight: '3px', position: 'relative'}}>
                <MoreHoriz/>
              </IconButton>
              <div
                style={{
                  display: this.state.anchorEl === r.project_name ? 'inherit' : 'none'}}
                  className={classes.projectListWrap}>
                  <div className={classes.paletteSwitchWrap}>
                  <MenuItem key={0} style={{paddingRight: 0}}>
                      <div style={{
                        width: '20px',
                        height: '20px',
                        background: r.color,
                        marginRight: '4px', 
                      }}/>
                      <Typography
                        variant={'caption'}>
                        {'Set Hightlight Color'}
                      </Typography>
                      <KeyboardArrowRight
                        style={{color: '#4c565c', marginLeft: '8px'}}/>
                    </MenuItem>
                    <Divider style={{backgroundColor: color.gray.hoverGray}}/>
                    <div className={classes.paletteWrap}>
                      {PalleteColor.map((color, i)=>
                        <div className={classes.paletteItem} key={i}
                          style={{background: color}}>
                          {r.color === color ? 
                          <Done className={classes.paletteItemCheck}/>
                            :
                            null}
                        </div>
                        )}
                    </div>
                    </div> 
                  {ProjectItem.map((r, i)=>
                    <MenuItem key={i + 1}>
                      <Typography
                        variant={'caption'}>
                        {r.title}
                      </Typography>
                    </MenuItem> )}
                )
            </div>
          </ListItem>
          <div style={{
            display: this.state.anchorEl === r.project_name ? 'inherit' : 'none',}}
            className={classes.projectListBg}
            onClick={()=>this.handleToggleClose()}/>
          </div>
        )}
    </List>;

    const MENU = () =>
      <Drawer
        variant="permanent"
        className={classNames(classes.drawer, {
          [classes.drawerOpen]: this.props.open,
          [classes.drawerClose]: !this.props.open,
        })}
        classes={{
          paper: classNames({
            [classes.drawerOpen]: this.props.open,
            [classes.drawerClose]: !this.props.open,
            [classes.toolbarWrap]: this.props.open,
          }),
        }}
        open={this.props.open}>
        <div>
        <div className={classes.toolbar}>
          <div style={{display: 'flex'}}>
            <ToolBarAvatar style={{marginLeft: '2px'}} color={this.state.project.color}>
              <Typography fontWeight={3} color={'#fff'}>
                {String(this.state.project.name).substring(0, 1).toUpperCase()}
              </Typography>
            </ToolBarAvatar>
            <Button className={classes.selectBtn}
              onClick={()=>this.handleModal('prjSelectOpne')}>
              <Typography
                variant={'body1'} fontWeight={3} color={'#fff'}>
                {this.state.project.name}
                <i className="fas fa-sort" style={{marginLeft: '8px'}}/>
              </Typography>
            </Button>
            <FormControl style={{position: 'relative', marginLeft: '-150px'}}>
              <Select
                className={classes.eventSelect}
                open={this.state.prjSelectOpne}
                onClose={()=>this.handleModal('prjSelectOpne')}
                onOpen={()=>this.handleModal('prjSelectOpne')}
                value={this.state.project}
                name={'project'}
                onChange={e=>this.handleChanged(e)}>
                {this.state.projectArr.map((r, i)=>
                  <MenuItem
                    key={i}
                    value={r}
                    className={classes.eventSelectItem}>
                      <ToolBarAvatar
                        color={r.color}
                        style={{marginRight: '8px'}}>
                        <Typography variant={'body1'} color={'#fff'}>
                          {String(r.name).substring(0, 1).toUpperCase()}
                        </Typography>
                      </ToolBarAvatar>
                      <Typography
                        variant={'caption'}
                        className={classes.text}>
                        {r.name}
                      </Typography>
                  </MenuItem>
                  )}
              </Select>
            </FormControl>
          </div>
          <IconButton 
            className={classNames(classes.iconBtn,
            this.props.open ? classes.toolbarItemOP : null)} onClick={handleOpen}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        <Divider style={{backgroundColor: color.gray.hoverGray}}/>
          {MenuList__Component(MenuArr)}
        {/* 페이버릿 메뉴 */}
        <Divider style={{backgroundColor: color.gray.hoverGray}}/>
          <div style={{minHeight:'42px'}} className={classes.toolbar}>
            <Typography
               fontWeight={2} 
               color={'#fff'}>
                Favorites
              </Typography>
            <IconButton 
              className={classNames(classes.iconBtn,
                !this.state.favoriteOpen ? classes.toolbarItemOP : null)}
              onClick={()=>this.handleModal('favoriteOpen')}>
              {this.state.favoriteOpen ?
                <ExpandLess /> :
                <ExpandMore />}
            </IconButton>
          </div>
          <Collapse in={this.state.favoriteOpen}  timeout='auto' unmountOnExit>
            {projectListComponent1()}
          </Collapse>
        {/* 페이버릿 메뉴 */}
        <Divider style={{backgroundColor: color.gray.hoverGray}}/>
          <div style={{minHeight:'42px'}} className={classes.toolbar}>
            <Typography
               fontWeight={2} 
               color={'#fff'}>
                Report
              </Typography>
            <IconButton 
              className={classNames(classes.iconBtn,
                !this.state.reportOpen ? classes.toolbarItemOP : null)}
              onClick={()=>this.handleModal('reportOpen')}>
              {this.state.reportOpen ?
                <ExpandLess /> :
                <ExpandMore />}
            </IconButton>
          </div>
          <Collapse in={this.state.reportOpen}  timeout='auto' unmountOnExit>
            {this.props.projectList.length === 0 ? 
              <div className={classes.toolbarFavo}>
                <Typography
                  color={color.gray.hoverGray}
                  variant={'caption'}>
                  Nothing to report
                </Typography>
              </div>
              :
              projectListComponent()}
          </Collapse>
        <Divider style={{backgroundColor: color.gray.hoverGray}}/>
          <div style={{minHeight:'42px'}} className={classes.toolbar}>
              <Typography
               fontWeight={2} 
               color={ this.props.open ? '#fff' : color.gray.offGray }>
                Project
              </Typography>
            <IconButton 
              onClick={()=>this.handleModal('modal')}
              className={classNames(classes.iconBtn,
              this.props.open ? classes.toolbarItemOP : null)}>
               <Add />
            </IconButton>
          </div>
            {this.props.loading_project_list ?
                <CircularProgress variant="determinate" />
                : projectListComponent()}
          </div>
          <div className={classes.bottomWrap}>
            <Typography
              variant={'h4'}
              fontWeight={3} 
              style={{alignItems: 'center'}}
              color={ this.props.open ? '#fff' : color.gray.offGray }>
              INPOOL
            </Typography>
            <Button
              variant="outlined"
              className={classNames(classes.invtBtn,
              this.props.open ? classes.toolbarItemOP : classes.iconClose)}
              onClick={()=>this.handleModal('inviteModalOpen')}>
              Invite to Inpool
            </Button>
          </div>
          <Modal
            open={this.state.modal}
            projectId={'p000000000014'}
            handleModal={()=>this.handleModal('modal')}/>
          <InviteModal
            classes={classes}
            open={this.state.inviteModalOpen}
            projectName={this.state.project.name}
            handleModal={()=>this.handleModal('inviteModalOpen')}/>
      </Drawer>
    return (
      <>
        {workspace ? <MENU /> : undefined}
      </>
    )
  }
}
export default LeftComponent;
