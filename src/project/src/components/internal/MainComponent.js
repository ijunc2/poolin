import React from 'react';
import { Route } from 'react-router-dom';
import style from '../../assets/styles/header'
import {withStyles} from "@material-ui/core";
import classNames from 'classnames';
import {ENTRANCE, OVERVIEW, MyTasks, PIPE, CALENDAR, File, INBOX} from '../../pages/index.async';

class Main extends React.Component {
  render() {
    const {classes} = this.props;
    return (
      <main
        className={classNames(classes.content, {
          [classes.contentShift]: this.props.open
        })} style={{ 
      }}>
        <Route exact path="/" component={PIPE} />
         {/* <Route exact path="/" component={PIPE} /> */}
        <Route exact path={`/:workspace_id/overview`} component={OVERVIEW} />
        <Route exact path={`/:workspace_id/calendar`} component={CALENDAR} />
        <Route exact path={`/:workspace_id/files`} component={File} />
        <Route exact path={`/:workspace_id/my-tasks`} component={MyTasks} />
        <Route exact path={`/:workspace_id/inbox`} component={INBOX} />
        <Route exact path={`/:workspace_id/reports`} component={OVERVIEW} />
      </main>
    )
  }
};

const MainComponent = withStyles(style, { withTheme: true })(Main);

export default MainComponent;