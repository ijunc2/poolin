import React from 'react';
import classNames from 'classnames';
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import MenuItem from "@material-ui/core/MenuItem";

import MoreHoriz from '@material-ui/icons/MoreHoriz';
import { Typography } from '../unit/index';
import color from '../../assets/styles/material/com/color';

const LeftProjectListComponent = ({ ...state }) => {

  const {
    children,
    href,
    ...other
  } = state;

  return (
  <List className={classes.ListWrap}>
    {projectArr.map((r, i)=>
      <>
          <ListItem 
            className={classNames(
            classes.toolbarItem,
            classes.ProjectItem)}
          button key={1}>
          <div style={{display: 'flex'}}>
            <Brightness1
              style={{color: true ? 
                color.gray.offGray : '#6AD790'}}
            className={classes.BrightBtn}>
            </Brightness1>
              <Typography
                fontWeight={2}>
                {r.project_name}
              </Typography>
            </div>
            <IconButton
              onClick={()=>this.handleToggleClick(r.project_name)}
              className={classes.iconBtn}
              style={{color: true ? 
                                0 === 0 ? color.gray.offGray : '#6AD790'
                                : color.gray.offGray,
                        marginRight: '3px', position: 'relative'}}>
              <MoreHoriz/>
            </IconButton>
            <div
              onClick={()=>this.handleToggleClose()}
              style={{
                position: 'absolute',
                display: this.state.anchorEl === r.project_name ? 'inherit' : 'none', 
                left: '200px',
                top: '20px',
                backgroundColor: '#fff',
                zIndex: 2,
                flexDirection: 'column',
              }}>
                <MenuItem key={1} selected={1 === 'Pyxis'}>
                  hhhhhh
                </MenuItem>
                <MenuItem key={2} selected={1 === 'Pyxis'}>
                  zzzzzz
                </MenuItem>
              )
          </div>
        </ListItem>
        <div style={{
          zIndex: 1,
          position: 'fixed',
          top: 0,
          right: 0,
          bottom: 0,
          left: 0,
          display: this.state.anchorEl === r.project_name ? 'inherit' : 'none',
          background: 'red',
          }}
          onClick={()=>this.handleToggleClose()}/>
        </>
      )}
  </List>
  );
};

export default LeftProjectListComponent;