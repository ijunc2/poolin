const common = {
  fontThin:'Rubik, sans-serif',
  fontWeight: ['200', '300', '400', '500', '600'],
  fontFamily1: 'Rubik, sans-serif',
}
export default common;